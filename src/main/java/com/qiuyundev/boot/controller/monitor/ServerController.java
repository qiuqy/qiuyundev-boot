package com.qiuyundev.boot.controller.monitor;

import cn.hutool.system.JavaInfo;
import cn.hutool.system.OsInfo;
import cn.hutool.system.SystemUtil;
import cn.hutool.system.oshi.CpuInfo;
import cn.hutool.system.oshi.OshiUtil;
import com.qiuyundev.boot.base.util.MultiThreadUtils;
import com.qiuyundev.boot.base.vo.Response;
import com.qiuyundev.boot.domain.vo.serve.Runtime;
import com.qiuyundev.boot.domain.vo.serve.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import oshi.hardware.CentralProcessor;
import oshi.hardware.GlobalMemory;
import oshi.software.os.OSFileStore;

import java.util.List;

@RestController
@RequestMapping("/system/server")
public class ServerController {

    @GetMapping("stat")
    @PreAuthorize("hasAuthority('monitor:server:stat')")
    public Response<ServerStatInfo> stat() {
        ServerStatInfo serverStatInfo = new ServerStatInfo();
        MultiThreadUtils.executeAsyncTasks(() -> {
            Runtime runtime = new Runtime();
            OsInfo osInfo = SystemUtil.getOsInfo();
            // 系统架构
            runtime.setArch(osInfo.getArch());
            runtime.setOs(osInfo.getName());
            JavaInfo javaInfo = SystemUtil.getJavaInfo();
            runtime.setJavaVendor(javaInfo.getVendor());
            runtime.setJavaVersion(javaInfo.getVersion());
            serverStatInfo.setRuntime(runtime);
        }, () -> {
            GlobalMemory globalMemory = OshiUtil.getMemory();
            Memory memory = new Memory();
            memory.setTotal(globalMemory.getTotal());
            memory.setAvailable(globalMemory.getAvailable());
            serverStatInfo.setMemory(memory);
        }, () -> {
            CpuInfo cpuInfo = OshiUtil.getCpuInfo();
            Cpu cpu = new Cpu();
            cpu.setCpuNum(cpuInfo.getCpuNum());
            cpu.setToTal(cpuInfo.getToTal());
            cpu.setSys(cpuInfo.getSys());
            cpu.setUser(cpuInfo.getUser());
            cpu.setWait(cpuInfo.getWait());
            cpu.setFree(cpuInfo.getFree());
            cpu.setUsed(cpuInfo.getUsed());
            CentralProcessor processor = OshiUtil.getProcessor();
            cpu.setCpuModel(processor.getProcessorIdentifier().getName());
            serverStatInfo.setCpu(cpu);
        }, () -> {
            List<OSFileStore> osFileStores = OshiUtil.getOs().getFileSystem().getFileStores();
            List<FileStore> fileStores = osFileStores.stream().map(osFileStore -> {
                FileStore fileStore = new FileStore();
                fileStore.setName(osFileStore.getName());
                fileStore.setLabel(osFileStore.getLabel());
                fileStore.setMount(osFileStore.getMount());
                fileStore.setDescription(osFileStore.getDescription());
                fileStore.setFsType(osFileStore.getType());
                fileStore.setFreeSpace(osFileStore.getUsableSpace());
                fileStore.setTotalSpace(osFileStore.getTotalSpace());
                return fileStore;
            }).toList();

            serverStatInfo.setFileStores(fileStores);
        });

        return Response.success(serverStatInfo);
    }
}
