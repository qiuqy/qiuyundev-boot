package com.qiuyundev.boot.controller.monitor;

import com.qiuyundev.boot.base.vo.PageResult;
import com.qiuyundev.boot.base.vo.Response;
import com.qiuyundev.boot.domain.vo.schedule.SysScheduleLogPageReq;
import com.qiuyundev.boot.domain.vo.schedule.SysScheduleLogResp;
import com.qiuyundev.boot.service.ISysScheduleLogService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 任务日志表 前端控制器
 * </p>
 *
 * @author Qiuyun
 * @since 2024-06-03
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/system/scheduleLog")
public class SysScheduleLogController {
    private final ISysScheduleLogService service;

    /**
     * 分页查询任务日志表
     *
     * @param pageReq 任务日志表分页查询参数
     * @return 任务日志表列表
     */
    @GetMapping("/page")
    public Response<PageResult<SysScheduleLogResp>> page(SysScheduleLogPageReq pageReq) {
        return Response.success(this.service.page(pageReq));
    }

    /**
     * 任务日志表详情
     *
     * @param id 任务日志表 ID
     * @return 任务日志表详情
     */
    @GetMapping("/{id}")
    public Response<SysScheduleLogResp> detail(@PathVariable("id") Integer id) {
        return Response.success(this.service.detail(id));
    }

}
