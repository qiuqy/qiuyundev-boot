package com.qiuyundev.boot.controller.monitor;

import com.qiuyundev.boot.base.vo.PageResult;
import com.qiuyundev.boot.base.vo.Response;
import com.qiuyundev.boot.domain.vo.onlineuser.OnlineKickReq;
import com.qiuyundev.boot.domain.vo.onlineuser.OnlinePageReq;
import com.qiuyundev.boot.domain.vo.onlineuser.OnlineUserResp;
import com.qiuyundev.boot.service.IOnlineUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/system/onlineUser")
@RequiredArgsConstructor
public class OnlineUserController {
    private final IOnlineUserService onlineUserService;

    /**
     * 分页查询在线用户
     *
     * @param onlinePageReq 在线用户分页查询参数
     * @return 在线用户列表
     */
    @GetMapping("/page")
    @PreAuthorize("hasAuthority('monitor:online:list')")
    public Response<PageResult<OnlineUserResp>> page(OnlinePageReq onlinePageReq) {
        return Response.success(this.onlineUserService.page(onlinePageReq));
    }

    @PostMapping("/kick")
    @PreAuthorize("hasAuthority('monitor:online:kick')")
    public Response<Boolean> kick(@RequestBody OnlineKickReq onlineKickReq) {
        return Response.success(this.onlineUserService.kick(onlineKickReq.getUserKey()));
    }
}
