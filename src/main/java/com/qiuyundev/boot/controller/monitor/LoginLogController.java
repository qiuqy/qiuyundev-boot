package com.qiuyundev.boot.controller.monitor;

import com.qiuyundev.boot.base.vo.PageResult;
import com.qiuyundev.boot.base.vo.Response;
import com.qiuyundev.boot.domain.vo.loginlog.LoginLogPageReq;
import com.qiuyundev.boot.domain.vo.loginlog.LoginLogResp;
import com.qiuyundev.boot.domain.vo.schedule.SysScheduleFormReq;
import com.qiuyundev.boot.domain.vo.schedule.SysSchedulePageReq;
import com.qiuyundev.boot.domain.vo.schedule.SysScheduleResp;
import com.qiuyundev.boot.service.ISysLoginLogService;
import com.qiuyundev.boot.service.ISysScheduleService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/system/loginLog")
public class LoginLogController {
    private final ISysLoginLogService loginLogService;


    /**
     * 分页查询登录记录
     *
     * @param loginLogPageReq 登录记录分页查询参数
     * @return 登录记录列表
     */
    @GetMapping("page")
    @PreAuthorize("hasAuthority('monitor:login-log:list')")
    public Response<PageResult<LoginLogResp>> page(LoginLogPageReq loginLogPageReq) {
        return Response.success(this.loginLogService.page(loginLogPageReq));
    }

    /**
     * <p>
     * 任务前端控制器
     * </p>
     *
     * @author Qiuyun
     * @since 2024-05-31
     */
    @RestController
    @RequiredArgsConstructor
    @RequestMapping("/tool/schedule")
    public static class SysScheduleController {
        private final ISysScheduleService service;

        /**
         * 分页查询任务表
         *
         * @param pageReq 任务表分页查询参数
         * @return 任务表列表
         */
        @GetMapping("/page")
        public Response<PageResult<SysScheduleResp>> page(SysSchedulePageReq pageReq) {
            return Response.success(this.service.page(pageReq));
        }

        /**
         * 任务表详情
         *
         * @param id 任务ID
         * @return 任务表详情
         */
        @GetMapping("/{id}")
        public Response<SysScheduleResp> detail(@PathVariable("id") Integer id) {
            return Response.success(this.service.detail(id));
        }

        /**
         * 新增任务表
         *
         * @param formReq 任务表新增参数
         * @return 任务表新增结果
         */
        @PostMapping
        public Response<Boolean> add(@RequestBody SysScheduleFormReq formReq) {
           return Response.success(this.service.add(formReq));
        }

        /**
         * 修改任务表
         *
         * @param id          任务ID
         * @param formReq 任务表修改参数
         * @return 任务表修改结果
         */
        @PutMapping("/{id}")
        public Response<Boolean> update(@PathVariable("id") Integer id, @RequestBody SysScheduleFormReq formReq) {
            formReq.setId(id);
            return Response.success(this.service.update(formReq));
        }


        /**
         * 删除任务表
         *
         * @param ids 任务ID 列表
         * @return 任务表删除结果
         */
        @DeleteMapping("/{ids}")
        public Response<Boolean> delete(@PathVariable("ids") Integer[] ids) {
            return Response.success(this.service.delete(ids));
        }

        /**
         * 手动执行一次任务
         *
         * @param id  任务ID
         * @return 执行结果
         */
        @PutMapping("/{id}/once")
        public Response<Boolean> once(@PathVariable("id") Integer id) {
            return Response.success(this.service.once(id));
        }

        /**
         * 启动任务
         *
         * @param id   任务ID
         * @return 执行结果
         */
        @PutMapping("/{id}/start")
        public Response<Boolean> start(@PathVariable("id") Integer id) {
            return Response.success(this.service.start(id));
        }

        /**
         * 停止任务
         *
         * @param id          任务ID
         * @return 执行结果
         */
        @PutMapping("/{id}/stop")
        public Response<Boolean> stop(@PathVariable("id") Integer id) {
            return Response.success(this.service.stop(id));
        }

    }
}
