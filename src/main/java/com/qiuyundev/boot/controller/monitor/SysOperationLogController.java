package com.qiuyundev.boot.controller.monitor;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import com.qiuyundev.boot.base.vo.PageResult;
import com.qiuyundev.boot.base.vo.Response;
import com.qiuyundev.boot.service.ISysOperationLogService;
import com.qiuyundev.boot.domain.vo.operation.OperationLogFormReq;
import com.qiuyundev.boot.domain.vo.operation.OperationLogPageReq;
import com.qiuyundev.boot.domain.vo.operation.OperationLogResp;

/**
 * <p>
 * 操作日志 前端控制器
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-21
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/system/operationLog")
public class SysOperationLogController {
    private final ISysOperationLogService service;

    /**
     * 分页查询操作日志
     *
     * @param pageReq 操作日志分页查询参数
     * @return 操作日志列表
     */
    @GetMapping("/page")
    @PreAuthorize("hasAuthority('monitor:operation-log:list')")
    public Response<PageResult<OperationLogResp>> page(OperationLogPageReq pageReq) {
        return Response.success(this.service.page(pageReq));
    }

    /**
     * 操作日志详情
     *
     * @param id 操作日志 ID
     * @return 操作日志详情
     */
    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('monitor:operation-log:detail')")
    public Response<OperationLogResp> detail(@PathVariable("id") Integer id) {
        return Response.success(this.service.detail(id));
    }

    /**
     * 新增操作日志
     *
     * @param formReq 操作日志新增参数
     * @return 操作日志新增结果
     */
    @PostMapping
    public Response<Boolean> add(@RequestBody OperationLogFormReq formReq) {
       return Response.success(this.service.add(formReq));
    }

    /**
     * 修改操作日志
     *
     * @param id          操作日志 ID
     * @param formReq 操作日志修改参数
     * @return 操作日志修改结果
     */
    @PutMapping("/{id}")
    public Response<Boolean> update(@PathVariable("id") Integer id, @RequestBody OperationLogFormReq formReq) {
        formReq.setId(id);
        return Response.success(this.service.update(formReq));
    }


    /**
     * 删除操作日志
     *
     * @param ids 操作日志 ID 列表
     * @return 操作日志删除结果
     */
    @DeleteMapping("/{ids}")
    public Response<Boolean> delete(@PathVariable("ids") Long[] ids) {
        return Response.success(this.service.delete(ids));
    }

}
