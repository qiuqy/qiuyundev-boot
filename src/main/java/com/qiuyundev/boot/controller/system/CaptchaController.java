package com.qiuyundev.boot.controller.system;

import com.qiuyundev.boot.base.util.BeanUtils;
import com.qiuyundev.boot.base.vo.Response;
import com.qiuyundev.boot.core.captcha.AbstractCaptchaService;
import com.qiuyundev.boot.domain.vo.captcha.ImageCaptchaResp;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 验证码 前端控制器
 *
 * @author Qiuyun
 * @since 2024/04/12
 */
@RestController
@RequestMapping("/auth/captcha")
@RequiredArgsConstructor
public class CaptchaController {
    private final AbstractCaptchaService captchaService;

    /**
     * 生成验证码
     */
    @GetMapping("/image")
    public Response<ImageCaptchaResp> captchaImage(@RequestParam(value = "width", required = false) Integer width,
                                                   @RequestParam(value = "height", required = false) Integer height) {
        AbstractCaptchaService.Captcha captcha = this.captchaService.generateCaptcha(width, height);
        return Response.success(BeanUtils.copy(captcha, ImageCaptchaResp.class));
    }
}
