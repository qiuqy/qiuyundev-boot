package com.qiuyundev.boot.controller.system;

import com.qiuyundev.boot.domain.vo.dict.DictItemFormReq;
import com.qiuyundev.boot.domain.vo.dict.DictItemPageReq;
import com.qiuyundev.boot.domain.vo.dict.DictItemResp;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import com.qiuyundev.boot.base.vo.PageResult;
import com.qiuyundev.boot.base.vo.Response;
import com.qiuyundev.boot.service.ISysDictItemService;

/**
 * <p>
 * 字典项表 前端控制器
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-13
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/system/dictItem")
public class SysDictItemController {
    private final ISysDictItemService service;

    /**
     * 分页查询字典项表
     *
     * @param pageReq 字典项表分页查询参数
     * @return 字典项表列表
     */
    @GetMapping("/page")
    @PreAuthorize("hasAuthority('system:dict-item:list')")
    public Response<PageResult<DictItemResp>> page(DictItemPageReq pageReq) {
        return Response.success(this.service.page(pageReq));
    }

    /**
     * 字典项表详情
     *
     * @param id 字典项表 ID
     * @return 字典项表详情
     */
    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('system:dict-item:detail')")
    public Response<DictItemResp> detail(@PathVariable("id") Integer id) {
        return Response.success(this.service.detail(id));
    }

    /**
     * 新增字典项表
     *
     * @param formReq 字典项表新增参数
     * @return 字典项表新增结果
     */
    @PostMapping
    @PreAuthorize("hasAuthority('system:dict-item:add')")
    public Response<Boolean> add(@RequestBody DictItemFormReq formReq) {
       return Response.success(this.service.add(formReq));
    }

    /**
     * 修改字典项表
     *
     * @param id          字典项表 ID
     * @param formReq 字典项表修改参数
     * @return 字典项表修改结果
     */
    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('system:dict-item:update')")
    public Response<Boolean> update(@PathVariable("id") Integer id, @RequestBody DictItemFormReq formReq) {
        formReq.setId(id);
        return Response.success(this.service.update(formReq));
    }


    /**
     * 删除字典项表
     *
     * @param ids 字典项表 ID 列表
     * @return 字典项表删除结果
     */
    @DeleteMapping("/{ids}")
    @PreAuthorize("hasAuthority('system:dict-item:delete')")
    public Response<Boolean> delete(@PathVariable("ids") Long[] ids) {
        return Response.success(this.service.delete(ids));
    }

}
