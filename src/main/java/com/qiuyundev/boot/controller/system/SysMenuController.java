package com.qiuyundev.boot.controller.system;

import com.qiuyundev.boot.base.vo.Response;
import com.qiuyundev.boot.domain.vo.TreeVO;
import com.qiuyundev.boot.domain.vo.menu.MenuFormReq;
import com.qiuyundev.boot.domain.vo.menu.MenuListReq;
import com.qiuyundev.boot.domain.vo.menu.MenuResp;
import com.qiuyundev.boot.service.ISysMenuService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 菜单 前端控制器
 * </p>
 *
 * @author Qiuyun
 * @since 2024/04/12
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/system/menu")
public class SysMenuController {
    private final ISysMenuService menuService;

    /**
     * 菜单列表
     *
     * @param menuListReq 菜单分页查询参数
     * @return 菜单列表
     */
    @GetMapping("/list")
    @PreAuthorize("hasAuthority('system:menu:list')")
    public Response<List<TreeVO.TreeResp>> list(MenuListReq menuListReq) {
        return Response.success(this.menuService.list(menuListReq));
    }

    /**
     * 菜单详情
     *
     * @param id 菜单 ID
     * @return 菜单详情
     */
    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('system:menu:detail')")
    public Response<MenuResp> detail(@PathVariable("id") Integer id) {
        return Response.success(this.menuService.detail(id));
    }

    /**
     * 新增菜单
     *
     * @param menuFormReq 菜单新增参数
     * @return 菜单新增结果
     */
    @PostMapping
    @PreAuthorize("hasAuthority('system:menu:add')")
    public Response<Boolean> add(@RequestBody MenuFormReq menuFormReq) {
        return Response.success(this.menuService.add(menuFormReq));
    }

    /**
     * 修改菜单
     *
     * @param id          菜单 ID
     * @param menuFormReq 菜单修改参数
     * @return 菜单修改结果
     */
    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('system:menu:update')")
    public Response<Boolean> update(@PathVariable("id") Integer id, @RequestBody MenuFormReq menuFormReq) {
        menuFormReq.setId(id);
        return Response.success(this.menuService.update(menuFormReq));
    }


    /**
     * 修改菜单
     *
     * @param ids 菜单 ID 列表
     * @return 菜单修改结果
     */
    @DeleteMapping("/{ids}")
    @PreAuthorize("hasAuthority('system:menu:delete')")
    public Response<Boolean> delete(@PathVariable("ids") Long[] ids) {
        return Response.success(this.menuService.delete(ids));
    }

    /**
     * 菜单树
     *
     * @param treeReq 菜单树查询参数
     * @return 菜单树
     */
    @GetMapping("/tree")
    public Response<List<TreeVO.TreeResp>> tree(TreeVO.TreeReq treeReq) {
        return Response.success(this.menuService.tree(treeReq));
    }

    /**
     * 查询全部权限
     *
     * @return 全部权限
     */
    @GetMapping("/permissions")
    public Response<List<String>> permissions() {
        return Response.success(this.menuService.permissions());
    }

}
