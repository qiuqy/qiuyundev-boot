package com.qiuyundev.boot.controller.system;

import com.qiuyundev.boot.base.vo.PageResult;
import com.qiuyundev.boot.core.log.ModuleEnum;
import com.qiuyundev.boot.core.log.OperationLog;
import com.qiuyundev.boot.core.log.OperationTypeEnum;
import com.qiuyundev.boot.domain.vo.config.ConfigFormReq;
import com.qiuyundev.boot.domain.vo.config.ConfigPageReq;
import com.qiuyundev.boot.domain.vo.config.ConfigResp;
import com.qiuyundev.boot.service.ISysConfigService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import com.qiuyundev.boot.base.vo.Response;

/**
 * <p>
 * 参数配置 前端控制器
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-06
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/system/config")
public class SysConfigController {
    private final ISysConfigService service;

    /**
     * 分页查询参数配置
     *
     * @param pageReq 参数配置分页查询参数
     * @return 参数配置列表
     */
    @OperationLog(module = ModuleEnum.SYSTEM, tag = "参数配置", type = OperationTypeEnum.QUERY, summary = "分页查询参数配置")
    @GetMapping("/page")
    @PreAuthorize("hasAuthority('system:config:list')")
    public Response<PageResult<ConfigResp>> page(ConfigPageReq pageReq) {
        return Response.success(this.service.page(pageReq));
    }

    /**
     * 参数配置详情
     *
     * @param id 参数配置 ID
     * @return 参数配置详情
     */
    @OperationLog(module = ModuleEnum.SYSTEM, tag = "参数配置", type = OperationTypeEnum.QUERY, summary = "参数配置详情")
    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('system:config:detail')")
    public Response<ConfigResp> detail(@PathVariable("id") Integer id) {
        return Response.success(this.service.detail(id));
    }

    /**
     * 新增参数配置
     *
     * @param formReq 参数配置新增参数
     * @return 参数配置新增结果
     */
    @OperationLog(module = ModuleEnum.SYSTEM, tag = "参数配置", type = OperationTypeEnum.ADD, summary = "新增参数配置")
    @PostMapping
    @PreAuthorize("hasAuthority('system:config:add')")
    public Response<Boolean> add(@RequestBody ConfigFormReq formReq) {
       return Response.success(this.service.add(formReq));
    }

    /**
     * 修改参数配置
     *
     * @param id          参数配置 ID
     * @param formReq 参数配置修改参数
     * @return 参数配置修改结果
     */
    @OperationLog(module = ModuleEnum.SYSTEM, tag = "参数配置", type = OperationTypeEnum.UPDATE, summary = "修改参数配置")
    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('system:config:update')")
    public Response<Boolean> update(@PathVariable("id") Integer id, @RequestBody ConfigFormReq formReq) {
        formReq.setId(id);
        return Response.success(this.service.update(formReq));
    }


    /**
     * 删除参数配置
     *
     * @param ids 参数配置 ID 列表
     * @return 参数配置删除结果
     */
    @OperationLog(module = ModuleEnum.SYSTEM, tag = "参数配置", type = OperationTypeEnum.DELETE, summary = "删除参数配置")
    @DeleteMapping("/{ids}")
    @PreAuthorize("hasAuthority('system:config:delete')")
    public Response<Boolean> delete(@PathVariable("ids") Long[] ids) {
        return Response.success(this.service.delete(ids));
    }

}
