package com.qiuyundev.boot.controller.system;

import com.qiuyundev.boot.domain.vo.dict.DictTypeResp;
import com.qiuyundev.boot.domain.vo.post.PostBriefResp;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import com.qiuyundev.boot.base.vo.PageResult;
import com.qiuyundev.boot.base.vo.Response;
import com.qiuyundev.boot.service.ISysPostService;
import com.qiuyundev.boot.domain.vo.post.PostFormReq;
import com.qiuyundev.boot.domain.vo.post.PostPageReq;
import com.qiuyundev.boot.domain.vo.post.PostResp;

import java.util.List;

/**
 * <p>
 * 职位 前端控制器
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-07
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/system/post")
public class SysPostController {
    private final ISysPostService service;

    /**
     * 分页查询职位
     *
     * @param pageReq 职位分页查询参数
     * @return 职位列表
     */
    @GetMapping("/page")
    @PreAuthorize("hasAuthority('system:post:list')")
    public Response<PageResult<PostResp>> page(PostPageReq pageReq) {
        return Response.success(this.service.page(pageReq));
    }

    /**
     * 职位详情
     *
     * @param id 职位 ID
     * @return 职位详情
     */
    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('system:post:detail')")
    public Response<PostResp> detail(@PathVariable("id") Integer id) {
        return Response.success(this.service.detail(id));
    }

    /**
     * 新增职位
     *
     * @param formReq 职位新增参数
     * @return 职位新增结果
     */
    @PostMapping
    @PreAuthorize("hasAuthority('system:post:add')")
    public Response<Boolean> add(@RequestBody PostFormReq formReq) {
       return Response.success(this.service.add(formReq));
    }

    /**
     * 修改职位
     *
     * @param id          职位 ID
     * @param formReq 职位修改参数
     * @return 职位修改结果
     */
    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('system:post:update')")
    public Response<Boolean> update(@PathVariable("id") Integer id, @RequestBody PostFormReq formReq) {
        formReq.setId(id);
        return Response.success(this.service.update(formReq));
    }


    /**
     * 删除职位
     *
     * @param ids 职位 ID 列表
     * @return 职位删除结果
     */
    @DeleteMapping("/{ids}")
    @PreAuthorize("hasAuthority('system:post:delete')")
    public Response<Boolean> delete(@PathVariable("ids") Long[] ids) {
        return Response.success(this.service.delete(ids));
    }

    /**
     * 岗位下拉选
     *
     * @return 岗位下拉选
     */
    @GetMapping("/options")
    public Response<List<PostBriefResp>> options() {
        return Response.success(this.service.options());
    }

}
