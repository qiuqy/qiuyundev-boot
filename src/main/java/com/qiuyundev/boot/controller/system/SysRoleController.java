package com.qiuyundev.boot.controller.system;

import com.qiuyundev.boot.base.vo.PageResult;
import com.qiuyundev.boot.base.vo.Response;
import com.qiuyundev.boot.domain.vo.role.RoleBriefResp;
import com.qiuyundev.boot.domain.vo.role.RoleFormReq;
import com.qiuyundev.boot.domain.vo.role.RolePageReq;
import com.qiuyundev.boot.domain.vo.role.RoleResp;
import com.qiuyundev.boot.service.ISysRoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 角色 前端控制器
 * </p>
 *
 * @author Qiuyun
 * @since 2024/04/12
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/system/role")
public class SysRoleController {
    private final ISysRoleService roleService;

    /**
     * 分页查询角色
     *
     * @param rolePageReq 角色分页查询参数
     * @return 角色列表
     */
    @GetMapping("/page")
    @PreAuthorize("hasAuthority('system:role:list') or hasRole('admin')")
    public Response<PageResult<RoleResp>> page(RolePageReq rolePageReq) {
        return Response.success(this.roleService.page(rolePageReq));
    }

    /**
     * 角色详情
     *
     * @param id 角色 ID
     * @return 角色详情
     */
    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('system:role:detail') or hasRole('admin')")
    public Response<RoleResp> detail(@PathVariable("id") Integer id) {
        return Response.success(this.roleService.detail(id));
    }

    /**
     * 新增角色
     *
     * @param roleFormReq 角色新增参数
     * @return 角色新增结果
     */
    @PostMapping
    @PreAuthorize("hasAuthority('system:role:add') or hasRole('admin')")
    public Response<Boolean> add(@RequestBody RoleFormReq roleFormReq) {
        return Response.success(this.roleService.add(roleFormReq));
    }

    /**
     * 修改角色
     *
     * @param id          角色 ID
     * @param roleFormReq 角色修改参数
     * @return 角色修改结果
     */
    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('system:role:update') or hasRole('admin')")
    public Response<Boolean> update(@PathVariable("id") Integer id, @RequestBody RoleFormReq roleFormReq) {
        roleFormReq.setId(id);
        return Response.success(this.roleService.update(roleFormReq));
    }


    /**
     * 删除角色
     *
     * @param ids 角色 ID 列表
     * @return 角色删除结果
     */
    @DeleteMapping("/{ids}")
    @PreAuthorize("hasAuthority('system:role:delete') or hasRole('admin')")
    public Response<Boolean> delete(@PathVariable("ids") Long[] ids) {
        return Response.success(this.roleService.delete(ids));
    }

    /**
     * 查询角色下拉选列表
     *
     * @return 角色下拉选列表
     */
    @GetMapping("options")
    public Response<List<RoleBriefResp>> options() {
        return Response.success(this.roleService.options());
    }
}
