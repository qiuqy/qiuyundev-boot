package com.qiuyundev.boot.controller.system;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import com.qiuyundev.boot.base.vo.PageResult;
import com.qiuyundev.boot.base.vo.Response;
import com.qiuyundev.boot.service.ISysDictTypeService;
import com.qiuyundev.boot.domain.vo.dict.DictTypeFormReq;
import com.qiuyundev.boot.domain.vo.dict.DictTypePageReq;
import com.qiuyundev.boot.domain.vo.dict.DictTypeResp;

import java.util.List;

/**
 * <p>
 * 字典类型 前端控制器
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-13
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/system/dictType")
public class SysDictTypeController {
    private final ISysDictTypeService service;

    /**
     * 分页查询字典类型
     *
     * @param pageReq 字典类型分页查询参数
     * @return 字典类型列表
     */
    @GetMapping("/page")
    @PreAuthorize("hasAuthority('system:dict-type:list')")
    public Response<PageResult<DictTypeResp>> page(DictTypePageReq pageReq) {
        return Response.success(this.service.page(pageReq));
    }

    /**
     * 字典类型详情
     *
     * @param id 字典类型 ID
     * @return 字典类型详情
     */
    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('system:dict-type:detail')")
    public Response<DictTypeResp> detail(@PathVariable("id") Integer id) {
        return Response.success(this.service.detail(id));
    }

    /**
     * 新增字典类型
     *
     * @param formReq 字典类型新增参数
     * @return 字典类型新增结果
     */
    @PostMapping
    @PreAuthorize("hasAuthority('system:dict-type:add')")
    public Response<Boolean> add(@RequestBody DictTypeFormReq formReq) {
       return Response.success(this.service.add(formReq));
    }

    /**
     * 修改字典类型
     *
     * @param id          字典类型 ID
     * @param formReq 字典类型修改参数
     * @return 字典类型修改结果
     */
    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('system:dict-type:update')")
    public Response<Boolean> update(@PathVariable("id") Integer id, @RequestBody DictTypeFormReq formReq) {
        formReq.setId(id);
        return Response.success(this.service.update(formReq));
    }


    /**
     * 删除字典类型
     *
     * @param ids 字典类型 ID 列表
     * @return 字典类型删除结果
     */
    @DeleteMapping("/{ids}")
    @PreAuthorize("hasAuthority('system:dict-type:delete')")
    public Response<Boolean> delete(@PathVariable("ids") Long[] ids) {
        return Response.success(this.service.delete(ids));
    }

    /**
     * 字典类型下拉选
     *
     * @return 字典类型下拉选
     */
    @GetMapping("/options")
    public Response<List<DictTypeResp>> options() {
        return Response.success(this.service.options());
    }

}
