package com.qiuyundev.boot.controller.system;


import com.qiuyundev.boot.base.vo.Response;
import com.qiuyundev.boot.core.security.LoginUser;
import com.qiuyundev.boot.core.security.SecurityUtils;
import com.qiuyundev.boot.domain.vo.account.AccountInfoResp;
import com.qiuyundev.boot.domain.vo.account.AccountMenuResp;
import com.qiuyundev.boot.service.IAccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 账户 前端控制器
 *
 * @author Qiuyun
 * @since 2024/04/12
 */
@RestController
@RequestMapping("/account")
@RequiredArgsConstructor
public class AccountController {
    private final IAccountService accountService;

    /**
     * 获取当前登录用户信息
     */
    @GetMapping("/profile")
    public Response<AccountInfoResp> profile() {
        LoginUser loginUser = SecurityUtils.getUserInfo();
        AccountInfoResp accountInfoResp = AccountInfoResp.builder()
                .id(loginUser.getUser().getId())
                .username(loginUser.getUser().getUsername())
                .nickname(loginUser.getUser().getNickname())
                .avatar(loginUser.getUser().getAvatar())
                .email(loginUser.getUser().getEmail())
                .phone(loginUser.getUser().getPhone())
                .ip(loginUser.getLoginInfo().getIp())
                .build();
        return Response.success(accountInfoResp);
    }

    /**
     * 获取当前登录用户菜单列表
     */
    @GetMapping("/menus")
    public Response<List<AccountMenuResp>> menus() {
        return Response.success(this.accountService.menus());
    }

    /**
     * 获取当前登录用户菜单列表
     */
//    @SneakyThrows
//    @GetMapping("/menus")
//    public Response<JsonNode> getUserInfo() {
//        return Response.success(new ObjectMapper().readTree("[{\n" +
//                "            \"id\": 12,\n" +
//                "            \"path\": \"/document\",\n" +
//                "            \"component\": \"\",\n" +
//                "            \"name\": \"文档\",\n" +
//                "            \"meta\": {\n" +
//                "                \"title\": \"文档\",\n" +
//                "                \"icon\": \"ion:tv-outline\",\n" +
//                "                \"isExt\": false,\n" +
//                "                \"extOpenMode\": 1,\n" +
//                "                \"type\": 0,\n" +
//                "                \"orderNo\": 2,\n" +
//                "                \"show\": 1,\n" +
//                "                \"activeMenu\": null,\n" +
//                "                \"status\": 1,\n" +
//                "                \"keepAlive\": 0\n" +
//                "            },\n" +
//                "            \"redirect\": \"/document/https://www.typeorm.org/\",\n" +
//                "            \"children\": [\n" +
//                "                {\n" +
//                "                    \"id\": 14,\n" +
//                "                    \"path\": \"https://www.typeorm.org/\",\n" +
//                "                    \"name\": \"Typeorm中文文档(外链)\",\n" +
//                "                    \"meta\": {\n" +
//                "                        \"title\": \"Typeorm中文文档(外链)\",\n" +
//                "                        \"icon\": \"\",\n" +
//                "                        \"isExt\": true,\n" +
//                "                        \"extOpenMode\": 1,\n" +
//                "                        \"type\": 1,\n" +
//                "                        \"orderNo\": 3,\n" +
//                "                        \"show\": 1,\n" +
//                "                        \"activeMenu\": null,\n" +
//                "                        \"status\": 1,\n" +
//                "                        \"keepAlive\": 0\n" +
//                "                    }\n" +
//                "                },\n" +
//                "                {\n" +
//                "                    \"id\": 15,\n" +
//                "                    \"path\": \"https://docs.nestjs.cn/\",\n" +
//                "                    \"name\": \"Nest.js中文文档(内嵌)\",\n" +
//                "                    \"meta\": {\n" +
//                "                        \"title\": \"Nest.js中文文档(内嵌)\",\n" +
//                "                        \"icon\": \"\",\n" +
//                "                        \"isExt\": true,\n" +
//                "                        \"extOpenMode\": 2,\n" +
//                "                        \"type\": 1,\n" +
//                "                        \"orderNo\": 4,\n" +
//                "                        \"show\": 1,\n" +
//                "                        \"activeMenu\": null,\n" +
//                "                        \"status\": 1,\n" +
//                "                        \"keepAlive\": 0\n" +
//                "                    }\n" +
//                "                },\n" +
//                "                {\n" +
//                "                    \"id\": 112,\n" +
//                "                    \"path\": \"https://antdv.com/components/overview-cn\",\n" +
//                "                    \"name\": \"antdv文档(内嵌)\",\n" +
//                "                    \"meta\": {\n" +
//                "                        \"title\": \"antdv文档(内嵌)\",\n" +
//                "                        \"icon\": \"\",\n" +
//                "                        \"isExt\": true,\n" +
//                "                        \"extOpenMode\": 2,\n" +
//                "                        \"type\": 1,\n" +
//                "                        \"orderNo\": 255,\n" +
//                "                        \"show\": 1,\n" +
//                "                        \"activeMenu\": null,\n" +
//                "                        \"status\": 1,\n" +
//                "                        \"keepAlive\": 0\n" +
//                "                    }\n" +
//                "                }\n" +
//                "            ]\n" +
//                "        },\n" +
//                "        {\n" +
//                "            \"id\": 1,\n" +
//                "            \"path\": \"/system\",\n" +
//                "            \"component\": \"\",\n" +
//                "            \"name\": \"系统管理\",\n" +
//                "            \"meta\": {\n" +
//                "                \"title\": \"系统管理\",\n" +
//                "                \"icon\": \"ant-design:setting-outlined\",\n" +
//                "                \"isExt\": false,\n" +
//                "                \"extOpenMode\": 1,\n" +
//                "                \"type\": 0,\n" +
//                "                \"orderNo\": 254,\n" +
//                "                \"show\": 1,\n" +
//                "                \"activeMenu\": null,\n" +
//                "                \"status\": 1,\n" +
//                "                \"keepAlive\": 0\n" +
//                "            },\n" +
//                "            \"redirect\": \"/system/user\",\n" +
//                "            \"children\": [\n" +
//                "                {\n" +
//                "                    \"id\": 2,\n" +
//                "                    \"path\": \"/system/user\",\n" +
//                "                    \"name\": \"用户管理\",\n" +
//                "                    \"component\": \"system/user/index\",\n" +
//                "                    \"meta\": {\n" +
//                "                        \"title\": \"用户管理\",\n" +
//                "                        \"icon\": \"ant-design:user-outlined\",\n" +
//                "                        \"isExt\": false,\n" +
//                "                        \"extOpenMode\": 1,\n" +
//                "                        \"type\": 1,\n" +
//                "                        \"orderNo\": 0,\n" +
//                "                        \"show\": 1,\n" +
//                "                        \"activeMenu\": null,\n" +
//                "                        \"status\": 1,\n" +
//                "                        \"keepAlive\": 0\n" +
//                "                    }\n" +
//                "                },\n" +
//                "                {\n" +
//                "                    \"id\": 3,\n" +
//                "                    \"path\": \"/system/role\",\n" +
//                "                    \"name\": \"角色管理\",\n" +
//                "                    \"component\": \"system/role/index\",\n" +
//                "                    \"meta\": {\n" +
//                "                        \"title\": \"角色管理\",\n" +
//                "                        \"icon\": \"ep:user\",\n" +
//                "                        \"isExt\": false,\n" +
//                "                        \"extOpenMode\": 1,\n" +
//                "                        \"type\": 1,\n" +
//                "                        \"orderNo\": 1,\n" +
//                "                        \"show\": 1,\n" +
//                "                        \"activeMenu\": null,\n" +
//                "                        \"status\": 1,\n" +
//                "                        \"keepAlive\": 0\n" +
//                "                    }\n" +
//                "                },\n" +
//                "                {\n" +
//                "                    \"id\": 4,\n" +
//                "                    \"path\": \"/system/menu\",\n" +
//                "                    \"name\": \"菜单管理\",\n" +
//                "                    \"component\": \"system/menu/index\",\n" +
//                "                    \"meta\": {\n" +
//                "                        \"title\": \"菜单管理\",\n" +
//                "                        \"icon\": \"ep:menu\",\n" +
//                "                        \"isExt\": false,\n" +
//                "                        \"extOpenMode\": 1,\n" +
//                "                        \"type\": 1,\n" +
//                "                        \"orderNo\": 2,\n" +
//                "                        \"show\": 1,\n" +
//                "                        \"activeMenu\": null,\n" +
//                "                        \"status\": 1,\n" +
//                "                        \"keepAlive\": 0\n" +
//                "                    }\n" +
//                "                },\n" +
//                "                {\n" +
//                "                    \"id\": 61,\n" +
//                "                    \"path\": \"/system/dept\",\n" +
//                "                    \"name\": \"部门管理\",\n" +
//                "                    \"component\": \"system/dept/index\",\n" +
//                "                    \"meta\": {\n" +
//                "                        \"title\": \"部门管理\",\n" +
//                "                        \"icon\": \"ant-design:deployment-unit-outlined\",\n" +
//                "                        \"isExt\": false,\n" +
//                "                        \"extOpenMode\": 1,\n" +
//                "                        \"type\": 1,\n" +
//                "                        \"orderNo\": 3,\n" +
//                "                        \"show\": 1,\n" +
//                "                        \"activeMenu\": null,\n" +
//                "                        \"status\": 1,\n" +
//                "                        \"keepAlive\": 0\n" +
//                "                    }\n" +
//                "                },\n" +
//                "                {\n" +
//                "                    \"id\": 56,\n" +
//                "                    \"path\": \"/system/dict-type\",\n" +
//                "                    \"name\": \"字典管理\",\n" +
//                "                    \"component\": \"system/dict-type/index\",\n" +
//                "                    \"meta\": {\n" +
//                "                        \"title\": \"字典管理\",\n" +
//                "                        \"icon\": \"ant-design:book-outlined\",\n" +
//                "                        \"isExt\": false,\n" +
//                "                        \"extOpenMode\": 1,\n" +
//                "                        \"type\": 1,\n" +
//                "                        \"orderNo\": 4,\n" +
//                "                        \"show\": 1,\n" +
//                "                        \"activeMenu\": null,\n" +
//                "                        \"status\": 1,\n" +
//                "                        \"keepAlive\": 0\n" +
//                "                    }\n" +
//                "                },\n" +
//                "                {\n" +
//                "                    \"id\": 5,\n" +
//                "                    \"path\": \"/system/monitor\",\n" +
//                "                    \"component\": \"\",\n" +
//                "                    \"name\": \"系统监控\",\n" +
//                "                    \"meta\": {\n" +
//                "                        \"title\": \"系统监控\",\n" +
//                "                        \"icon\": \"ep:monitor\",\n" +
//                "                        \"isExt\": false,\n" +
//                "                        \"extOpenMode\": 1,\n" +
//                "                        \"type\": 0,\n" +
//                "                        \"orderNo\": 5,\n" +
//                "                        \"show\": 1,\n" +
//                "                        \"activeMenu\": null,\n" +
//                "                        \"status\": 1,\n" +
//                "                        \"keepAlive\": 0\n" +
//                "                    },\n" +
//                "                    \"redirect\": \"/system/monitor/online\",\n" +
//                "                    \"children\": [\n" +
//                "                        {\n" +
//                "                            \"id\": 6,\n" +
//                "                            \"path\": \"/system/monitor/online\",\n" +
//                "                            \"name\": \"在线用户\",\n" +
//                "                            \"component\": \"system/monitor/online/index\",\n" +
//                "                            \"meta\": {\n" +
//                "                                \"title\": \"在线用户\",\n" +
//                "                                \"icon\": \"\",\n" +
//                "                                \"isExt\": false,\n" +
//                "                                \"extOpenMode\": 1,\n" +
//                "                                \"type\": 1,\n" +
//                "                                \"orderNo\": 0,\n" +
//                "                                \"show\": 1,\n" +
//                "                                \"activeMenu\": null,\n" +
//                "                                \"status\": 1,\n" +
//                "                                \"keepAlive\": 0\n" +
//                "                            }\n" +
//                "                        },\n" +
//                "                        {\n" +
//                "                            \"id\": 7,\n" +
//                "                            \"path\": \"/system/monitor/login-log\",\n" +
//                "                            \"name\": \"登录日志\",\n" +
//                "                            \"component\": \"system/monitor/log/login/index\",\n" +
//                "                            \"meta\": {\n" +
//                "                                \"title\": \"登录日志\",\n" +
//                "                                \"icon\": \"\",\n" +
//                "                                \"isExt\": false,\n" +
//                "                                \"extOpenMode\": 1,\n" +
//                "                                \"type\": 1,\n" +
//                "                                \"orderNo\": 0,\n" +
//                "                                \"show\": 1,\n" +
//                "                                \"activeMenu\": null,\n" +
//                "                                \"status\": 1,\n" +
//                "                                \"keepAlive\": 0\n" +
//                "                            }\n" +
//                "                        },\n" +
//                "                        {\n" +
//                "                            \"id\": 68,\n" +
//                "                            \"path\": \"/health\",\n" +
//                "                            \"name\": \"健康检查\",\n" +
//                "                            \"component\": \"\",\n" +
//                "                            \"meta\": {\n" +
//                "                                \"title\": \"健康检查\",\n" +
//                "                                \"icon\": \"\",\n" +
//                "                                \"isExt\": false,\n" +
//                "                                \"extOpenMode\": 1,\n" +
//                "                                \"type\": 1,\n" +
//                "                                \"orderNo\": 4,\n" +
//                "                                \"show\": 0,\n" +
//                "                                \"activeMenu\": null,\n" +
//                "                                \"status\": 1,\n" +
//                "                                \"keepAlive\": 0\n" +
//                "                            }\n" +
//                "                        },\n" +
//                "                        {\n" +
//                "                            \"id\": 8,\n" +
//                "                            \"path\": \"/system/monitor/serve\",\n" +
//                "                            \"name\": \"服务监控\",\n" +
//                "                            \"component\": \"system/monitor/serve/index\",\n" +
//                "                            \"meta\": {\n" +
//                "                                \"title\": \"服务监控\",\n" +
//                "                                \"icon\": \"\",\n" +
//                "                                \"isExt\": false,\n" +
//                "                                \"extOpenMode\": 1,\n" +
//                "                                \"type\": 1,\n" +
//                "                                \"orderNo\": 4,\n" +
//                "                                \"show\": 1,\n" +
//                "                                \"activeMenu\": null,\n" +
//                "                                \"status\": 1,\n" +
//                "                                \"keepAlive\": 0\n" +
//                "                            }\n" +
//                "                        }\n" +
//                "                    ]\n" +
//                "                },\n" +
//                "                {\n" +
//                "                    \"id\": 9,\n" +
//                "                    \"path\": \"/system/schedule\",\n" +
//                "                    \"component\": \"\",\n" +
//                "                    \"name\": \"任务调度\",\n" +
//                "                    \"meta\": {\n" +
//                "                        \"title\": \"任务调度\",\n" +
//                "                        \"icon\": \"ant-design:schedule-filled\",\n" +
//                "                        \"isExt\": false,\n" +
//                "                        \"extOpenMode\": 1,\n" +
//                "                        \"type\": 0,\n" +
//                "                        \"orderNo\": 6,\n" +
//                "                        \"show\": 1,\n" +
//                "                        \"activeMenu\": null,\n" +
//                "                        \"status\": 1,\n" +
//                "                        \"keepAlive\": 0\n" +
//                "                    },\n" +
//                "                    \"redirect\": \"/system/task\",\n" +
//                "                    \"children\": [\n" +
//                "                        {\n" +
//                "                            \"id\": 10,\n" +
//                "                            \"path\": \"/system/task\",\n" +
//                "                            \"name\": \"任务管理\",\n" +
//                "                            \"component\": \"system/schedule/task/index\",\n" +
//                "                            \"meta\": {\n" +
//                "                                \"title\": \"任务管理\",\n" +
//                "                                \"icon\": \"\",\n" +
//                "                                \"isExt\": false,\n" +
//                "                                \"extOpenMode\": 1,\n" +
//                "                                \"type\": 1,\n" +
//                "                                \"orderNo\": 0,\n" +
//                "                                \"show\": 1,\n" +
//                "                                \"activeMenu\": null,\n" +
//                "                                \"status\": 1,\n" +
//                "                                \"keepAlive\": 0\n" +
//                "                            }\n" +
//                "                        },\n" +
//                "                        {\n" +
//                "                            \"id\": 11,\n" +
//                "                            \"path\": \"/system/task/log\",\n" +
//                "                            \"name\": \"任务日志\",\n" +
//                "                            \"component\": \"system/schedule/log/index\",\n" +
//                "                            \"meta\": {\n" +
//                "                                \"title\": \"任务日志\",\n" +
//                "                                \"icon\": \"\",\n" +
//                "                                \"isExt\": false,\n" +
//                "                                \"extOpenMode\": 1,\n" +
//                "                                \"type\": 1,\n" +
//                "                                \"orderNo\": 0,\n" +
//                "                                \"show\": 1,\n" +
//                "                                \"activeMenu\": null,\n" +
//                "                                \"status\": 1,\n" +
//                "                                \"keepAlive\": 0\n" +
//                "                            }\n" +
//                "                        }\n" +
//                "                    ]\n" +
//                "                },\n" +
//                "                {\n" +
//                "                    \"id\": 107,\n" +
//                "                    \"path\": \"system/dict-item/:id\",\n" +
//                "                    \"name\": \"字典项管理\",\n" +
//                "                    \"component\": \"system/dict-item/index\",\n" +
//                "                    \"meta\": {\n" +
//                "                        \"title\": \"字典项管理\",\n" +
//                "                        \"icon\": \"ant-design:facebook-outlined\",\n" +
//                "                        \"isExt\": false,\n" +
//                "                        \"extOpenMode\": 1,\n" +
//                "                        \"type\": 1,\n" +
//                "                        \"orderNo\": 255,\n" +
//                "                        \"show\": 0,\n" +
//                "                        \"activeMenu\": \"/system/dict-type\",\n" +
//                "                        \"status\": 1,\n" +
//                "                        \"keepAlive\": 0\n" +
//                "                    }\n" +
//                "                },\n" +
//                "                {\n" +
//                "                    \"id\": 86,\n" +
//                "                    \"path\": \"/param-config\",\n" +
//                "                    \"name\": \"参数配置\",\n" +
//                "                    \"component\": \"system/param-config/index\",\n" +
//                "                    \"meta\": {\n" +
//                "                        \"title\": \"参数配置\",\n" +
//                "                        \"icon\": \"ep:edit\",\n" +
//                "                        \"isExt\": false,\n" +
//                "                        \"extOpenMode\": 1,\n" +
//                "                        \"type\": 1,\n" +
//                "                        \"orderNo\": 255,\n" +
//                "                        \"show\": 1,\n" +
//                "                        \"activeMenu\": null,\n" +
//                "                        \"status\": 1,\n" +
//                "                        \"keepAlive\": 0\n" +
//                "                    }\n" +
//                "                }\n" +
//                "            ]\n" +
//                "        },\n" +
//                "        {\n" +
//                "            \"id\": 48,\n" +
//                "            \"path\": \"/tool\",\n" +
//                "            \"component\": \"\",\n" +
//                "            \"name\": \"系统工具\",\n" +
//                "            \"meta\": {\n" +
//                "                \"title\": \"系统工具\",\n" +
//                "                \"icon\": \"ant-design:tool-outlined\",\n" +
//                "                \"isExt\": false,\n" +
//                "                \"extOpenMode\": 1,\n" +
//                "                \"type\": 0,\n" +
//                "                \"orderNo\": 254,\n" +
//                "                \"show\": 1,\n" +
//                "                \"activeMenu\": null,\n" +
//                "                \"status\": 1,\n" +
//                "                \"keepAlive\": 0\n" +
//                "            },\n" +
//                "            \"redirect\": \"/tool/email\",\n" +
//                "            \"children\": [\n" +
//                "                {\n" +
//                "                    \"id\": 49,\n" +
//                "                    \"path\": \"/tool/email\",\n" +
//                "                    \"name\": \"邮件工具\",\n" +
//                "                    \"component\": \"tool/email/index\",\n" +
//                "                    \"meta\": {\n" +
//                "                        \"title\": \"邮件工具\",\n" +
//                "                        \"icon\": \"ant-design:send-outlined\",\n" +
//                "                        \"isExt\": false,\n" +
//                "                        \"extOpenMode\": 1,\n" +
//                "                        \"type\": 1,\n" +
//                "                        \"orderNo\": 1,\n" +
//                "                        \"show\": 1,\n" +
//                "                        \"activeMenu\": null,\n" +
//                "                        \"status\": 1,\n" +
//                "                        \"keepAlive\": 0\n" +
//                "                    }\n" +
//                "                },\n" +
//                "                {\n" +
//                "                    \"id\": 51,\n" +
//                "                    \"path\": \"/tool/storage\",\n" +
//                "                    \"name\": \"存储管理\",\n" +
//                "                    \"component\": \"tool/storage/index\",\n" +
//                "                    \"meta\": {\n" +
//                "                        \"title\": \"存储管理\",\n" +
//                "                        \"icon\": \"ant-design:appstore-outlined\",\n" +
//                "                        \"isExt\": false,\n" +
//                "                        \"extOpenMode\": 1,\n" +
//                "                        \"type\": 1,\n" +
//                "                        \"orderNo\": 2,\n" +
//                "                        \"show\": 1,\n" +
//                "                        \"activeMenu\": null,\n" +
//                "                        \"status\": 1,\n" +
//                "                        \"keepAlive\": 0\n" +
//                "                    }\n" +
//                "                }\n" +
//                "            ]\n" +
//                "        },\n" +
//                "        {\n" +
//                "            \"id\": 115,\n" +
//                "            \"path\": \"netdisk\",\n" +
//                "            \"component\": null,\n" +
//                "            \"name\": \"网盘管理\",\n" +
//                "            \"meta\": {\n" +
//                "                \"title\": \"网盘管理\",\n" +
//                "                \"icon\": \"ant-design:cloud-server-outlined\",\n" +
//                "                \"isExt\": false,\n" +
//                "                \"extOpenMode\": 1,\n" +
//                "                \"type\": 0,\n" +
//                "                \"orderNo\": 255,\n" +
//                "                \"show\": 1,\n" +
//                "                \"activeMenu\": null,\n" +
//                "                \"status\": 1,\n" +
//                "                \"keepAlive\": 0\n" +
//                "            },\n" +
//                "            \"redirect\": \"/netdisk/manage\",\n" +
//                "            \"children\": [\n" +
//                "                {\n" +
//                "                    \"id\": 116,\n" +
//                "                    \"path\": \"manage\",\n" +
//                "                    \"name\": \"文件管理\",\n" +
//                "                    \"component\": \"netdisk/manage\",\n" +
//                "                    \"meta\": {\n" +
//                "                        \"title\": \"文件管理\",\n" +
//                "                        \"icon\": \"\",\n" +
//                "                        \"isExt\": false,\n" +
//                "                        \"extOpenMode\": 1,\n" +
//                "                        \"type\": 1,\n" +
//                "                        \"orderNo\": 252,\n" +
//                "                        \"show\": 1,\n" +
//                "                        \"activeMenu\": null,\n" +
//                "                        \"status\": 1,\n" +
//                "                        \"keepAlive\": 0\n" +
//                "                    }\n" +
//                "                },\n" +
//                "                {\n" +
//                "                    \"id\": 127,\n" +
//                "                    \"path\": \"overview\",\n" +
//                "                    \"name\": \"网盘概览\",\n" +
//                "                    \"component\": \"netdisk/overview\",\n" +
//                "                    \"meta\": {\n" +
//                "                        \"title\": \"网盘概览\",\n" +
//                "                        \"icon\": \"\",\n" +
//                "                        \"isExt\": false,\n" +
//                "                        \"extOpenMode\": 1,\n" +
//                "                        \"type\": 1,\n" +
//                "                        \"orderNo\": 254,\n" +
//                "                        \"show\": 1,\n" +
//                "                        \"activeMenu\": null,\n" +
//                "                        \"status\": 1,\n" +
//                "                        \"keepAlive\": 0\n" +
//                "                    }\n" +
//                "                }\n" +
//                "            ]\n" +
//                "        },\n" +
//                "        {\n" +
//                "            \"id\": 43,\n" +
//                "            \"path\": \"/about\",\n" +
//                "            \"name\": \"关于\",\n" +
//                "            \"component\": \"account/about\",\n" +
//                "            \"meta\": {\n" +
//                "                \"title\": \"关于\",\n" +
//                "                \"icon\": \"ant-design:info-circle-outlined\",\n" +
//                "                \"isExt\": false,\n" +
//                "                \"extOpenMode\": 1,\n" +
//                "                \"type\": 1,\n" +
//                "                \"orderNo\": 260,\n" +
//                "                \"show\": 1,\n" +
//                "                \"activeMenu\": null,\n" +
//                "                \"status\": 1,\n" +
//                "                \"keepAlive\": 0\n" +
//                "            }\n" +
//                "        }\n" +
//                "    ]"));
//    }


    /**
     * 获取当前登录用户权限
     */
    @GetMapping("/permissions")
    public Response<List<String>> permissions() {
        return Response.success(this.accountService.permissions());
    }

}
