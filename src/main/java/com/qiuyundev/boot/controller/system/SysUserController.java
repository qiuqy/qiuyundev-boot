package com.qiuyundev.boot.controller.system;

import com.qiuyundev.boot.base.vo.PageResult;
import com.qiuyundev.boot.base.vo.Response;
import com.qiuyundev.boot.base.validation.ParamValidationGroup;
import com.qiuyundev.boot.domain.vo.user.UserFormReq;
import com.qiuyundev.boot.domain.vo.user.UserPageReq;
import com.qiuyundev.boot.domain.vo.user.UserResp;
import com.qiuyundev.boot.service.ISysUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 用户 前端控制器
 * </p>
 *
 * @author Qiuyun
 * @since 2024/04/12
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/system/user")
public class SysUserController {
    private final ISysUserService userService;

    /**
     * 分页查询用户
     *
     * @param userPageReq 用户分页查询参数
     * @return 用户列表
     */
    @GetMapping("page")
    @PreAuthorize("hasAuthority('system:user:list')")
    public Response<PageResult<UserResp>> page(UserPageReq userPageReq) {
        return Response.success(this.userService.page(userPageReq));
    }

    /**
     * 用户详情
     *
     * @param id 用户 ID
     * @return 用户详情
     */
    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('system:user:detail')")
    public Response<UserResp> detail(@PathVariable("id") Integer id) {
        return Response.success(this.userService.detail(id));
    }

    /**
     * 新增用户
     *
     * @param userFormReq 用户新增参数
     * @return 用户新增结果
     */
    @PostMapping
    @PreAuthorize("hasAuthority('system:user:add')")
    public Response<Boolean> add(@RequestBody @Validated(value = ParamValidationGroup.Add.class) UserFormReq userFormReq) {
        return Response.success(this.userService.add(userFormReq));
    }

    /**
     * 修改用户
     *
     * @param id          用户 ID
     * @param userFormReq 用户修改参数
     * @return 用户修改结果
     */
    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('system:user:update')")
    public Response<Boolean> update(@PathVariable("id") Integer id, @RequestBody @Validated(value = ParamValidationGroup.Update.class)  UserFormReq userFormReq) {
        userFormReq.setId(id);
        return Response.success(this.userService.update(userFormReq));
    }


    /**
     * 删除用户
     *
     * @param ids 用户 ID 列表
     * @return 用户删除结果
     */
    @DeleteMapping("/{ids}")
    @PreAuthorize("hasAuthority('system:user:delete')")
    public Response<Boolean> delete(@PathVariable("ids") Long[] ids) {
        return Response.success(this.userService.delete(ids));
    }
}
