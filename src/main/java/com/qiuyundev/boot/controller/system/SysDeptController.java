package com.qiuyundev.boot.controller.system;

import com.qiuyundev.boot.base.vo.Response;
import com.qiuyundev.boot.domain.vo.TreeVO;
import com.qiuyundev.boot.domain.vo.dept.DeptFormReq;
import com.qiuyundev.boot.domain.vo.dept.DeptListReq;
import com.qiuyundev.boot.domain.vo.dept.DeptResp;
import com.qiuyundev.boot.service.ISysDeptService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 部门前端 控制器
 *
 * @author Qiuyun
 * @since 2024/04/19
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/system/dept")
public class SysDeptController {
    private final ISysDeptService deptService;

    /**
     * 查询部门列表
     *
     * @param deptListReq 部门列表数据
     * @return 部门列表
     */
    @GetMapping("/list")
    @PreAuthorize("hasAuthority('system:dept:list')")
    public Response<List<TreeVO.TreeResp>> list(DeptListReq deptListReq) {
        return Response.success(this.deptService.list(deptListReq));
    }

    /**
     * 部门详情
     *
     * @param id 部门 ID
     * @return 部门详情
     */
    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('system:dept:detail')")
    public Response<DeptResp> detail(@PathVariable Integer id) {
        return Response.success(this.deptService.detail(id));
    }

    /**
     * 新增部门
     *
     * @param deptFormReq 部门新增参数
     * @return 部门新增结果
     */
    @PostMapping
    @PreAuthorize("hasAuthority('system:dept:add')")
    public Response<Boolean> add(@RequestBody @Validated DeptFormReq deptFormReq) {
        return Response.success(this.deptService.add(deptFormReq));
    }

    /**
     * 修改部门
     *
     * @param id          部门 ID
     * @param deptFormReq 部门修改参数
     * @return 部门修改结果
     */
    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('system:dept:update')")
    public Response<Boolean> update(@PathVariable("id") Integer id, @RequestBody @Validated DeptFormReq deptFormReq) {
        deptFormReq.setId(id);
        return Response.success(this.deptService.update(deptFormReq));
    }


    /**
     * 删除部门
     *
     * @param ids 部门 ID 列表
     * @return 部门删除结果
     */
    @DeleteMapping("/{ids}")
    @PreAuthorize("hasAuthority('system:dept:delete')")
    public Response<Boolean> delete(@PathVariable("ids") Long[] ids) {
        return Response.success(this.deptService.delete(ids));
    }


    /**
     * 查询部门树状
     *
     * @param deptPageReq 部门分页查询参数
     * @return 部门列表
     */
    @GetMapping("tree")
    public Response<List<TreeVO.TreeResp>> tree(TreeVO.TreeReq deptPageReq) {
        return Response.success(this.deptService.tree(deptPageReq));
    }
}
