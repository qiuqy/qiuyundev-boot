package com.qiuyundev.boot.controller.tool;


import com.qiuyundev.boot.base.vo.ApiResult;
import com.qiuyundev.boot.base.vo.Response;
import com.qiuyundev.boot.core.email.*;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;

@RestController
@RequestMapping("/system/mail")
@RequiredArgsConstructor
public class MailController {
    private final MailService mailService;

    /**
     * 发送简单邮件
     *
     * @param simpleMail 简单邮件
     * @return {@link ApiResult}
     */
    @PostMapping("/send")
    public Response<Void> send(@RequestBody SimpleMail simpleMail) {
        Mail mail = new Mail();
        mail.setTo(new String[]{simpleMail.getTo()});
        mail.setSubject(simpleMail.getSubject());
        mail.setText(simpleMail.getContent());
        if (this.mailService.sendSimpleMailMessage(mail).isSuccess()) {
            return Response.success();
        }
        return Response.failed();
    }


    /**
     * 发送简单邮件
     *
     * @param mail 简单邮件
     * @return {@link ApiResult}
     */
    @PostMapping("/sendSimpleMailMessage")
    public ApiResult sendSimpleMailMessage(@RequestBody Mail mail) {
        return this.mailService.sendSimpleMailMessage(mail);
    }

    /**
     * 发送带有附件邮件 - 测试
     *
     * @param mail 邮件
     * @return {@link ApiResult}
     */
    @PostMapping("/sendMailWithFile")
    public ApiResult sendMailWithFile(@RequestBody MailWithFile mail) {
        File file = new File("C:\\Users\\97208\\Desktop\\测试.pdf");
        mail.setFile(file);
        return this.mailService.sendMailWithFile(mail);
    }

    /**
     * 发送模板邮件
     *
     * @param mail 邮件
     * @return {@link ApiResult}
     */
    @PostMapping("/sendTemplateMail")
    public ApiResult sendTemplateMail(@RequestBody TemplateMail mail) {
        return this.mailService.sendTemplateMail(mail);
    }

}
