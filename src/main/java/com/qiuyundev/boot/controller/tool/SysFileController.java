package com.qiuyundev.boot.controller.tool;

import com.qiuyundev.boot.base.vo.PageResult;
import com.qiuyundev.boot.base.vo.Response;
import com.qiuyundev.boot.domain.vo.File;
import com.qiuyundev.boot.domain.vo.file.FilePageReq;
import com.qiuyundev.boot.domain.vo.file.FileResp;
import com.qiuyundev.boot.service.ISysFileService;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 * 文件 前端控制器
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-20
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/system/file")
public class SysFileController {
    private final ISysFileService service;


    @PostMapping("/upload")
    @PreAuthorize("hasAuthority('tool:storage:upload')")
    public Response<File> uploadFile(@RequestParam("file") MultipartFile file) {
        return Response.success(File.builder().path(this.service.uploadFile(file)).build());
    }

    @GetMapping("/preview/{id:.+}")
    public void previewFile(@PathVariable("id") Integer id, HttpServletResponse response) {
        this.service.previewFile(id, response);
    }

    /**
     * 分页查询文件
     *
     * @param pageReq 文件分页查询参数
     * @return 文件列表
     */
    @GetMapping("/page")
    @PreAuthorize("hasAuthority('tool:storage:list')")
    public Response<PageResult<FileResp>> page(FilePageReq pageReq) {
        return Response.success(this.service.page(pageReq));
    }

//    /**
//     * 文件详情
//     *
//     * @param id 文件 ID
//     * @return 文件详情
//     */
//    @GetMapping("/{id}")
//    public Response<FileResp> detail(@PathVariable("id") Integer id) {
//        return Response.success(this.service.detail(id));
//    }
//
//    /**
//     * 新增文件
//     *
//     * @param formReq 文件新增参数
//     * @return 文件新增结果
//     */
//    @PostMapping
//    public Response<Boolean> add(@RequestBody FileFormReq formReq) {
//        return Response.success(this.service.add(formReq));
//    }
//
//    /**
//     * 修改文件
//     *
//     * @param id      文件 ID
//     * @param formReq 文件修改参数
//     * @return 文件修改结果
//     */
//    @PutMapping("/{id}")
//    public Response<Boolean> update(@PathVariable("id") Integer id, @RequestBody FileFormReq formReq) {
//        formReq.setId(id);
//        return Response.success(this.service.update(formReq));
//    }


    /**
     * 删除文件
     *
     * @param ids 文件 ID 列表
     * @return 文件删除结果
     */
    @DeleteMapping("/{ids}")
    @PreAuthorize("hasAuthority('tool:storage:delete')")
    public Response<Boolean> delete(@PathVariable("ids") Long[] ids) {
        return Response.success(this.service.delete(ids));
    }

}
