package com.qiuyundev.boot.controller.tool;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qiuyundev.boot.base.vo.PageResult;
import com.qiuyundev.boot.base.vo.Response;
import com.qiuyundev.boot.core.db.domain.TableColumnInfo;
import com.qiuyundev.boot.core.db.domain.TableInfo;
import com.qiuyundev.boot.core.generator.EnhanceFreemarkerTemplateEngine;
import com.qiuyundev.boot.core.generator.GeneratorService;
import com.qiuyundev.boot.domain.vo.generator.CodeGeneratorReq;
import com.qiuyundev.boot.domain.vo.generator.PreviewResp;
import com.qiuyundev.boot.domain.vo.generator.TableColumnInfoReq;
import com.qiuyundev.boot.domain.vo.generator.TableColumnInfoResp;
import com.qiuyundev.boot.service.ICodeGeneratorService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/system/generator")
@RestController
@RequiredArgsConstructor
public class CodeGeneratorController {
    private final ICodeGeneratorService codeGeneratorService;
    private final GeneratorService generatorService;

    @GetMapping("/page")
    @PreAuthorize("hasAuthority('tool:generator:list')")
    public Response<PageResult<TableInfo>> selectTables(CodeGeneratorReq codeGeneratorReq) {
        // 使用 PageHelper
        PageInfo<TableInfo> pageInfo = PageHelper.startPage(codeGeneratorReq.getPage(), codeGeneratorReq.getPageSize()).doSelectPageInfo(() -> {
            codeGeneratorService.selectTables(codeGeneratorReq.getTableName());
        });
        return Response.success(PageResult.of(pageInfo));
    }

    @GetMapping("/tableColumns")
    public Response<List<TableColumnInfoResp>> selectTableColumns(@RequestParam("tableName") String tableName) {
        return Response.success(this.codeGeneratorService.selectTableColumns(tableName));
    }

    @PostMapping("/saveOrUpdateConfig")
    public Response<Boolean> saveOrUpdateConfig(@RequestBody List<TableColumnInfoReq> list) {
        return Response.success(this.codeGeneratorService.saveOrUpdateConfig(list));
    }


    @GetMapping("/preview")
    @PreAuthorize("hasAuthority('tool:generator:preview')")
    public Response<PreviewResp> preview(@RequestParam("tableName") String tableName) {
        try {
            generatorService.codeGenerator(true, tableName);
            return Response.success(PreviewResp.of(EnhanceFreemarkerTemplateEngine.CodeGeneratorContextHolder.getContext()));
        } finally {
            EnhanceFreemarkerTemplateEngine.CodeGeneratorContextHolder.clearContext();
        }
    }
}
