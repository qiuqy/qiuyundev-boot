package com.qiuyundev.boot.controller.sample;

import com.qiuyundev.boot.domain.entity.SysUser;
import com.qiuyundev.boot.service.IDataScopeDemoService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping("/sample/dataScope")
public class DataScopeDemoController {

    private final IDataScopeDemoService dataScopeDemoService;


    /**
     * 自定义数据权限演示
     * <p> 需要修改角色里面的数据范围为 自定义数据权限演示, 然后重新登录后再调用此接口，
     * 以下的接口同理。
     */
    @GetMapping("/custom")
    public SysUser custom(@RequestParam("id") Integer id) {
        return this.dataScopeDemoService.custom(id);
    }

    /**
     * 本人部门数据权限演示
     */
    @GetMapping("/selfDept")
    public List<SysUser> selfDept(@RequestParam("id") Integer id) {
        return this.dataScopeDemoService.selfDept(id);
    }

    /**
     * 本人部门及下级部门数据权限演示
     */
    @GetMapping("/selfDeptAndChildren")
    public List<SysUser> selfDeptAndChildren(@RequestParam("id") Integer id) {
        return this.dataScopeDemoService.selfDeptAndChildren(Collections.singletonList(id));
    }

    /**
     * 仅本人数据权限
     */
    @GetMapping("/self")
    public List<Map<String, Object>> self() {
        return this.dataScopeDemoService.dataScopeDemoSelf();
    }
}
