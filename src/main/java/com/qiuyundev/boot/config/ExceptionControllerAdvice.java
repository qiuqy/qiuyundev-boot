package com.qiuyundev.boot.config;

import com.qiuyundev.boot.base.enums.response.ResponseEnum;
import com.qiuyundev.boot.base.exception.BusinessException;
import com.qiuyundev.boot.base.vo.Response;
import jakarta.validation.ConstraintViolationException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.stream.Collectors;

/**
 * 统一异常处理
 *
 * @author Qiuyun
 * @since 2024/04/12
 */
@RestControllerAdvice
public class ExceptionControllerAdvice {

    /**
     * {@code @RequestBody @RequestParam} 参数校验不通过时抛出的异常处理
     */
    @ExceptionHandler({BindException.class, ConstraintViolationException.class})
    public Response<?> methodArgumentNotValidExceptionHandler(Exception e) {
        if (e instanceof BindException) {
            BindingResult bindingResult = ((BindException) e).getBindingResult();
            String message = bindingResult.getFieldErrors()
                    .stream()
                    .map(error -> error.getField() + ": " + error.getDefaultMessage())
                    .collect(Collectors.joining(","));

            return Response.failed(ResponseEnum.BAD_REQUEST.getCode(), message);
        }

        return Response.failed(500, e.getMessage());
    }

    /**
     * 业务异常的统一处理
     */
    @ExceptionHandler({BusinessException.class})
    public Response<?> businessExceptionHandler(BusinessException e) {
        return Response.failed(e.getCode(), e.getMessage());
    }

    /**
     * 坏凭证异常处理程序
     *
     * @param e e
     * @return {@link Response}<{@link ?}>
     */
    @ExceptionHandler({BadCredentialsException.class})
    public Response<?> badCredentialsExceptionHandler(BadCredentialsException e) {
        return Response.failed(500, e.getMessage());
    }

    /**
     * 顶级异常捕获并统一处理，当其他异常无法处理时候选择使用
     */
    @ExceptionHandler({Throwable.class})
    public Response<?> handle(Throwable e) {
        e.printStackTrace();
        return Response.failed(500, e.getMessage());
    }

}
