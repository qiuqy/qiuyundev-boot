package com.qiuyundev.boot.config;


import com.qiuyundev.boot.core.security.DatabaseUserDetailService;
import com.qiuyundev.boot.core.security.exception.RestAccessDeniedHandler;
import com.qiuyundev.boot.core.security.exception.RestAuthenticationEntryPoint;
import com.qiuyundev.boot.core.security.filter.CaptchaValidationFilter;
import com.qiuyundev.boot.core.security.filter.JwtAuthenticationTokenFilter;
import com.qiuyundev.boot.core.security.handler.RestAuthenticationFailureHandler;
import com.qiuyundev.boot.core.security.handler.RestAuthenticationSuccessHandler;
import com.qiuyundev.boot.core.security.handler.RestLogoutHandler;
import com.qiuyundev.boot.core.security.handler.RestLogoutSuccessHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.logout.LogoutFilter;

import static com.qiuyundev.boot.core.security.SecurityConstants.LOGIN_PROCESSING_URL;
import static com.qiuyundev.boot.core.security.SecurityConstants.LOGOUT_URL;

/**
 * 安全配置
 *
 * @author Qiuyun
 * @since 2024/04/17
 */
@Configuration
@EnableWebSecurity // springboot 项目此注解可以省略，原因参考：SpringBootWebSecurityConfiguration.WebSecurityEnablerConfiguration
@EnableMethodSecurity
@RequiredArgsConstructor
public class WebSecurityConfig {
    private final RestAuthenticationSuccessHandler restAuthenticationSuccessHandler;
    private final RestAuthenticationFailureHandler restAuthenticationFailureHandler;
    private final RestAuthenticationEntryPoint restAuthenticationEntryPoint;
    private final RestAccessDeniedHandler restAccessDeniedHandler;
    private final RestLogoutHandler restLogoutHandler;
    private final RestLogoutSuccessHandler restLogoutSuccessHandler;
    private final DatabaseUserDetailService userDetailsService;
    private final JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter;
    private final CaptchaValidationFilter captchaValidationFilter;


    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        return http.authorizeHttpRequests((customizer) ->
                        customizer.requestMatchers(
                                        // 获取验证码接口
                                        "/auth/captcha/image",
                                        "/test/**",
                                        // 文件静态资源
                                        "/file/**"
                                )
                                .permitAll()
                                .anyRequest()
                                .authenticated())
                .formLogin(customize ->
                        // 自定义登录地址
                        customize.loginProcessingUrl(LOGIN_PROCESSING_URL)
                                // 身份验证成功处理
                                .successHandler(restAuthenticationSuccessHandler)
                                // 身份验证失败处理
                                .failureHandler(restAuthenticationFailureHandler))
                .logout(customize -> customize.logoutUrl(LOGOUT_URL)
                        .addLogoutHandler(restLogoutHandler)
                        .logoutSuccessHandler(restLogoutSuccessHandler)
                )
//                .oauth2Login(oauth2LoginCustomizer ->
//                        oauth2LoginCustomizer.userInfoEndpoint(userInfoEndpointCustomizer ->
//                                userInfoEndpointCustomizer.userService(userDetailsService))  )
                // 不关闭 csrf，logout 无法成功
                .csrf(AbstractHttpConfigurer::disable)
                // 使用 JWT， 关闭默认的 Session 会话机制，这样不会产生 Cookie
                .sessionManagement(customizer -> customizer.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                // 异常处理
                .exceptionHandling(customizer ->
                        // 未认证的处理
                        customizer.authenticationEntryPoint(restAuthenticationEntryPoint)
                                // 拒绝访问处理器
                                .accessDeniedHandler(restAccessDeniedHandler)
                )
                .userDetailsService(userDetailsService)
                // 过滤器有哪些以及顺序如何可以参考 DefaultSecurityFilterChain
                // 验证码过滤器
                .addFilterBefore(this.captchaValidationFilter, LogoutFilter.class)
                // token 过滤器
                .addFilterBefore(this.jwtAuthenticationTokenFilter, LogoutFilter.class)
                .build();
    }


}
