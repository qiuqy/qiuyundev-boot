package com.qiuyundev.boot.config.prpperties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Jwt参数
 *
 * @author Qiuyun
 * @since 2024/04/17
 */
@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "jwt")
public class JwtProperties {
    /**
     * 签发者
     */
    private String issuer = "qiuyundev";

    /**
     * 密钥
     */
    private String secret = "qiuyundev.com/qiuyundev.com/qiuyundev.com";

    /**
     * token有效时间（毫秒）
     */
    private Long ttl = 30 * 60 * 1000L;

}
