package com.qiuyundev.boot.config.prpperties;


import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 验证码
 *
 * @author Qiuyun
 * @since 2024/04/17
 */
@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "captcha")
public class CaptchaProperties {

    /**
     * 是否启用验证码
     */
    private boolean enabled;
}
