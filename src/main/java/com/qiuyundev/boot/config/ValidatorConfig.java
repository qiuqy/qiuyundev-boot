package com.qiuyundev.boot.config;

import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.hibernate.validator.HibernateValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 参数校验配置
 *
 * @author Qiuyun
 * @since 2024/04/17
 */
@Configuration
public class ValidatorConfig {
    /**
     * 配置 Validator的 fail_fast.
     *
     * @return Validator
     */
    @Bean
    public Validator validator() {
        Validator validator;
        try (ValidatorFactory validatorFactory = Validation.byProvider(HibernateValidator.class)
                .configure()
                .failFast(true)
                .buildValidatorFactory()) {
            validator = validatorFactory.getValidator();
        }
        return validator;
    }

}
