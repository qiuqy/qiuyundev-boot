package com.qiuyundev.boot.config.thread;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * 线程池配置
 *
 * @author Qiuyun
 * @since 2024/04/12
 */
@Configuration
@RequiredArgsConstructor
public class ThreadPoolConfig {

    /**
     * 核心线程数 = cpu核心数 + 1
     */
    private static final int CORE_SIZE = Runtime.getRuntime().availableProcessors() + 1;

    /**
     * 线程的名称前缀
     */
    private static final String EXECUTOR_NAME_PREFIX = "thread-pool";

    /**
     * 线程池属性
     */
    private final ThreadPoolProperties threadPoolProperties;

    /**
     * 线程池配置
     *
     * @return {@link ThreadPoolTaskExecutor}
     */
    @Bean(name = "threadPoolTaskExecutor")
    @ConditionalOnProperty(prefix = "thread-pool", name = "enabled", havingValue = "true")
    public ThreadPoolTaskExecutor threadPoolTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        // 配置核心线程数
        executor.setCorePoolSize(CORE_SIZE);
        // 配置最大线程数
        executor.setMaxPoolSize(CORE_SIZE * 2);
        // 配置队列大小
        executor.setQueueCapacity(threadPoolProperties.getQueueCapacity());
        // 线程的名称前缀
        executor.setThreadNamePrefix(EXECUTOR_NAME_PREFIX + "-");
        // 线程活跃时间（秒）
        executor.setKeepAliveSeconds(threadPoolProperties.getKeepAliveSeconds());
        // 等待所有任务结束后再关闭线程池
        executor.setWaitForTasksToCompleteOnShutdown(true);
        /*
         * 拒绝策略
         * 1. ThreadPoolExecutor.AbortPolicy 丢弃任务并抛出RejectedExecutionException异常(默认)。
         * 2. ThreadPoolExecutor.DiscardPolicy 丢弃任务，但是不抛出异常。
         * 3. ThreadPoolExecutor.DiscardOldestPolicy 丢弃队列最前面的任务，然后重新尝试执行任务
         * 4. ThreadPoolExecutor.CallerRunsPolicy 由调用线程处理该任务
         */
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.AbortPolicy());
        // 执行初始化
        executor.initialize();
        return executor;
    }

}
