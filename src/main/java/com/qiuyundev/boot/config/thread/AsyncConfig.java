package com.qiuyundev.boot.config.thread;

import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;


/**
 * 异步任务线程池配置
 *
 * @author Qiuyun
 * @since 2024/04/12
 */
@EnableAsync
@Configuration
public class AsyncConfig implements AsyncConfigurer {
    /**
     * 核心线程数 = cpu核心数 + 1
     */
    private static final int CORE_SIZE = Runtime.getRuntime().availableProcessors() + 1;

    /**
     * 定时任务线程的名称前缀
     */
    private static final String ASYNC_EXECUTOR_NAME_PREFIX = "async-pool";


    /**
     * 异步任务线程池配置
     */
    @Override
    public Executor getAsyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        // 配置核心线程数
        executor.setCorePoolSize(CORE_SIZE);
        // 配置最大线程数
        executor.setMaxPoolSize(CORE_SIZE * 2);
        // 配置队列大小
        executor.setQueueCapacity(1000);
        // 线程的名称前缀
        executor.setThreadNamePrefix(ASYNC_EXECUTOR_NAME_PREFIX);
        // 线程活跃时间（秒）
        executor.setKeepAliveSeconds(60);
        // 等待所有任务结束后再关闭线程池
        executor.setWaitForTasksToCompleteOnShutdown(true);
        /*
         * 拒绝策略
         * 1. ThreadPoolExecutor.AbortPolicy 丢弃任务并抛出RejectedExecutionException异常(默认)。
         * 2. ThreadPoolExecutor.DiscardPolicy 丢弃任务，但是不抛出异常。
         * 3. ThreadPoolExecutor.DiscardOldestPolicy 丢弃队列最前面的任务，然后重新尝试执行任务
         * 4. ThreadPoolExecutor.CallerRunsPolicy 由调用线程处理该任务
         */
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.AbortPolicy());
        // 执行初始化
        executor.initialize();
        return executor;
    }

    /**
     * 异步未捕获的异常处理程序
     */
    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return new AsyncExceptionHandler();
    }

    /**
     * 异步任务的异常处理
     */
    @Slf4j
    public static class AsyncExceptionHandler implements AsyncUncaughtExceptionHandler {

        @Override
        public void handleUncaughtException(Throwable ex, Method method, Object... params) {
            log.error("调用异步方法发生异常：" + method + "[" + Arrays.toString(params) + "]", ex);
        }
    }
}
