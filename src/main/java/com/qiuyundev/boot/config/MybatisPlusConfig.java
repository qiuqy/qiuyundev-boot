package com.qiuyundev.boot.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.DataPermissionInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.qiuyundev.boot.base.util.SpringUtils;
import com.qiuyundev.boot.core.datascope.MyBatisPlusDataPermissionHandler;
import com.qiuyundev.boot.core.datascope.MyBatisPlusDataPermissionInterceptor;
import com.qiuyundev.boot.core.mybatisplus.DefaultMetaObjectHandler;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * mybatis plus 配置
 *
 * @author Qiuyun
 * @since 2024/04/12
 */
@Configuration
// 扫描 Mapper 文件夹
@MapperScan("com.qiuyundev.boot.mapper")
//  EnableTransactionManagement 是一个 Spring 框架中的注解，用于开启事务管理。
// 当设置为 proxyTargetClass = true 时，表示使用 CGLIB 代理来创建代理对象，而不是默认的基于接口的 JDK 动态代理。
// 这样可以确保在没有实现接口的情况下，也能为类生成代理对象。
@EnableTransactionManagement(proxyTargetClass = true)
public class MybatisPlusConfig {


    /**
     * 拦截器
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        // 数据权限插件
        DataPermissionInterceptor dataPermissionInterceptor = new MyBatisPlusDataPermissionInterceptor();
        dataPermissionInterceptor.setDataPermissionHandler(new MyBatisPlusDataPermissionHandler());
        interceptor.addInnerInterceptor(dataPermissionInterceptor);
        // 分页插件
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
        return interceptor;
    }

    /**
     * 元对象字段填充控制器
     */
    @Bean
    public MetaObjectHandler metaObjectHandler() {
        return new DefaultMetaObjectHandler();
    }

}
