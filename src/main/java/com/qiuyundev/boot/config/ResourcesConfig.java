package com.qiuyundev.boot.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 静态资源配置
 * <p> 未做权限控制，注意文件安全
 */
@Configuration
public class ResourcesConfig implements WebMvcConfigurer {

    @Value("${file.upload.dir}")
    private String uploadDir;

    /**
     * 本地文件的静态资源的映射
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        // 本地文件的静态资源的映射规则
        registry.addResourceHandler("/file/**")
                .addResourceLocations("file:" + this.getUploadDir());
    }

    public String getUploadDir() {
        if (this.uploadDir.startsWith("/")) {
            return uploadDir + "/";
        }
        return "/" + uploadDir + "/";
    }
}
