package com.qiuyundev.boot.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.qiuyundev.boot.base.vo.PageResult;
import com.qiuyundev.boot.domain.entity.SysRole;
import com.qiuyundev.boot.domain.vo.role.RoleBriefResp;
import com.qiuyundev.boot.domain.vo.role.RoleFormReq;
import com.qiuyundev.boot.domain.vo.role.RolePageReq;
import com.qiuyundev.boot.domain.vo.role.RoleResp;

import java.util.List;

/**
 * <p>
 * 角色 服务类
 * </p>
 *
 * @author Qiuyun
 * @since 2024/04/12
 */
public interface ISysRoleService extends IService<SysRole> {
    List<SysRole> findByUserId(Integer userId);


    /**
     * 分页查询角色
     *
     * @param rolePageReq 角色分页查询参数
     * @return 角色列表
     */
    PageResult<RoleResp> page(RolePageReq rolePageReq);

    /**
     * 角色详情
     *
     * @param id 角色 ID
     * @return 角色详情
     */
    RoleResp detail(Integer id);

    /**
     * 新增角色
     *
     * @param roleFormReq 角色新增参数
     * @return 角色新增结果
     */
    Boolean add(RoleFormReq roleFormReq);

    /**
     * 修改角色
     *
     * @param roleFormReq 角色修改参数
     * @return 角色修改结果
     */
    Boolean update(RoleFormReq roleFormReq);


    /**
     * 删除角色
     *
     * @param ids 角色 ID 列表
     * @return 角色删除结果
     */
    Boolean delete(Long[] ids);

    List<RoleBriefResp> options();
}
