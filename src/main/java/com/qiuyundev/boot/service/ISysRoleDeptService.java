package com.qiuyundev.boot.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.qiuyundev.boot.domain.entity.SysRoleDept;

import java.util.List;

/**
 * <p>
 * 角色部门 服务接口
 * </p>
 *
 * @author Qiuyun
 * @since 2024/04/11
 */
public interface ISysRoleDeptService extends IService<SysRoleDept> {

    boolean saveRoleDeptList(List<Integer> deptIds, Integer roleId, boolean updateFlag);
}
