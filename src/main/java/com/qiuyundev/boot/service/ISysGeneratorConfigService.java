package com.qiuyundev.boot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qiuyundev.boot.domain.entity.SysGeneratorConfig;

/**
 * <p>
 * 代码生成器配置 服务类
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-31
 */
public interface ISysGeneratorConfigService extends IService<SysGeneratorConfig> {

}
