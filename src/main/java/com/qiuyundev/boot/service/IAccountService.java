package com.qiuyundev.boot.service;

import com.qiuyundev.boot.domain.vo.account.AccountMenuResp;

import java.util.List;

/**
 * <p>
 * 账户 服务接口
 * </p>
 *
 * @author Qiuyun
 * @since 2024/04/18
 */
public interface IAccountService {

    List<AccountMenuResp> menus();

    List<String> permissions();
}
