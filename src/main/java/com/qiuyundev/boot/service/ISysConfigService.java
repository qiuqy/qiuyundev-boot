package com.qiuyundev.boot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qiuyundev.boot.base.vo.PageResult;
import com.qiuyundev.boot.domain.entity.SysConfig;
import com.qiuyundev.boot.domain.vo.config.ConfigFormReq;
import com.qiuyundev.boot.domain.vo.config.ConfigPageReq;
import com.qiuyundev.boot.domain.vo.config.ConfigResp;

/**
 * <p>
 * 参数配置 服务类
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-06
 */
public interface ISysConfigService extends IService<SysConfig> {
    /**
     * 分页查询参数配置
     *
     * @param pageReq 参数配置分页查询参数
     * @return 参数配置列表
     */
    PageResult<ConfigResp> page(ConfigPageReq pageReq);

    /**
     * 参数配置详情
     *
     * @param id 参数配置 ID
     * @return 参数配置详情
     */
    ConfigResp detail(Integer id);

    /**
     * 新增参数配置
     *
     * @param formReq 参数配置新增参数
     * @return 参数配置新增结果
     */
    Boolean add(ConfigFormReq formReq);

    /**
     * 修改参数配置
     *
     * @param formReq 参数配置修改参数
     * @return 参数配置修改结果
     */
    Boolean update(ConfigFormReq formReq);


    /**
     * 删除参数配置
     *
     * @param ids 参数配置 ID 列表
     * @return 参数配置删除结果
     */
    Boolean delete(Long[] ids);
}
