package com.qiuyundev.boot.service;

import com.qiuyundev.boot.domain.entity.SysDictItem;
import com.baomidou.mybatisplus.extension.service.IService;

import com.qiuyundev.boot.base.vo.PageResult;
import com.qiuyundev.boot.domain.vo.dict.DictItemFormReq;
import com.qiuyundev.boot.domain.vo.dict.DictItemPageReq;
import com.qiuyundev.boot.domain.vo.dict.DictItemResp;

/**
* <p>
    * 字典项表 服务类
    * </p>
*
* @author Qiuyun
* @since 2024-05-13
*/
public interface ISysDictItemService extends IService<SysDictItem> {
   /**
    * 分页查询字典项表
    *
    * @param pageReq 字典项表分页查询参数
    * @return 字典项表列表
    */
   PageResult<DictItemResp> page(DictItemPageReq pageReq);

    /**
     * 字典项表详情
     *
     * @param id 字典项表 ID
     * @return 字典项表详情
     */
    DictItemResp detail(Integer id);

    /**
     * 新增字典项表
     *
     * @param formReq 字典项表新增参数
     * @return 字典项表新增结果
     */
    Boolean add(DictItemFormReq formReq);

    /**
     * 修改字典项表
     *
     * @param formReq 字典项表修改参数
     * @return 字典项表修改结果
     */
    Boolean update(DictItemFormReq formReq);


    /**
     * 删除字典项表
     *
     * @param ids 字典项表 ID 列表
     * @return 字典项表删除结果
     */
    Boolean delete(Long[] ids);
}
