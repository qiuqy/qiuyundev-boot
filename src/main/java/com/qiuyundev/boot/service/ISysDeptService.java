package com.qiuyundev.boot.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.qiuyundev.boot.domain.entity.SysDept;
import com.qiuyundev.boot.domain.vo.TreeVO;
import com.qiuyundev.boot.domain.vo.dept.DeptBriefResp;
import com.qiuyundev.boot.domain.vo.dept.DeptFormReq;
import com.qiuyundev.boot.domain.vo.dept.DeptListReq;
import com.qiuyundev.boot.domain.vo.dept.DeptResp;

import java.util.List;

/**
 * <p>
 * 部门 服务接口
 * </p>
 *
 * @author Qiuyun
 * @since 2024/04/11
 */
public interface ISysDeptService extends IService<SysDept> {

    /**
     * 部门列表
     *
     * @param deptListReq 部门列表请求参数
     * @return 部门列表
     */
    List<TreeVO.TreeResp> list(DeptListReq deptListReq);

    /**
     * 部门详情
     *
     * @param id 部门 ID
     * @return 部门详情
     */
    DeptResp detail(Integer id);

    /**
     * 新增部门
     *
     * @param deptFormReq 部门新增参数
     * @return 部门新增结果
     */
    Boolean add(DeptFormReq deptFormReq);

    /**
     * 修改部门
     *
     * @param deptFormReq 部门修改参数
     * @return 部门修改结果
     */
    Boolean update(DeptFormReq deptFormReq);


    /**
     * 删除部门
     *
     * @param ids 部门 ID 列表
     * @return 部门删除结果
     */
    Boolean delete(Long[] ids);

    List<TreeVO.TreeResp> tree(TreeVO.TreeReq deptTreeReq);


    List<DeptBriefResp> options();

    List<Integer> findSelfAndChildrenIds(Integer id);
}
