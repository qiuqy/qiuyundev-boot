package com.qiuyundev.boot.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.qiuyundev.boot.domain.entity.SysRoleMenu;

import java.util.List;

/**
 * <p>
 * 角色菜单 服务类
 * </p>
 *
 * @author Qiuyun
 * @since 2024/04/11
 */
public interface ISysRoleMenuService extends IService<SysRoleMenu> {


    boolean saveRoleMenuList(List<Integer> menuIds, Integer roleId, boolean updateFlag);
}
