package com.qiuyundev.boot.service;

import com.qiuyundev.boot.domain.entity.SysUser;

import java.util.List;
import java.util.Map;

public interface IDataScopeDemoService {
    SysUser custom(Integer id);

    List<SysUser> selfDept(Integer id);

    List<SysUser> selfDeptAndChildren(List<Integer> ids);

    List<Map<String, Object>>  dataScopeDemoSelf();
}
