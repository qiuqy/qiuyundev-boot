package com.qiuyundev.boot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qiuyundev.boot.domain.entity.SysUserPost;

public interface ISysUserPostService extends IService<SysUserPost> {
}
