package com.qiuyundev.boot.service;

import com.qiuyundev.boot.base.vo.PageResult;
import com.qiuyundev.boot.core.security.LoginUser;
import com.qiuyundev.boot.domain.vo.onlineuser.OnlinePageReq;
import com.qiuyundev.boot.domain.vo.onlineuser.OnlineUserResp;

public interface IOnlineUserService {
    void addOrUpdateOnlineUser(LoginUser.LoginInfo loginInfo, double score);

    void deleteOnlineUser(LoginUser.LoginInfo loginInfo);

    PageResult<OnlineUserResp> page(OnlinePageReq onlinePageReq);

    Boolean kick(String userKey);
}
