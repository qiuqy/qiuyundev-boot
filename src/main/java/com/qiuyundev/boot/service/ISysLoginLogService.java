package com.qiuyundev.boot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qiuyundev.boot.base.vo.PageResult;
import com.qiuyundev.boot.core.security.LoginUser;
import com.qiuyundev.boot.domain.entity.SysLoginLog;
import com.qiuyundev.boot.domain.vo.loginlog.LoginLogPageReq;
import com.qiuyundev.boot.domain.vo.loginlog.LoginLogResp;
import org.springframework.scheduling.annotation.Async;

public interface ISysLoginLogService extends IService<SysLoginLog> {
    PageResult<LoginLogResp> page(LoginLogPageReq loginLogPageReq);

    @Async
    void saveLoginLog(LoginUser.LoginInfo loginInfo);
}
