package com.qiuyundev.boot.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.qiuyundev.boot.domain.entity.SysUserRole;

/**
 * <p>
 * 用户角色 服务接口
 * </p>
 *
 * @author Qiuyun
 * @since 2024/04/11
 */
public interface ISysUserRoleService extends IService<SysUserRole> {

}
