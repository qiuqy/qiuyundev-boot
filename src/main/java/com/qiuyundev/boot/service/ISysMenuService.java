package com.qiuyundev.boot.service;


import cn.hutool.core.lang.Pair;
import com.baomidou.mybatisplus.extension.service.IService;
import com.qiuyundev.boot.domain.entity.SysMenu;
import com.qiuyundev.boot.domain.enums.menu.MenuTypeEnum;
import com.qiuyundev.boot.domain.vo.TreeVO;
import com.qiuyundev.boot.domain.vo.account.AccountMenuResp;
import com.qiuyundev.boot.domain.vo.menu.MenuFormReq;
import com.qiuyundev.boot.domain.vo.menu.MenuListReq;
import com.qiuyundev.boot.domain.vo.menu.MenuResp;

import java.util.List;

/**
 * <p>
 * 菜单 服务接口
 * </p>
 *
 * @author Qiuyun
 * @since 2024/04/11
 */
public interface ISysMenuService extends IService<SysMenu> {
    /**
     * 菜单列表
     *
     * @param menuListReq 菜单列表请求参数
     * @return 菜单列表
     */
    List<TreeVO.TreeResp> list(MenuListReq menuListReq);

    /**
     * 菜单详情
     *
     * @param id 菜单 ID
     * @return 菜单详情
     */
    MenuResp detail(Integer id);

    /**
     * 新增菜单
     *
     * @param menuFormReq 菜单新增参数
     * @return 菜单新增结果
     */
    Boolean add(MenuFormReq menuFormReq);

    /**
     * 修改菜单
     *
     * @param menuFormReq 菜单修改参数
     * @return 菜单修改结果
     */
    Boolean update(MenuFormReq menuFormReq);


    /**
     * 删除菜单
     *
     * @param ids 菜单 ID 列表
     * @return 菜单删除结果
     */
    Boolean delete(Long[] ids);

    List<TreeVO.TreeResp> tree(TreeVO.TreeReq treeReq);

    List<String> selectPermsByUserId(Integer userId);

//    List<SysMenu> selectMenusByUserIdAndType(Integer userId, MenuTypeEnum type);

//    List<SysMenu> selectMenusByUserId(Integer userId);

    //    @Override
//    public List<SysMenu> selectMenusByUserIdAndType(Integer userId, MenuTypeEnum type) {
//        return this.baseMapper.selectMenusByUserIdAndType(userId, type);
//    }

    List<AccountMenuResp> selectMenusByUserId(Integer userId);

    Pair<List<AccountMenuResp>, List<String>> selectMenusAndPermsByUserId(Integer userId);

    List<String> permissions();
}
