package com.qiuyundev.boot.service;

import com.qiuyundev.boot.core.db.domain.TableInfo;
import com.qiuyundev.boot.domain.vo.generator.TableColumnInfoReq;
import com.qiuyundev.boot.domain.vo.generator.TableColumnInfoResp;

import java.util.List;

public interface ICodeGeneratorService {
    List<TableInfo> selectTables(String tableName);

    List<TableColumnInfoResp> selectTableColumns(String tableName);

    Boolean saveOrUpdateConfig(List<TableColumnInfoReq> list);
}
