package com.qiuyundev.boot.service;

import com.qiuyundev.boot.domain.entity.SysOperationLog;
import com.baomidou.mybatisplus.extension.service.IService;

import com.qiuyundev.boot.base.vo.PageResult;
import com.qiuyundev.boot.domain.vo.operation.OperationLogFormReq;
import com.qiuyundev.boot.domain.vo.operation.OperationLogPageReq;
import com.qiuyundev.boot.domain.vo.operation.OperationLogResp;

/**
* <p>
    * 操作日志 服务类
    * </p>
*
* @author Qiuyun
* @since 2024-05-21
*/
public interface ISysOperationLogService extends IService<SysOperationLog> {
   /**
    * 分页查询操作日志
    *
    * @param pageReq 操作日志分页查询参数
    * @return 操作日志列表
    */
   PageResult<OperationLogResp> page(OperationLogPageReq pageReq);

    /**
     * 操作日志详情
     *
     * @param id 操作日志 ID
     * @return 操作日志详情
     */
    OperationLogResp detail(Integer id);

    /**
     * 新增操作日志
     *
     * @param formReq 操作日志新增参数
     * @return 操作日志新增结果
     */
    Boolean add(OperationLogFormReq formReq);

    /**
     * 修改操作日志
     *
     * @param formReq 操作日志修改参数
     * @return 操作日志修改结果
     */
    Boolean update(OperationLogFormReq formReq);


    /**
     * 删除操作日志
     *
     * @param ids 操作日志 ID 列表
     * @return 操作日志删除结果
     */
    Boolean delete(Long[] ids);
}
