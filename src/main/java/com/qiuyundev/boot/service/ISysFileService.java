package com.qiuyundev.boot.service;

import com.qiuyundev.boot.domain.entity.SysFile;
import com.baomidou.mybatisplus.extension.service.IService;

import com.qiuyundev.boot.base.vo.PageResult;
import com.qiuyundev.boot.domain.vo.file.FileFormReq;
import com.qiuyundev.boot.domain.vo.file.FilePageReq;
import com.qiuyundev.boot.domain.vo.file.FileResp;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.multipart.MultipartFile;

/**
* <p>
    * 文件 服务类
    * </p>
*
* @author Qiuyun
* @since 2024-05-20
*/
public interface ISysFileService extends IService<SysFile> {
   /**
    * 分页查询文件
    *
    * @param pageReq 文件分页查询参数
    * @return 文件列表
    */
   PageResult<FileResp> page(FilePageReq pageReq);

    /**
     * 文件详情
     *
     * @param id 文件 ID
     * @return 文件详情
     */
    FileResp detail(Integer id);

    /**
     * 新增文件
     *
     * @param formReq 文件新增参数
     * @return 文件新增结果
     */
    Boolean add(FileFormReq formReq);

    /**
     * 修改文件
     *
     * @param formReq 文件修改参数
     * @return 文件修改结果
     */
    Boolean update(FileFormReq formReq);


    /**
     * 删除文件
     *
     * @param ids 文件 ID 列表
     * @return 文件删除结果
     */
    Boolean delete(Long[] ids);

 /**
  * 上传文件
  *
  * @param file 文件
  * @return {@link String}
  */
 String uploadFile(MultipartFile file);

    /**
     * 预览文件(只能预览图片)
     *
     * @param id       文件id
     * @param response 响应
     */
    void previewFile(Integer id, HttpServletResponse response);
}
