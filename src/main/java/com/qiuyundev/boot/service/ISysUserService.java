package com.qiuyundev.boot.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.qiuyundev.boot.base.vo.PageResult;
import com.qiuyundev.boot.core.security.LoginUser;
import com.qiuyundev.boot.domain.entity.SysUser;
import com.qiuyundev.boot.domain.vo.user.UserFormReq;
import com.qiuyundev.boot.domain.vo.user.UserPageReq;
import com.qiuyundev.boot.domain.vo.user.UserResp;

/**
 * <p>
 * 用户 服务类
 * </p>
 *
 * @author Qiuyun
 * @since 2024/04/12
 */
public interface ISysUserService extends IService<SysUser> {

    /**
     * 分页查询用户
     *
     * @param userPageReq 用户分页查询参数
     * @return 用户列表
     */
    PageResult<UserResp> page(UserPageReq userPageReq);

    /**
     * 用户详情
     *
     * @param id 用户 ID
     * @return 用户详情
     */
    UserResp detail(Integer id);

    /**
     * 新增用户
     *
     * @param userFormReq 用户新增参数
     * @return 用户新增结果
     */
    Boolean add(UserFormReq userFormReq);

    /**
     * 修改用户
     *
     * @param userFormReq 用户修改参数
     * @return 用户修改结果
     */
    Boolean update(UserFormReq userFormReq);


    /**
     * 删除用户
     *
     * @param ids 用户 ID 列表
     * @return 用户删除结果
     */
    Boolean delete(Long[] ids);

    void updateLoginInfo(LoginUser.LoginInfo loginInfo);
}
