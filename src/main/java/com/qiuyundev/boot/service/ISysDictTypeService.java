package com.qiuyundev.boot.service;

import com.qiuyundev.boot.domain.entity.SysDictType;
import com.baomidou.mybatisplus.extension.service.IService;

import com.qiuyundev.boot.base.vo.PageResult;
import com.qiuyundev.boot.domain.vo.dict.DictTypeFormReq;
import com.qiuyundev.boot.domain.vo.dict.DictTypePageReq;
import com.qiuyundev.boot.domain.vo.dict.DictTypeResp;

import java.util.List;

/**
* <p>
    * 字典类型 服务类
    * </p>
*
* @author Qiuyun
* @since 2024-05-13
*/
public interface ISysDictTypeService extends IService<SysDictType> {
   /**
    * 分页查询字典类型
    *
    * @param pageReq 字典类型分页查询参数
    * @return 字典类型列表
    */
   PageResult<DictTypeResp> page(DictTypePageReq pageReq);

    /**
     * 字典类型详情
     *
     * @param id 字典类型 ID
     * @return 字典类型详情
     */
    DictTypeResp detail(Integer id);

    /**
     * 新增字典类型
     *
     * @param formReq 字典类型新增参数
     * @return 字典类型新增结果
     */
    Boolean add(DictTypeFormReq formReq);

    /**
     * 修改字典类型
     *
     * @param formReq 字典类型修改参数
     * @return 字典类型修改结果
     */
    Boolean update(DictTypeFormReq formReq);


    /**
     * 删除字典类型
     *
     * @param ids 字典类型 ID 列表
     * @return 字典类型删除结果
     */
    Boolean delete(Long[] ids);

 /**
  * 字典类型下拉选
  *
  * @return 字典类型下拉选
  */
    List<DictTypeResp> options();
}
