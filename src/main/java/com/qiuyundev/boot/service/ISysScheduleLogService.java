package com.qiuyundev.boot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qiuyundev.boot.base.vo.PageResult;
import com.qiuyundev.boot.domain.entity.SysScheduleLog;
import com.qiuyundev.boot.domain.vo.schedule.SysScheduleLogPageReq;
import com.qiuyundev.boot.domain.vo.schedule.SysScheduleLogResp;

import java.time.LocalDateTime;

/**
 * <p>
 * 任务日志表 服务类
 * </p>
 *
 * @author Qiuyun
 * @since 2024-06-03
 */
public interface ISysScheduleLogService extends IService<SysScheduleLog> {
    /**
     * 分页查询任务日志表
     *
     * @param pageReq 任务日志表分页查询参数
     * @return 任务日志表列表
     */
    PageResult<SysScheduleLogResp> page(SysScheduleLogPageReq pageReq);

    /**
     * 任务日志表详情
     *
     * @param id 任务日志表 ID
     * @return 任务日志表详情
     */
    SysScheduleLogResp detail(Integer id);

    void saveScheduleLog(Exception exception, long costTime, LocalDateTime startTime, Integer scheduleId, String scheduleName);
}
