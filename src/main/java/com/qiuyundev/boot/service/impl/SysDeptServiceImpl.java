package com.qiuyundev.boot.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qiuyundev.boot.base.util.BeanUtils;
import com.qiuyundev.boot.base.util.DatabaseUtils;
import com.qiuyundev.boot.core.mybatisplus.BaseBriefEntity;
import com.qiuyundev.boot.domain.entity.SysDept;
import com.qiuyundev.boot.domain.vo.TreeVO;
import com.qiuyundev.boot.domain.vo.dept.*;
import com.qiuyundev.boot.mapper.SysDeptMapper;
import com.qiuyundev.boot.service.ISysDeptService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 部门 服务实现类
 * </p>
 *
 * @author Qiuyun
 * @since 2024/04/12
 */
@Service
public class SysDeptServiceImpl extends ServiceImpl<SysDeptMapper, SysDept> implements ISysDeptService {

    @Override
    public List<TreeVO.TreeResp> list(DeptListReq deptListReq) {
        if (!StringUtils.hasText(deptListReq.getName())) {
            // 查出全部部门数据
            List<TreeVO.TreeResp> allDept = this.list()
                    .stream()
                    .map(e -> (TreeVO.TreeResp) BeanUtils.copy(e, DeptListResp.class))
                    .toList();
            // 先找出顶级部门，然后递归获取所有子部门
            return TreeVO.buildTree(allDept);
        }

        return this.lambdaQuery()
                .like(StringUtils.hasText(deptListReq.getName()), SysDept::getName, deptListReq.getName())
                .list()
                .stream()
                .map(e -> (TreeVO.TreeResp) BeanUtils.copy(e, DeptListResp.class))
                .toList();
    }

    /**
     * 部门详情
     *
     * @param id 部门 ID
     * @return 部门详情
     */
    @Override
    public DeptResp detail(Integer id) {
        return BeanUtils.copy(this.getById(id), DeptResp.class);
    }

    private void recursion(Integer parentId, List<Integer> nodes) {
        if (null != parentId) {
            nodes.add(parentId);
            SysDept dept = this.getById(parentId);
            if (null != dept) {
                recursion(dept.getParentId(), nodes);
            }
        }
    }

    private String buildNodes(Integer parentId) {
        // 递归获取部门树状结构的路径id
        List<Integer> nodes = new ArrayList<>();
        this.recursion(parentId, nodes);
        if (!nodes.isEmpty()) {
            // 反转
            List<Integer> reversed = nodes.reversed();
            return reversed.stream().map(Object::toString).collect(Collectors.joining(","));
        }

        return null;
    }

    /**
     * 新增部门
     *
     * @param deptFormReq 部门新增参数
     * @return 部门新增结果
     */
    @Override
    public Boolean add(DeptFormReq deptFormReq) {
        SysDept sysDept = BeanUtils.copy(deptFormReq, SysDept.class);
        sysDept.setNodes(this.buildNodes(deptFormReq.getParentId()));
        return this.save(sysDept);
    }


    /**
     * 修改部门
     *
     * @param deptFormReq 部门修改参数
     * @return 部门修改结果
     */
    @Override
    public Boolean update(DeptFormReq deptFormReq) {
        SysDept sysDept = BeanUtils.copy(deptFormReq, SysDept.class);
        sysDept.setNodes(this.buildNodes(deptFormReq.getParentId()));
        this.lambdaUpdate()
                .set(SysDept::getParentId, deptFormReq.getParentId())
                .set(SysDept::getName, deptFormReq.getName())
                .set(SysDept::getNodes, sysDept.getNodes())
                .set(SysDept::getSort, deptFormReq.getSort())
                .set(SysDept::getRemark, deptFormReq.getRemark())
                .eq(SysDept::getId, deptFormReq.getId());
        return this.updateById(sysDept);
    }

    /**
     * 删除部门
     *
     * @param ids 部门 ID 列表
     * @return 部门删除结果
     */
    @Override
    @Transactional
    public Boolean delete(Long[] ids) {
        return this.removeByIds(Arrays.asList(ids));
    }

    @Override
    public List<TreeVO.TreeResp> tree(TreeVO.TreeReq deptTreeReq) {
        // 查出全部部门数据
        List<TreeVO.TreeResp> allDept = this.list()
                .stream()
                .map(e -> BeanUtils.copy(e, TreeVO.TreeResp.class))
                .toList();
        // 先找出顶级部门，然后递归获取所有子部门
        return TreeVO.searchTree(deptTreeReq, allDept);
    }

    /**
     * 获取全部部门数据的精简列表
     *
     * @return 精简部门列表
     */
    @Override
    public List<DeptBriefResp> options() {
        List<SysDept> options = this.lambdaQuery()
                .select(SysDept::getId, SysDept::getName)
                .list();
        return BeanUtils.copyList(options, DeptBriefResp.class);
    }

    /**
     * 根据ID获取自己部门ID以及所有下级部门ID
     *
     * @param id 身份证件
     * @return {@link List}<{@link Integer}>
     */
    @Override
    public List<Integer> findSelfAndChildrenIds(Integer id) {
        return this.lambdaQuery()
                .select(SysDept::getId)
                .and(e -> e.eq(SysDept::getId, id)
                        .or()
                        .apply(DatabaseUtils.findInSet(id, "nodes")))
                .list()
                .stream().map(BaseBriefEntity::getId)
                .toList();
    }

}
