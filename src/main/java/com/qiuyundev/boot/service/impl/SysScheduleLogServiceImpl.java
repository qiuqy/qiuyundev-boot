package com.qiuyundev.boot.service.impl;

import cn.hutool.core.date.LocalDateTimeUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qiuyundev.boot.base.enums.ApiStatusEnum;
import com.qiuyundev.boot.base.util.BeanUtils;
import com.qiuyundev.boot.base.vo.PageResult;
import com.qiuyundev.boot.domain.entity.SysLoginLog;
import com.qiuyundev.boot.domain.entity.SysScheduleLog;
import com.qiuyundev.boot.domain.vo.schedule.SysScheduleLogPageReq;
import com.qiuyundev.boot.domain.vo.schedule.SysScheduleLogResp;
import com.qiuyundev.boot.mapper.SysScheduleLogMapper;
import com.qiuyundev.boot.service.ISysScheduleLogService;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * <p>
 * 任务日志表 服务实现类
 * </p>
 *
 * @author Qiuyun
 * @since 2024-06-03
 */
@Service
public class SysScheduleLogServiceImpl extends ServiceImpl<SysScheduleLogMapper, SysScheduleLog> implements ISysScheduleLogService {
    /**
     * 分页查询任务日志表
     *
     * @param pageReq 任务日志表分页查询参数
     * @return 任务日志表列表
     */
    @Override
    public PageResult<SysScheduleLogResp> page(SysScheduleLogPageReq pageReq) {
        // 如果传入时间范围查询参数，要把结束时间当天的数据查询出来
        if (StringUtils.hasText(pageReq.getEndTime())) {
            LocalDate localDate = LocalDateTimeUtil.parseDate(pageReq.getEndTime()).plusDays(1);
            pageReq.setEndTime(LocalDateTimeUtil.formatNormal(localDate));
        }
        IPage<SysScheduleLog> page = this.lambdaQuery()
                .like(StringUtils.hasText(pageReq.getScheduleName()), SysScheduleLog::getScheduleName, pageReq.getScheduleName())
                .eq(null != pageReq.getStatus(), SysScheduleLog::getStatus, pageReq.getStatus())
                .between(StringUtils.hasText(pageReq.getStartTime())
                                && StringUtils.hasText(pageReq.getEndTime()),
                        SysScheduleLog::getExecuteTime, pageReq.getStartTime(), pageReq.getEndTime())
                .orderByDesc(SysScheduleLog::getId)
                .page(pageReq.page());

        return PageResult.of(page, SysScheduleLogResp.class);
    }

    /**
     * 任务日志表详情
     *
     * @param id 任务日志表ID
     * @return 任务日志表详情
     */
    @Override
    public SysScheduleLogResp detail(Integer id) {
        return BeanUtils.copy(this.getById(id), SysScheduleLogResp.class);
    }

    @Async
    @Override
    public void saveScheduleLog(Exception exception,
                                long costTime,
                                LocalDateTime startTime,
                                Integer scheduleId,
                                String scheduleName) {
        SysScheduleLog sysScheduleLog = new SysScheduleLog();
        sysScheduleLog.setScheduleId(scheduleId);
        sysScheduleLog.setScheduleName(scheduleName);
        if (null != exception) {
            sysScheduleLog.setExceptionInfo(exception.getMessage());
            sysScheduleLog.setStatus(ApiStatusEnum.FAILURE);
        } else {
            sysScheduleLog.setStatus(ApiStatusEnum.SUCCESS);
        }
        sysScheduleLog.setScheduleName(scheduleName);
        sysScheduleLog.setCostTime(costTime);
        sysScheduleLog.setExecuteTime(startTime);
        this.save(sysScheduleLog);
    }


}
