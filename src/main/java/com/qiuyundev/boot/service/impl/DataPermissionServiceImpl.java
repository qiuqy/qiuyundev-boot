package com.qiuyundev.boot.service.impl;

import com.qiuyundev.boot.core.datascope.DataPermissionService;
import com.qiuyundev.boot.domain.entity.SysRoleDept;
import com.qiuyundev.boot.service.ISysDeptService;
import com.qiuyundev.boot.service.ISysRoleDeptService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

/**
 * DataPermissionServiceImpl
 *
 * @author Qiuyun
 * @since 2024/04/12
 */
@RequiredArgsConstructor
@Service(DataPermissionService.beanName)
public class DataPermissionServiceImpl implements DataPermissionService {
    private final ISysRoleDeptService roleDeptService;
    private final ISysDeptService deptService;


    @Override
    public String getRoleDeptIds(String roleId) {
        return this.roleDeptService.lambdaQuery()
                .select(SysRoleDept::getDeptId)
                .eq(SysRoleDept::getRoleId, roleId)
                .list()
                .stream()
                .map(roleDept -> roleDept.getDeptId().toString())
                .collect(Collectors.joining(","));
    }

    @Override
    public String getDeptAndChildrenIds(String deptId) {
        return this.deptService.findSelfAndChildrenIds(Integer.valueOf(deptId))
                .stream()
                .map(Object::toString)
                .collect(Collectors.joining(","));
    }

}
