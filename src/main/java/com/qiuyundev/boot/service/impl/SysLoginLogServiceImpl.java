package com.qiuyundev.boot.service.impl;

import cn.hutool.core.date.LocalDateTimeUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qiuyundev.boot.base.util.BeanUtils;
import com.qiuyundev.boot.base.vo.PageResult;
import com.qiuyundev.boot.core.security.LoginUser;
import com.qiuyundev.boot.domain.entity.SysLoginLog;
import com.qiuyundev.boot.domain.vo.loginlog.LoginLogPageReq;
import com.qiuyundev.boot.domain.vo.loginlog.LoginLogResp;
import com.qiuyundev.boot.mapper.SysLoginLogMapper;
import com.qiuyundev.boot.service.ISysLoginLogService;
import com.qiuyundev.boot.service.ISysUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.util.Date;

@Service
@RequiredArgsConstructor
public class SysLoginLogServiceImpl extends ServiceImpl<SysLoginLogMapper, SysLoginLog> implements ISysLoginLogService {
    private final ISysUserService userService;

    /**
     * 分页查询登录日志
     *
     * @param loginLogPageReq 登录日志分页查询参数
     * @return 登录日志列表
     */
    @Override
    public PageResult<LoginLogResp> page(LoginLogPageReq loginLogPageReq) {
        // 如果传入时间范围查询参数，要把结束时间当天的数据查询出来
        if (StringUtils.hasText(loginLogPageReq.getEndTime())) {
            LocalDate localDate = LocalDateTimeUtil.parseDate(loginLogPageReq.getEndTime()).plusDays(1);
            loginLogPageReq.setEndTime(LocalDateTimeUtil.formatNormal(localDate));
        }
        IPage<SysLoginLog> sysLoginLogIPage = this.lambdaQuery()
                .like(StringUtils.hasText(loginLogPageReq.getUsername()), SysLoginLog::getUsername, loginLogPageReq.getUsername())
                .like(StringUtils.hasText(loginLogPageReq.getLocation()), SysLoginLog::getLocation, loginLogPageReq.getLocation())
                .like(StringUtils.hasText(loginLogPageReq.getIp()), SysLoginLog::getIp, loginLogPageReq.getIp())
                .between(StringUtils.hasText(loginLogPageReq.getStartTime())
                                && StringUtils.hasText(loginLogPageReq.getEndTime()),
                        SysLoginLog::getLoginTime, loginLogPageReq.getStartTime(), loginLogPageReq.getEndTime())
                .orderByDesc(SysLoginLog::getId)
                .page(loginLogPageReq.page());

        return PageResult.of(sysLoginLogIPage, LoginLogResp.class);
    }

    /**
     * 保存登录日志
     *
     * @param loginInfo 登录信息
     */
    @Async
    @Override
    public void saveLoginLog(LoginUser.LoginInfo loginInfo) {
        // 保存登录日志
        this.save(BeanUtils.copy(loginInfo, SysLoginLog.class));
        // 更新用户登录信息
        this.userService.updateLoginInfo(loginInfo);
        System.out.println("保存登录日志" + new Date());
    }


}
