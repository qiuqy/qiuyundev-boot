package com.qiuyundev.boot.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qiuyundev.boot.base.util.BeanUtils;
import com.qiuyundev.boot.base.vo.PageResult;
import com.qiuyundev.boot.core.schedule.CronTaskRegistrar;
import com.qiuyundev.boot.core.schedule.MethodInvokerUtils;
import com.qiuyundev.boot.core.schedule.SchedulingRunnable;
import com.qiuyundev.boot.domain.entity.SysSchedule;
import com.qiuyundev.boot.domain.enums.schedule.ScheduleStatusEnum;
import com.qiuyundev.boot.domain.vo.schedule.SysScheduleFormReq;
import com.qiuyundev.boot.domain.vo.schedule.SysSchedulePageReq;
import com.qiuyundev.boot.domain.vo.schedule.SysScheduleResp;
import com.qiuyundev.boot.mapper.SysScheduleMapper;
import com.qiuyundev.boot.service.ISysScheduleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;

/**
 * <p>
 * 任务服务实现类
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-31
 */
@Service
@RequiredArgsConstructor
public class SysScheduleServiceImpl extends ServiceImpl<SysScheduleMapper, SysSchedule> implements ISysScheduleService {
    private final CronTaskRegistrar cronTaskRegistrar;

    /**
     * 分页查询任务表
     *
     * @param pageReq 任务表分页查询参数
     * @return 任务表列表
     */
    @Override
    public PageResult<SysScheduleResp> page(SysSchedulePageReq pageReq) {
        IPage<SysSchedule> page = this.lambdaQuery()
                .orderByDesc(SysSchedule::getUpdateTime)
                .page(pageReq.page());

        return PageResult.of(page, SysScheduleResp.class);
    }

    /**
     * 任务表详情
     *
     * @param id 任务表ID
     * @return 任务表详情
     */
    @Override
    public SysScheduleResp detail(Integer id) {
        return BeanUtils.copy(this.getById(id), SysScheduleResp.class);
    }

    /**
     * 新增任务表
     *
     * @param formReq 任务表新增参数
     * @return 任务表新增结果
     */
    @Override
    public Boolean add(SysScheduleFormReq formReq) {
        SysSchedule schedule = BeanUtils.copy(formReq, SysSchedule.class);
        boolean saved = this.save(schedule);
        if (saved) {
            if (schedule.getStatus() == ScheduleStatusEnum.RUNNING) {
                SchedulingRunnable task = new SchedulingRunnable(schedule.getId(), schedule.getName(), schedule.getInvokeTarget());
                cronTaskRegistrar.addCronTask(task, schedule.getCronExpression());
            }
        }
        return saved;
    }


    /**
     * 修改任务表
     *
     * @param formReq 任务表修改参数
     * @return 任务表修改结果
     */
    @Override
    public Boolean update(SysScheduleFormReq formReq) {
        SysSchedule schedule = BeanUtils.copy(formReq, SysSchedule.class);
        boolean update = this.updateById(BeanUtils.copy(formReq, SysSchedule.class));
        if (update) {
            // 修改成功,则先删除任务器中的任务,并重新添加
            SchedulingRunnable taskTemp = new SchedulingRunnable(schedule.getId(), null, null);
            cronTaskRegistrar.removeCronTask(taskTemp);
            if (schedule.getStatus() == ScheduleStatusEnum.RUNNING) {
                SchedulingRunnable task = new SchedulingRunnable(schedule.getId(), schedule.getName(), schedule.getInvokeTarget());
                cronTaskRegistrar.addCronTask(task, schedule.getCronExpression());
            }
        }
        return update;
    }

    /**
     * 删除任务表
     *
     * @param ids 任务ID列表
     * @return 任务表删除结果
     */
    @Override
    @Transactional
    public Boolean delete(Integer[] ids) {
        boolean removed = this.removeByIds(Arrays.asList(ids));
        if (removed) {
            for (Integer id : ids) {
                // 修改成功,则先删除任务器中的任务,并重新添加
                SchedulingRunnable taskTemp = new SchedulingRunnable(id, null, null);
                cronTaskRegistrar.removeCronTask(taskTemp);
            }
        }
        return removed;
    }

    @Override
    public Boolean once(Integer id) {
        // 获取任务信息
        SysSchedule schedule = this.getById(id);
        if (null != schedule) {
            try {
                MethodInvokerUtils.invokeMethod(schedule.getInvokeTarget());
                return true;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        return false;
    }

    @Override
    public Boolean start(Integer id) {
        // 获取任务信息
        SysSchedule schedule = this.getById(id);
        if (null != schedule) {
            if (schedule.getStatus() == ScheduleStatusEnum.STOP) {
                schedule.setStatus(ScheduleStatusEnum.RUNNING);
                boolean update = this.updateById(schedule);
                if (update) {
                    SchedulingRunnable task = new SchedulingRunnable(schedule.getId(), schedule.getName(), schedule.getInvokeTarget());
                    cronTaskRegistrar.addCronTask(task, schedule.getCronExpression());
                }
            }
        }
        return false;
    }

    @Override
    public Boolean stop(Integer id) {
        // 获取任务信息
        SysSchedule schedule = this.getById(id);
        if (null != schedule) {
            if (schedule.getStatus() == ScheduleStatusEnum.RUNNING) {
                schedule.setStatus(ScheduleStatusEnum.STOP);
                boolean update = this.updateById(schedule);
                if (update) {
                    SchedulingRunnable task = new SchedulingRunnable(schedule.getId(), schedule.getName(), schedule.getInvokeTarget());
                    cronTaskRegistrar.removeCronTask(task);
                }
            }
        }
        return false;
    }

}
