package com.qiuyundev.boot.service.impl;

import cn.hutool.core.date.LocalDateTimeUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qiuyundev.boot.base.util.BeanUtils;
import com.qiuyundev.boot.base.vo.PageResult;
import com.qiuyundev.boot.domain.entity.SysOperationLog;
import com.qiuyundev.boot.domain.vo.operation.OperationLogFormReq;
import com.qiuyundev.boot.domain.vo.operation.OperationLogPageReq;
import com.qiuyundev.boot.domain.vo.operation.OperationLogResp;
import com.qiuyundev.boot.mapper.SysOperationLogMapper;
import com.qiuyundev.boot.service.ISysOperationLogService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.util.Arrays;

/**
 * <p>
 * 操作日志 服务实现类
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-21
 */
@Service
public class SysOperationLogServiceImpl extends ServiceImpl<SysOperationLogMapper, SysOperationLog> implements ISysOperationLogService {
    /**
     * 分页查询操作日志
     *
     * @param pageReq 操作日志分页查询参数
     * @return 操作日志列表
     */
    @Override
    public PageResult<OperationLogResp> page(OperationLogPageReq pageReq) {
        // 如果传入时间范围查询参数，要把结束时间当天的数据查询出来
        if (StringUtils.hasText(pageReq.getEndTime())) {
            LocalDate localDate = LocalDateTimeUtil.parseDate(pageReq.getEndTime()).plusDays(1);
            pageReq.setEndTime(LocalDateTimeUtil.formatNormal(localDate));
        }
        IPage<SysOperationLog> page = this.lambdaQuery()
                .like(StringUtils.hasText(pageReq.getTag()), SysOperationLog::getTag, pageReq.getTag())
                .like(StringUtils.hasText(pageReq.getSummary()), SysOperationLog::getSummary, pageReq.getSummary())
                .like(StringUtils.hasText(pageReq.getRequestUrl()), SysOperationLog::getRequestUrl, pageReq.getRequestUrl())
                .eq(null != pageReq.getStatus(), SysOperationLog::getStatus, pageReq.getStatus())
                .like(StringUtils.hasText(pageReq.getOperator()), SysOperationLog::getOperator, pageReq.getOperator())
                .between(StringUtils.hasText(pageReq.getStartTime())
                                && StringUtils.hasText(pageReq.getEndTime()),
                        SysOperationLog::getOperationTime, pageReq.getStartTime(), pageReq.getEndTime())
                .orderByDesc(SysOperationLog::getId)
                .page(pageReq.page());

        return PageResult.of(page, OperationLogResp.class);
    }

    /**
     * 操作日志详情
     *
     * @param id 操作日志ID
     * @return 操作日志详情
     */
    @Override
    public OperationLogResp detail(Integer id) {
        return BeanUtils.copy(this.getById(id), OperationLogResp.class);
    }

    /**
     * 新增操作日志
     *
     * @param formReq 操作日志新增参数
     * @return 操作日志新增结果
     */
    @Override
    public Boolean add(OperationLogFormReq formReq) {
        return this.save(BeanUtils.copy(formReq, SysOperationLog.class));
    }


    /**
     * 修改操作日志
     *
     * @param formReq 操作日志修改参数
     * @return 操作日志修改结果
     */
    @Override
    public Boolean update(OperationLogFormReq formReq) {
        return this.updateById(BeanUtils.copy(formReq, SysOperationLog.class));
    }

    /**
     * 删除操作日志
     *
     * @param ids 操作日志 ID列表
     * @return 操作日志删除结果
     */
    @Override
    @Transactional
    public Boolean delete(Long[] ids) {
        return this.removeByIds(Arrays.asList(ids));
    }

}
