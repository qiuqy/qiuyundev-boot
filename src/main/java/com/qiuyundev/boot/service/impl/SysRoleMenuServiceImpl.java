package com.qiuyundev.boot.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qiuyundev.boot.domain.entity.SysRoleMenu;
import com.qiuyundev.boot.mapper.SysRoleMenuMapper;
import com.qiuyundev.boot.service.ISysRoleMenuService;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;

/**
 * <p>
 * 角色菜单 服务实现类
 * </p>
 *
 * @author Qiuyun
 * @since 2024/04/12
 */
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements ISysRoleMenuService {

    @Override
    public boolean saveRoleMenuList(List<Integer> menuIds, Integer roleId, boolean updateFlag) {
        if (updateFlag) {
            this.lambdaUpdate()
                    .eq(SysRoleMenu::getRoleId, roleId)
                    .remove();
        }
        // 保存菜单
        if (ObjectUtils.isEmpty(menuIds)) {
            return false;
        }
        return this.saveBatch(menuIds.stream()
                .map(menuId -> {
                    SysRoleMenu sysRoleMenu = new SysRoleMenu();
                    sysRoleMenu.setRoleId(roleId);
                    sysRoleMenu.setMenuId(menuId);
                    return sysRoleMenu;
                })
                .toList());
    }
}
