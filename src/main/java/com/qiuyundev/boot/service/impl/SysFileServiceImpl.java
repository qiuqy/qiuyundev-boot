package com.qiuyundev.boot.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qiuyundev.boot.base.util.BeanUtils;
import com.qiuyundev.boot.base.vo.PageResult;
import com.qiuyundev.boot.core.file.FileService;
import com.qiuyundev.boot.core.security.SecurityUtils;
import com.qiuyundev.boot.domain.entity.SysFile;
import com.qiuyundev.boot.domain.enums.file.FileCategoryEnum;
import com.qiuyundev.boot.domain.vo.file.FileFormReq;
import com.qiuyundev.boot.domain.vo.file.FilePageReq;
import com.qiuyundev.boot.domain.vo.file.FileResp;
import com.qiuyundev.boot.mapper.SysFileMapper;
import com.qiuyundev.boot.service.ISysFileService;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.Objects;

/**
 * <p>
 * 文件 服务实现类
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-20
 */
@Service
@RequiredArgsConstructor
public class SysFileServiceImpl extends ServiceImpl<SysFileMapper, SysFile> implements ISysFileService {
    private final FileService fileService;

    @Value("${file.path.prefix:}")
    private String filePathPrefix;

    /**
     * 分页查询文件
     *
     * @param pageReq 文件分页查询参数
     * @return 文件列表
     */
    @Override
    public PageResult<FileResp> page(FilePageReq pageReq) {
        IPage<SysFile> page = this.lambdaQuery()
                .like(StringUtils.hasText(pageReq.getName()), SysFile::getName, pageReq.getName())
                .orderByDesc(SysFile::getUpdateTime)
                .page(pageReq.page());

        return PageResult.of(page, FileResp.class);
    }

    /**
     * 文件详情
     *
     * @param id 文件ID
     * @return 文件详情
     */
    @Override
    public FileResp detail(Integer id) {
        return BeanUtils.copy(this.getById(id), FileResp.class);
    }

    /**
     * 新增文件
     *
     * @param formReq 文件新增参数
     * @return 文件新增结果
     */
    @Override
    public Boolean add(FileFormReq formReq) {
        return this.save(BeanUtils.copy(formReq, SysFile.class));
    }


    /**
     * 修改文件
     *
     * @param formReq 文件修改参数
     * @return 文件修改结果
     */
    @Override
    public Boolean update(FileFormReq formReq) {
        return this.updateById(BeanUtils.copy(formReq, SysFile.class));
    }

    /**
     * 删除文件
     *
     * @param ids 文件 ID列表
     * @return 文件删除结果
     */
    @Override
    @Transactional
    public Boolean delete(Long[] ids) {
        for (Long id : ids) {
            SysFile file = this.getById(id);
            if (file != null) {
                // 删除文件
                this.fileService.deleteFile(file.getPath());
            }
        }

        return this.removeByIds(Arrays.asList(ids));
    }

    @Override
    public String uploadFile(MultipartFile file) {
        String path = this.fileService.uploadFile(file);
        if (path != null) {
            SysFile sysFile = new SysFile();
            sysFile.setName(file.getOriginalFilename());
            // 获取文件后缀名称
            String extension = Objects.requireNonNull(file.getOriginalFilename())
                    .substring(file.getOriginalFilename().lastIndexOf("."));
            sysFile.setExtension(extension);
            sysFile.setCategory(FileCategoryEnum.fromExtension(extension));
            sysFile.setSize(file.getSize());
            sysFile.setExtension(extension);
            sysFile.setContentType(file.getContentType());
            sysFile.setPath(path);
            sysFile.setUploadUsername(SecurityUtils.getUsername());
            this.save(sysFile);
            return filePathPrefix + path;
        }

        return null;
    }

    @Override
    public void previewFile(Integer id, HttpServletResponse response) {
        SysFile file = this.getById(id);
        if (file == null) {
            throw new RuntimeException("文件不存在");
        }
        this.fileService.previewFile(file.getPath(), file.getContentType(), response);
    }

}
