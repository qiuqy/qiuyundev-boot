package com.qiuyundev.boot.service.impl;

import com.qiuyundev.boot.core.security.SecurityUtils;
import com.qiuyundev.boot.domain.vo.account.AccountMenuResp;
import com.qiuyundev.boot.service.IAccountService;
import com.qiuyundev.boot.service.ISysMenuService;
import com.qiuyundev.boot.service.ISysUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 账户 服务实现类
 * </p>
 *
 * @author Qiuyun
 * @since 2024/04/18
 */
@Service
@RequiredArgsConstructor
public class AccountServiceImpl implements IAccountService {
    private final ISysUserService userService;
    private final ISysMenuService menuService;

    /**
     * 获取当前登录用户菜单
     *
     * @return 权限列表
     */
    @Override
    public List<AccountMenuResp> menus() {
        // 菜单里面信息太多，没必要登录的时候放入缓存，太占内存了
        // return SecurityUtils.getUserInfo().getMenus();
        return this.menuService.selectMenusByUserId(SecurityUtils.getUserId());
    }

    /**
     * 获取当前登录用户权限 - 记录菜单更新时间，token 根据更新时间刷新下
     *
     * @return 权限列表
     */
    @Override
    public List<String> permissions() {
        // 非实时查询，使用登录时候放入缓存的权限
        // return SecurityUtils.getUserInfo().getPermissions();
        // 实时查询
        return this.menuService.selectPermsByUserId(SecurityUtils.getUserId());
    }
}
