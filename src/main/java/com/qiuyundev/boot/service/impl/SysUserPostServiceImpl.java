package com.qiuyundev.boot.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qiuyundev.boot.domain.entity.SysUserPost;
import com.qiuyundev.boot.mapper.SysUserPostMapper;
import com.qiuyundev.boot.service.ISysUserPostService;
import org.springframework.stereotype.Service;

@Service
public class SysUserPostServiceImpl extends ServiceImpl<SysUserPostMapper, SysUserPost> implements ISysUserPostService {
}
