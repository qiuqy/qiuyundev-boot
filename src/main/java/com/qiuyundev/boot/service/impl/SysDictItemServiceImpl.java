package com.qiuyundev.boot.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qiuyundev.boot.base.util.BeanUtils;
import com.qiuyundev.boot.base.vo.PageResult;
import com.qiuyundev.boot.domain.entity.SysDictItem;
import com.qiuyundev.boot.domain.vo.dict.DictItemFormReq;
import com.qiuyundev.boot.domain.vo.dict.DictItemPageReq;
import com.qiuyundev.boot.domain.vo.dict.DictItemResp;
import com.qiuyundev.boot.mapper.SysDictItemMapper;
import com.qiuyundev.boot.service.ISysDictItemService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Arrays;

/**
 * <p>
 * 字典项表 服务实现类
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-13
 */
@Service
public class SysDictItemServiceImpl extends ServiceImpl<SysDictItemMapper, SysDictItem> implements ISysDictItemService {
    /**
     * 分页查询字典项表
     *
     * @param pageReq 字典项表分页查询参数
     * @return 字典项表列表
     */
    @Override
    public PageResult<DictItemResp> page(DictItemPageReq pageReq) {
        IPage<SysDictItem> page = this.lambdaQuery()
                .eq(null != pageReq.getDictTypeCode(), SysDictItem::getDictTypeCode, pageReq.getDictTypeCode())
                .like(StringUtils.hasText(pageReq.getLabel()), SysDictItem::getLabel, pageReq.getLabel())
                .like(StringUtils.hasText(pageReq.getValue()), SysDictItem::getValue, pageReq.getValue())
                .orderByAsc(SysDictItem::getSort)
                .page(pageReq.page());

        return PageResult.of(page, DictItemResp.class);
    }

    /**
     * 字典项表详情
     *
     * @param id 字典项表ID
     * @return 字典项表详情
     */
    @Override
    public DictItemResp detail(Integer id) {
        return BeanUtils.copy(this.getById(id), DictItemResp.class);
    }

    /**
     * 新增字典项表
     *
     * @param formReq 字典项表新增参数
     * @return 字典项表新增结果
     */
    @Override
    public Boolean add(DictItemFormReq formReq) {
        return this.save(BeanUtils.copy(formReq, SysDictItem.class));
    }


    /**
     * 修改字典项表
     *
     * @param formReq 字典项表修改参数
     * @return 字典项表修改结果
     */
    @Override
    public Boolean update(DictItemFormReq formReq) {
        return this.updateById(BeanUtils.copy(formReq, SysDictItem.class));
    }

    /**
     * 删除字典项表
     *
     * @param ids 字典项表 ID列表
     * @return 字典项表删除结果
     */
    @Override
    @Transactional
    public Boolean delete(Long[] ids) {
        return this.removeByIds(Arrays.asList(ids));
    }

}
