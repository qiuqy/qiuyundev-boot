package com.qiuyundev.boot.service.impl;

import com.qiuyundev.boot.domain.entity.SysUser;
import com.qiuyundev.boot.mapper.DataScopeDemoMapper;
import com.qiuyundev.boot.service.IDataScopeDemoService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class DataScopeDemoServiceImpl implements IDataScopeDemoService {
    private final DataScopeDemoMapper dataScopeDemoMapper;
    /**
     * 自定义数据权限演示
     * @return
     */
    @Override
    public SysUser custom(Integer id) {
        return this.dataScopeDemoMapper.selectById(id);
    }

    /**
     * 本人部门数据权限演示
     */
    @Override
    public List<SysUser> selfDept(Integer id) {
        return this.dataScopeDemoMapper.list(id);
    }

    /**
     * 本人部门及下级部门数据权限演示
     */
    @Override
    public List<SysUser> selfDeptAndChildren(List<Integer> ids) {
        return this.dataScopeDemoMapper.selectBatchIds(ids);
    }

    /**
     * 仅本人数据权限
     */
    @Override
    public List<Map<String, Object>> dataScopeDemoSelf() {
        return this.dataScopeDemoMapper.selectOperationLog();
    }
}
