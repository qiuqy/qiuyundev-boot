package com.qiuyundev.boot.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qiuyundev.boot.base.util.BeanUtils;
import com.qiuyundev.boot.base.vo.PageResult;
import com.qiuyundev.boot.core.datascope.DataScopeEnum;
import com.qiuyundev.boot.core.sse.SseMessage;
import com.qiuyundev.boot.core.sse.SseService;
import com.qiuyundev.boot.domain.entity.SysRole;
import com.qiuyundev.boot.domain.entity.SysRoleDept;
import com.qiuyundev.boot.domain.entity.SysRoleMenu;
import com.qiuyundev.boot.domain.vo.role.RoleBriefResp;
import com.qiuyundev.boot.domain.vo.role.RoleFormReq;
import com.qiuyundev.boot.domain.vo.role.RolePageReq;
import com.qiuyundev.boot.domain.vo.role.RoleResp;
import com.qiuyundev.boot.mapper.SysRoleMapper;
import com.qiuyundev.boot.service.ISysRoleDeptService;
import com.qiuyundev.boot.service.ISysRoleMenuService;
import com.qiuyundev.boot.service.ISysRoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * 角色 服务实现类
 * </p>
 *
 * @author Qiuyun
 * @since 2024/04/12
 */
@Service
@RequiredArgsConstructor
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {
    private final ISysRoleMenuService roleMenuService;
    private final SseService sseService;
    private final ISysRoleDeptService roleDeptService;

//    public List<Long> getCustom(Long roleId) {
//        return this.roleDeptService
//                .list(Wrappers.<RoleDept>lambdaQuery().eq(RoleDept::getRoleId, roleId))
//                .stream()
//                .map(RoleDept::getDeptId)
//                .collect(Collectors.toList());
//    }

    @Override
    public List<SysRole> findByUserId(Integer userId) {
        return this.baseMapper.findByUserId(userId);
    }

    /**
     * 分页查询角色
     *
     * @param rolePageReq 角色分页查询参数
     * @return 角色列表
     */
    @Override
    public PageResult<RoleResp> page(RolePageReq rolePageReq) {
        IPage<SysRole> sysRoleIPage = this.lambdaQuery()
                .like(StringUtils.hasText(rolePageReq.getName()), SysRole::getName, rolePageReq.getName())
                .like(StringUtils.hasText(rolePageReq.getCode()), SysRole::getCode, rolePageReq.getCode())
                .eq(null != rolePageReq.getStatus(), SysRole::getStatus, rolePageReq.getStatus())
                .like(StringUtils.hasText(rolePageReq.getRemark()), SysRole::getRemark, rolePageReq.getRemark())
                .orderByDesc(SysRole::getUpdateTime)
                .page(rolePageReq.page());

        return PageResult.of(sysRoleIPage, RoleResp.class);
    }

    /**
     * 角色详情
     *
     * @param id 角色 ID
     * @return 角色详情
     */
    @Override
    public RoleResp detail(Integer id) {
        RoleResp roleResp = BeanUtils.copy(this.getById(id), RoleResp.class);
        // 获取当前角色的菜单ID列表
        List<Integer> menuIds = this.roleMenuService.lambdaQuery()
                .eq(SysRoleMenu::getRoleId, id)
                .list()
                .stream()
                .map(SysRoleMenu::getMenuId)
                .toList();
        roleResp.setMenuIds(menuIds);

        // 获取当前角色自定义部门权限的部门ID列表
        if (roleResp.getDataScope() == DataScopeEnum.CUSTOM) {
            List<Integer> deptIds = this.roleDeptService.lambdaQuery()
                    .eq(SysRoleDept::getRoleId, id)
                    .list()
                    .stream()
                    .map(SysRoleDept::getDeptId)
                    .toList();
            roleResp.setDeptIds(deptIds);
        }

        return roleResp;
    }

    /**
     * 新增角色
     *
     * @param roleFormReq 角色新增参数
     * @return 角色新增结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean add(RoleFormReq roleFormReq) {
        // 判断名称或者标识是否重复 todo
        SysRole sysRole = BeanUtils.copy(roleFormReq, SysRole.class);
        boolean saved = this.save(sysRole);
        if (saved) {
            this.roleMenuService.saveRoleMenuList(roleFormReq.getMenuIds(), sysRole.getId(), false);
            if (roleFormReq.getDataScope() != DataScopeEnum.CUSTOM) {
                roleFormReq.setDeptIds(null);
            }
            this.roleDeptService.saveRoleDeptList(roleFormReq.getDeptIds(), sysRole.getId(), false);
            this.sseService.sendByType(SseMessage.SseMessageTypeEnum.updatePermsAndMenus);
        }

        return saved;
    }


    /**
     * 修改角色
     *
     * @param roleFormReq 角色修改参数
     * @return 角色修改结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean update(RoleFormReq roleFormReq) {
        boolean update = this.updateById(BeanUtils.copy(roleFormReq, SysRole.class));
        if (update) {
            this.roleMenuService.saveRoleMenuList(roleFormReq.getMenuIds(), roleFormReq.getId(), true);
            if (roleFormReq.getDataScope() != DataScopeEnum.CUSTOM) {
                roleFormReq.setDeptIds(null);
            }
            this.roleDeptService.saveRoleDeptList(roleFormReq.getDeptIds(), roleFormReq.getId(), true);
            this.sseService.sendByType(SseMessage.SseMessageTypeEnum.updatePermsAndMenus);
        }
        return update;
    }

    /**
     * 删除角色
     *
     * @param ids 角色 ID 列表
     * @return 角色删除结果
     */
    @Override
    @Transactional
    public Boolean delete(Long[] ids) {
        boolean removed = this.removeByIds(Arrays.asList(ids));
        if (removed) {
            this.sseService.sendByType(SseMessage.SseMessageTypeEnum.updatePermsAndMenus);
        }
        return removed;
    }


    /**
     * 获取全部角色数据的精简列表
     *
     * @return 精简角色列表
     */
    @Override
    public List<RoleBriefResp> options() {
        List<SysRole> options = this.lambdaQuery()
                .select(SysRole::getId, SysRole::getName)
                .list();
        return BeanUtils.copyList(options, RoleBriefResp.class);
    }

}
