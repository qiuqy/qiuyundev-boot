package com.qiuyundev.boot.service.impl;

import com.qiuyundev.boot.base.util.BeanUtils;
import com.qiuyundev.boot.core.db.DatabaseService;
import com.qiuyundev.boot.core.db.DatabaseSupplier;
import com.qiuyundev.boot.core.db.domain.TableColumnInfo;
import com.qiuyundev.boot.core.db.domain.TableInfo;
import com.qiuyundev.boot.domain.entity.SysGeneratorConfig;
import com.qiuyundev.boot.domain.vo.generator.TableColumnInfoReq;
import com.qiuyundev.boot.domain.vo.generator.TableColumnInfoResp;
import com.qiuyundev.boot.service.ICodeGeneratorService;
import com.qiuyundev.boot.service.ISysGeneratorConfigService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CodeGeneratorServiceImpl implements ICodeGeneratorService {
    private final DatabaseService databaseService;
    private final DatabaseSupplier databaseSupplier;
    private final ISysGeneratorConfigService generatorConfigService;

    @Override
    public List<TableInfo> selectTables(String tableName) {
        return this.databaseService.selectTables(databaseSupplier.getDatabaseName(), tableName);
    }

    /**
     * 查询表字段信息
     *
     * @return 数据库所有表
     */
    @Override
    public List<TableColumnInfoResp> selectTableColumns(String tableName) {
        List<TableColumnInfo> tableColumnInfos = this.databaseService.selectTableColumns(databaseSupplier.getDatabaseName(), tableName);
        List<TableColumnInfoResp> tableColumnInfoResps = BeanUtils.copyList(tableColumnInfos, TableColumnInfoResp.class);
        // 查询该表是由已有配置
        this.generatorConfigService.lambdaQuery()
                .eq(SysGeneratorConfig::getTableName, tableName).oneOpt()
                .ifPresent(sysGeneratorConfig -> {
                    tableColumnInfoResps.forEach(tableColumnInfoResp -> {
                        if (!ObjectUtils.isEmpty(sysGeneratorConfig.getFormColumn())) {
                            if (sysGeneratorConfig.getFormColumn().contains(tableColumnInfoResp.getColumnName())) {
                                tableColumnInfoResp.setIsFormColumn(true);
                            }
                        }
                        if (!ObjectUtils.isEmpty(sysGeneratorConfig.getRespColumn())) {
                            if (sysGeneratorConfig.getRespColumn().contains(tableColumnInfoResp.getColumnName())) {
                                tableColumnInfoResp.setIsRespColumn(true);
                            }
                        }
                        if (!ObjectUtils.isEmpty(sysGeneratorConfig.getQueryColumn())) {
                            if (sysGeneratorConfig.getQueryColumn().contains(tableColumnInfoResp.getColumnName())) {
                                tableColumnInfoResp.setIsQueryColumn(true);
                            }
                        }
                    });
                });
        return tableColumnInfoResps;
    }

    @Override
    public Boolean saveOrUpdateConfig(List<TableColumnInfoReq> list) {
        if (ObjectUtils.isEmpty(list)) {
            return false;
        }
        SysGeneratorConfig config = new SysGeneratorConfig();
        config.setTableName(list.getFirst().getTableName());
        config.setFormColumn(list.stream().filter(TableColumnInfoReq::getIsFormColumn).map(TableColumnInfoReq::getColumnName).toList());
        config.setRespColumn(list.stream().filter(TableColumnInfoReq::getIsRespColumn).map(TableColumnInfoReq::getColumnName).toList());
        config.setQueryColumn(list.stream().filter(TableColumnInfoReq::getIsQueryColumn).map(TableColumnInfoReq::getColumnName).toList());
        return this.generatorConfigService.saveOrUpdate(config);
    }

}
