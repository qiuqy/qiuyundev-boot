package com.qiuyundev.boot.service.impl;

import com.qiuyundev.boot.domain.entity.SysDictType;
import com.qiuyundev.boot.mapper.SysDictTypeMapper;
import com.qiuyundev.boot.service.ISysDictTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;
import com.baomidou.mybatisplus.core.metadata.IPage;

import java.util.Arrays;
import java.util.List;

import com.qiuyundev.boot.base.util.BeanUtils;
import com.qiuyundev.boot.base.vo.PageResult;
import com.qiuyundev.boot.domain.vo.dict.DictTypeFormReq;
import com.qiuyundev.boot.domain.vo.dict.DictTypePageReq;
import com.qiuyundev.boot.domain.vo.dict.DictTypeResp;
import org.springframework.util.StringUtils;

/**
 * <p>
 * 字典类型 服务实现类
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-13
 */
@Service
public class SysDictTypeServiceImpl extends ServiceImpl<SysDictTypeMapper, SysDictType> implements ISysDictTypeService {
    /**
     * 分页查询字典类型
     *
     * @param pageReq 字典类型分页查询参数
     * @return 字典类型列表
     */
    @Override
    public PageResult<DictTypeResp> page(DictTypePageReq pageReq) {
        IPage<SysDictType> page = this.lambdaQuery()
                .like(StringUtils.hasText(pageReq.getName()), SysDictType::getName, pageReq.getName())
                .like(StringUtils.hasText(pageReq.getCode()), SysDictType::getCode, pageReq.getCode())
                .orderByDesc(SysDictType::getUpdateTime)
                .page(pageReq.page());

        return PageResult.of(page, DictTypeResp.class);
    }

    /**
     * 字典类型详情
     *
     * @param id 字典类型ID
     * @return 字典类型详情
     */
    @Override
    public DictTypeResp detail(Integer id) {
        return BeanUtils.copy(this.getById(id), DictTypeResp.class);
    }

    /**
     * 新增字典类型
     *
     * @param formReq 字典类型新增参数
     * @return 字典类型新增结果
     */
    @Override
    public Boolean add(DictTypeFormReq formReq) {
        return this.save(BeanUtils.copy(formReq, SysDictType.class));
    }


    /**
     * 修改字典类型
     *
     * @param formReq 字典类型修改参数
     * @return 字典类型修改结果
     */
    @Override
    public Boolean update(DictTypeFormReq formReq) {
        return this.updateById(BeanUtils.copy(formReq, SysDictType.class));
    }

    /**
     * 删除字典类型
     *
     * @param ids 字典类型 ID列表
     * @return 字典类型删除结果
     */
    @Override
    @Transactional
    public Boolean delete(Long[] ids) {
        return this.removeByIds(Arrays.asList(ids));
    }

    /**
     * 字典类型下拉选
     *
     * @return 字典类型下拉选
     */
    @Override
    public List<DictTypeResp> options() {
        List<SysDictType> list = this.lambdaQuery()
                .orderByDesc(SysDictType::getId)
                .list();
        return BeanUtils.copyList(list, DictTypeResp.class);
    }

}
