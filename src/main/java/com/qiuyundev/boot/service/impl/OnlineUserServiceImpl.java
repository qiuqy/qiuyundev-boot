package com.qiuyundev.boot.service.impl;

import com.qiuyundev.boot.base.util.BeanUtils;
import com.qiuyundev.boot.base.vo.PageResult;
import com.qiuyundev.boot.core.security.JwtService;
import com.qiuyundev.boot.core.security.LoginUser;
import com.qiuyundev.boot.domain.vo.onlineuser.OnlinePageReq;
import com.qiuyundev.boot.domain.vo.onlineuser.OnlineUserResp;
import com.qiuyundev.boot.service.IOnlineUserService;
import io.jsonwebtoken.Claims;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.qiuyundev.boot.core.security.SecurityConstants.CLAIM_USER_KEY;

@Service
@RequiredArgsConstructor
public class OnlineUserServiceImpl implements IOnlineUserService {
    public static final String ONLINE_USER_KEY = "LOGIN:ONLINE_USER";
    private final JwtService jwtService;

    @Resource
    private RedisTemplate<String, LoginUser.LoginInfo> redisTemplate;

    /**
     * 新增或更新在线用户
     *
     * @param loginInfo 登录信息
     * @param score     过期时间
     */
    //    @Async
    @Override
    public void addOrUpdateOnlineUser(LoginUser.LoginInfo loginInfo, double score) {
        redisTemplate.opsForZSet().add(ONLINE_USER_KEY, loginInfo, score);
    }

    /**
     * 删除在线用户
     *
     * @param loginInfo 登录信息
     */
    @Override
    public void deleteOnlineUser(LoginUser.LoginInfo loginInfo) {
        redisTemplate.opsForZSet().remove(ONLINE_USER_KEY, loginInfo);
    }

    @Override
    public PageResult<OnlineUserResp> page(OnlinePageReq onlinePageReq) {
        // 分页从 zset 中查询在线用户
        int startIndex = (onlinePageReq.getPage() - 1) * onlinePageReq.getPageSize();
        int endIndex = startIndex + onlinePageReq.getPageSize() - 1;
        Long total = 0L;
        Set<ZSetOperations.TypedTuple<LoginUser.LoginInfo>> typedTuples;
        if (StringUtils.hasText(onlinePageReq.getUsername())
                || StringUtils.hasText(onlinePageReq.getIp())
                || StringUtils.hasText(onlinePageReq.getLocation())) {
            // 查询所有
            typedTuples = this.redisTemplate.opsForZSet()
                    .reverseRangeWithScores(ONLINE_USER_KEY, 0, -1);
            assert typedTuples != null;
            typedTuples = typedTuples.stream().filter(e -> {
                        LoginUser.LoginInfo loginInfo = e.getValue();
                        assert loginInfo != null;
                        boolean usernameFlag = true;
                        if (StringUtils.hasText(onlinePageReq.getUsername())) {
                            usernameFlag = loginInfo.getUsername().contains(onlinePageReq.getUsername());
                        }
                        boolean ipFlag = true;
                        if (StringUtils.hasText(onlinePageReq.getIp())) {
                            ipFlag = loginInfo.getIp().contains(onlinePageReq.getIp());
                        }
                        boolean locationFlag = true;
                        if (StringUtils.hasText(onlinePageReq.getLocation())) {
                            locationFlag = loginInfo.getLocation().contains(onlinePageReq.getLocation());
                        }
                        return usernameFlag && ipFlag && locationFlag;
                    }).skip(startIndex)
                    .limit(onlinePageReq.getPageSize())
                    .collect(Collectors.toSet());
        } else {
            // 统计总数
            total = this.redisTemplate.opsForZSet().zCard(ONLINE_USER_KEY);
            typedTuples = this.redisTemplate.opsForZSet()
                    .reverseRangeWithScores(ONLINE_USER_KEY, startIndex, endIndex);
        }


        List<OnlineUserResp> onlineUserRespList = new ArrayList<>();
        if (null != typedTuples) {
            // 获取当前 userKey
            String userKey;
            ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            if (requestAttributes != null) {
                HttpServletRequest request = requestAttributes.getRequest();
                String token = request.getHeader(HttpHeaders.AUTHORIZATION);
                Claims claims = this.jwtService.parseToken(token);
                // 解析对应的权限以及用户信息
                userKey = (String) claims.get(CLAIM_USER_KEY);
            } else {
                userKey = null;
            }

            onlineUserRespList = typedTuples.stream().map(e -> {
                LoginUser.LoginInfo loginInfo = e.getValue();
                OnlineUserResp onlineUserResp = BeanUtils.copy(loginInfo, OnlineUserResp.class);
                assert loginInfo != null;
                boolean isCurrent = loginInfo.getUserKey().equals(userKey);
                onlineUserResp.setIsCurrent(isCurrent);
                // 超级管理员和自己不允许踢下线
//                onlineUserResp.setDisable(SecurityUtils.isAdmin() || isCurrent);
                return onlineUserResp;
            }).toList();
        }

        return PageResult.of(onlinePageReq.getPage(), onlinePageReq.getPageSize(), null == total ? 0 : total, onlineUserRespList);
    }

    @Override
    public Boolean kick(String userKey) {
        jwtService.kickUser(userKey);
        Set<ZSetOperations.TypedTuple<LoginUser.LoginInfo>> typedTuples = this.redisTemplate
                .opsForZSet()
                .reverseRangeWithScores(ONLINE_USER_KEY, 0, -1);
        if (null != typedTuples) {
            for (ZSetOperations.TypedTuple<LoginUser.LoginInfo> typedTuple : typedTuples) {
                LoginUser.LoginInfo loginInfo = typedTuple.getValue();
                if (loginInfo != null && loginInfo.getUserKey().equals(userKey)) {
                    this.redisTemplate.opsForZSet().remove(ONLINE_USER_KEY, loginInfo);
                    return true;
                }
            }
        }

        return false;
    }


    public void cleanup() {
        // 清除即将过期的，删除提前一分钟，保证获取在线用户的时候获取用户信息缓存不会失败。
        redisTemplate.opsForZSet().removeRangeByScore(ONLINE_USER_KEY, 0, System.currentTimeMillis() + 60000);
    }
}
