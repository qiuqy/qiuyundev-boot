package com.qiuyundev.boot.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qiuyundev.boot.domain.entity.SysRoleDept;
import com.qiuyundev.boot.mapper.SysRoleDeptMapper;
import com.qiuyundev.boot.service.ISysRoleDeptService;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;

/**
 * <p>
 * 角色部门 服务实现类
 * </p>
 *
 * @author Qiuyun
 * @since 2024/04/12
 */
@Service
public class RoleDeptServiceImpl extends ServiceImpl<SysRoleDeptMapper, SysRoleDept> implements ISysRoleDeptService {
    @Override
    public boolean saveRoleDeptList(List<Integer> deptIds, Integer roleId, boolean updateFlag) {
        if (updateFlag) {
            this.lambdaUpdate()
                    .eq(SysRoleDept::getRoleId, roleId)
                    .remove();
        }
        // 保存菜单
        if (ObjectUtils.isEmpty(deptIds)) {
            return false;
        }

        return this.saveBatch(deptIds.stream()
                .map(deptId -> {
                    SysRoleDept sysRoleDept = new SysRoleDept();
                    sysRoleDept.setRoleId(roleId);
                    sysRoleDept.setDeptId(deptId);
                    return sysRoleDept;
                })
                .toList());
    }
}
