package com.qiuyundev.boot.service.impl;

import cn.hutool.core.lang.Pair;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qiuyundev.boot.base.util.BeanUtils;
import com.qiuyundev.boot.core.sse.SseMessage;
import com.qiuyundev.boot.core.sse.SseService;
import com.qiuyundev.boot.domain.entity.SysMenu;
import com.qiuyundev.boot.domain.enums.menu.ExternalOpenModeEnum;
import com.qiuyundev.boot.domain.enums.menu.MenuTypeEnum;
import com.qiuyundev.boot.domain.vo.TreeVO;
import com.qiuyundev.boot.domain.vo.account.AccountMenuResp;
import com.qiuyundev.boot.domain.vo.menu.MenuFormReq;
import com.qiuyundev.boot.domain.vo.menu.MenuListReq;
import com.qiuyundev.boot.domain.vo.menu.MenuListResp;
import com.qiuyundev.boot.domain.vo.menu.MenuResp;
import com.qiuyundev.boot.mapper.SysMenuMapper;
import com.qiuyundev.boot.service.ISysMenuService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 菜单 服务实现类
 * </p>
 *
 * @author Qiuyun
 * @since 2024/04/12
 */
@Service
@RequiredArgsConstructor
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements ISysMenuService {
    private final SseService sseService;
    /**
     * 分页查询菜单
     *
     * @param menuListReq 菜单分页查询参数
     * @return 菜单列表
     */
    @Override
    public List<TreeVO.TreeResp> list(MenuListReq menuListReq) {
        // 如果搜索条件全部为空，则查出全部，生成树形结构
        if (!StringUtils.hasText(menuListReq.getName()) && !StringUtils.hasText(menuListReq.getPath()) && !StringUtils.hasText(menuListReq.getComponent())) {
            List<TreeVO.TreeResp> allMenu = this.lambdaQuery()
                    .list()
                    .stream()
                    .map(sysMenu -> (TreeVO.TreeResp) BeanUtils.copy(sysMenu, MenuListResp.class))
                    .toList();
            return TreeVO.buildTree(allMenu);
        }
        return this.lambdaQuery()
                .like(StringUtils.hasText(menuListReq.getName()), SysMenu::getName, menuListReq.getName())
                .like(StringUtils.hasText(menuListReq.getPath()), SysMenu::getPath, menuListReq.getPath())
                .like(StringUtils.hasText(menuListReq.getComponent()), SysMenu::getComponent, menuListReq.getComponent())
                .list()
                .stream()
                .map(sysMenu -> (TreeVO.TreeResp) BeanUtils.copy(sysMenu, MenuListResp.class))
                .toList();
    }

    /**
     * 菜单详情
     *
     * @param id 菜单 ID
     * @return 菜单详情
     */
    @Override
    public MenuResp detail(Integer id) {
        return BeanUtils.copy(this.getById(id), MenuResp.class);
    }

    /**
     * 新增菜单
     *
     * @param menuFormReq 菜单新增参数
     * @return 菜单新增结果
     */
    @Override
    public Boolean add(MenuFormReq menuFormReq) {
        return this.save(BeanUtils.copy(menuFormReq, SysMenu.class));
    }


    /**
     * 修改菜单
     *
     * @param menuFormReq 菜单修改参数
     * @return 菜单修改结果
     */
    @Override
    public Boolean update(MenuFormReq menuFormReq) {
        boolean update = this.lambdaUpdate()
                .set(SysMenu::getParentId, menuFormReq.getParentId())
                .set(SysMenu::getName, menuFormReq.getName())
                .set(SysMenu::getIcon, menuFormReq.getIcon())
                .set(SysMenu::getType, menuFormReq.getType())
                .set(SysMenu::getPath, menuFormReq.getPath())
                .set(SysMenu::getComponent, menuFormReq.getComponent())
                .set(SysMenu::getPermission, menuFormReq.getPermission())
                .set(SysMenu::getSort, menuFormReq.getSort())
                .set(SysMenu::getIsKeepAlive, menuFormReq.getIsKeepAlive())
                .set(SysMenu::getIsShow, menuFormReq.getIsShow())
                .set(SysMenu::getActiveMenu, menuFormReq.getActiveMenu())
                .set(SysMenu::getIsExternal, menuFormReq.getIsExternal())
                .set(SysMenu::getExternalOpenMode, menuFormReq.getExternalOpenMode())
                .set(SysMenu::getStatus, menuFormReq.getStatus())
                .eq(SysMenu::getId, menuFormReq.getId())
                .update();
        if (update) {
            this.sseService.sendByType(SseMessage.SseMessageTypeEnum.updatePermsAndMenus);
        }
        return update;
    }

    /**
     * 删除菜单
     *
     * @param ids 菜单 ID 列表
     * @return 菜单删除结果
     */
    @Override
    @Transactional
    public Boolean delete(Long[] ids) {
        // todo 判断菜单是否已绑定角色
        boolean removed = this.removeByIds(Arrays.asList(ids));
        if (removed) {
            this.sseService.sendByType(SseMessage.SseMessageTypeEnum.updatePermsAndMenus);
        }
        return removed;
    }

    @Override
    public List<TreeVO.TreeResp> tree(TreeVO.TreeReq treeReq) {
        // 查出全部部门数据
        List<TreeVO.TreeResp> all = this.list()
                .stream()
                .map(e -> BeanUtils.copy(e, TreeVO.TreeResp.class))
                .toList();
        return TreeVO.searchTree(treeReq, all);
    }


    /**
     * 通过用户 id 查询用户权限
     *
     * @param userId 用户id
     * @return 用户权限列表
     */
    @Override
    public List<String> selectPermsByUserId(Integer userId) {
        return this.baseMapper.selectPermsByUserId(userId);
    }

    /**
     * 根据用户ID查询用户菜单权限
     * @param userId 用户id
     * @return 用户菜单权限
     */
    @Override
    public List<AccountMenuResp> selectMenusByUserId(Integer userId) {
        List<SysMenu> sysMenus = this.baseMapper.selectMenusByUserIdAndType(userId, Arrays.asList(MenuTypeEnum.DIRECTORY, MenuTypeEnum.MENU));
        List<AccountMenuResp> menus = sysMenus.stream()
                .map(sysMenu -> {
                    AccountMenuResp accountMenuResp = BeanUtils.copy(sysMenu, AccountMenuResp.class);
                    AccountMenuResp.MenuMetaResp menuMetaResp = BeanUtils.copy(sysMenu, AccountMenuResp.MenuMetaResp.class);
                    menuMetaResp.setTitle(sysMenu.getName());
                    accountMenuResp.setMeta(menuMetaResp);
                    return accountMenuResp;
                })
                .toList();
        return buildTree(menus);
    }

    @Override
    public Pair<List<AccountMenuResp>, List<String>> selectMenusAndPermsByUserId(Integer userId) {
        List<SysMenu> sysMenus = this.baseMapper.selectMenusByUserIdAndType(userId, null);
        // 菜单树形列表
        List<AccountMenuResp> menus = sysMenus.stream()
                .filter(e -> e.getType() == MenuTypeEnum.DIRECTORY || e.getType() == MenuTypeEnum.MENU)
                .map(sysMenu -> {
                    AccountMenuResp accountMenuResp = BeanUtils.copy(sysMenu, AccountMenuResp.class);
                    AccountMenuResp.MenuMetaResp menuMetaResp = BeanUtils.copy(sysMenu, AccountMenuResp.MenuMetaResp.class);
                    menuMetaResp.setTitle(sysMenu.getName());
                    accountMenuResp.setMeta(menuMetaResp);
                    return accountMenuResp;
                })
                .toList();
        // 获取权限集合
        List<String> prems = sysMenus.stream()
                .map(SysMenu::getPermission)
                .filter(StringUtils::hasText)
                .toList();
        return Pair.of(buildTree(menus), prems);
    }

    /**
     * 递归获取子节点
     *
     * @param parentTreeResp 父节点
     * @param all            所有数据
     * @return 子节点数据
     */
    private static List<AccountMenuResp> recursion(AccountMenuResp parentTreeResp, List<AccountMenuResp> all) {
//        if (parentTreeResp.getMeta().getIsExternal() && parentTreeResp.getMeta().getExternalOpenMode() == ExternalOpenModeEnum.NEW_WINDOW) {
//            parentTreeResp.setRedirect(parentTreeResp.getPath());
//        }
        return all.stream()
                .filter(treeResp -> parentTreeResp.getId().equals(treeResp.getParentId()))
                .peek(treeResp -> {
                    List<AccountMenuResp> children = recursion(treeResp, all);
                    if (!children.isEmpty()) {
                        treeResp.setChildren(children);
                        children.stream()
                                .filter(e -> !e.getMeta().getIsExternal())
                                .findFirst()
                                .ifPresent(e -> treeResp.setRedirect(e.getPath()));
                    }
                })
                .sorted(Comparator.comparingInt(treeResp -> (treeResp.getSort() == null ? 0 : treeResp.getSort())))
                .collect(Collectors.toList());
    }


    public static List<AccountMenuResp> buildTree(List<AccountMenuResp> treeRespList) {
        return treeRespList.stream()
                .filter(treeResp -> null == treeResp.getParentId())
                .peek(treeResp -> {
                    List<AccountMenuResp> children = recursion(treeResp, treeRespList);
                    if (!children.isEmpty()) {
                        treeResp.setChildren(children);
                        children.stream()
                                .filter(e -> !e.getMeta().getIsExternal())
                                .findFirst()
                                .ifPresent(e -> treeResp.setRedirect(e.getPath()));
                    }
                })
                .sorted(Comparator.comparingInt(c -> (c.getSort() == null ? 0 : c.getSort())))
                .toList();
    }

    @Override
    public List<String> permissions() {
        return this.lambdaQuery()
                .select(SysMenu::getPermission)
                .isNotNull(SysMenu::getPermission)
                .ne(SysMenu::getPermission, "")
                .list()
                .stream()
                .map(SysMenu::getPermission)
                .toList();
    }


}
