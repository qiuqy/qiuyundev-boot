package com.qiuyundev.boot.service.impl;

import com.qiuyundev.boot.domain.entity.SysPost;
import com.qiuyundev.boot.domain.vo.post.PostBriefResp;
import com.qiuyundev.boot.mapper.SysPostMapper;
import com.qiuyundev.boot.service.ISysPostService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;
import com.baomidou.mybatisplus.core.metadata.IPage;

import java.util.Arrays;
import java.util.List;

import com.qiuyundev.boot.base.util.BeanUtils;
import com.qiuyundev.boot.base.vo.PageResult;
import com.qiuyundev.boot.domain.vo.post.PostFormReq;
import com.qiuyundev.boot.domain.vo.post.PostPageReq;
import com.qiuyundev.boot.domain.vo.post.PostResp;
import org.springframework.util.StringUtils;

/**
 * <p>
 * 职位 服务实现类
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-07
 */
@Service
public class SysPostServiceImpl extends ServiceImpl<SysPostMapper, SysPost> implements ISysPostService {
    /**
     * 分页查询职位
     *
     * @param pageReq 职位分页查询参数
     * @return 职位列表
     */
    @Override
    public PageResult<PostResp> page(PostPageReq pageReq) {
        IPage<SysPost> page = this.lambdaQuery()
                .like(StringUtils.hasText(pageReq.getName()), SysPost::getName, pageReq.getName())
                .orderByDesc(SysPost::getUpdateTime)
                .page(pageReq.page());

        return PageResult.of(page, PostResp.class);
    }

    /**
     * 职位详情
     *
     * @param id 职位ID
     * @return 职位详情
     */
    @Override
    public PostResp detail(Integer id) {
        return BeanUtils.copy(this.getById(id), PostResp.class);
    }

    /**
     * 新增职位
     *
     * @param formReq 职位新增参数
     * @return 职位新增结果
     */
    @Override
    public Boolean add(PostFormReq formReq) {
        return this.save(BeanUtils.copy(formReq, SysPost.class));
    }


    /**
     * 修改职位
     *
     * @param formReq 职位修改参数
     * @return 职位修改结果
     */
    @Override
    public Boolean update(PostFormReq formReq) {
        return this.updateById(BeanUtils.copy(formReq, SysPost.class));
    }

    /**
     * 删除职位
     *
     * @param ids 职位 ID列表
     * @return 职位删除结果
     */
    @Override
    @Transactional
    public Boolean delete(Long[] ids) {
        return this.removeByIds(Arrays.asList(ids));
    }

    @Override
    public List<PostBriefResp> options() {
        List<SysPost> list = this.lambdaQuery()
                .select(SysPost::getId, SysPost::getName)
                .orderByDesc(SysPost::getId)
                .list();

        return BeanUtils.copyList(list, PostBriefResp.class);
    }

    @Override
    public List<SysPost> findByUserId(Integer userId) {
        return this.baseMapper.findByUserId(userId);
    }

}
