package com.qiuyundev.boot.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qiuyundev.boot.base.util.BeanUtils;
import com.qiuyundev.boot.base.vo.PageResult;
import com.qiuyundev.boot.domain.entity.SysConfig;
import com.qiuyundev.boot.domain.vo.config.ConfigFormReq;
import com.qiuyundev.boot.domain.vo.config.ConfigPageReq;
import com.qiuyundev.boot.domain.vo.config.ConfigResp;
import com.qiuyundev.boot.mapper.SysConfigMapper;
import com.qiuyundev.boot.service.ISysConfigService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Arrays;

/**
 * <p>
 * 参数配置 服务实现类
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-06
 */
@Service
public class SysConfigServiceImpl extends ServiceImpl<SysConfigMapper, SysConfig> implements ISysConfigService {
    /**
     * 分页查询参数配置
     *
     * @param pageReq 参数配置分页查询参数
     * @return 参数配置列表
     */
    @Override
    public PageResult<ConfigResp> page(ConfigPageReq pageReq) {
        IPage<SysConfig> page = this.lambdaQuery()
                .like(StringUtils.hasText(pageReq.getName()), SysConfig::getName, pageReq.getName())
                .orderByDesc(SysConfig::getUpdateTime)
                .page(pageReq.page());

        return PageResult.of(page, ConfigResp.class);
    }

    /**
     * 参数配置详情
     *
     * @param id 参数配置ID
     * @return 参数配置详情
     */
    @Override
    public ConfigResp detail(Integer id) {
        return BeanUtils.copy(this.getById(id), ConfigResp.class);
    }

    /**
     * 新增参数配置
     *
     * @param formReq 参数配置新增参数
     * @return 参数配置新增结果
     */
    @Override
    public Boolean add(ConfigFormReq formReq) {
        return this.save(BeanUtils.copy(formReq, SysConfig.class));
    }


    /**
     * 修改参数配置
     *
     * @param formReq 参数配置修改参数
     * @return 参数配置修改结果
     */
    @Override
    public Boolean update(ConfigFormReq formReq) {
        return this.updateById(BeanUtils.copy(formReq, SysConfig.class));
    }

    /**
     * 删除参数配置
     *
     * @param ids 参数配置 ID列表
     * @return 参数配置删除结果
     */
    @Override
    @Transactional
    public Boolean delete(Long[] ids) {
        return this.removeByIds(Arrays.asList(ids));
    }

}
