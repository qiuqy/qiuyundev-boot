package com.qiuyundev.boot.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qiuyundev.boot.base.util.BeanUtils;
import com.qiuyundev.boot.base.vo.PageResult;
import com.qiuyundev.boot.core.security.LoginUser;
import com.qiuyundev.boot.core.sse.SseMessage;
import com.qiuyundev.boot.core.sse.SseService;
import com.qiuyundev.boot.domain.entity.*;
import com.qiuyundev.boot.domain.vo.dept.DeptBriefResp;
import com.qiuyundev.boot.domain.vo.post.PostBriefResp;
import com.qiuyundev.boot.domain.vo.role.RoleBriefResp;
import com.qiuyundev.boot.domain.vo.user.UserFormReq;
import com.qiuyundev.boot.domain.vo.user.UserPageReq;
import com.qiuyundev.boot.domain.vo.user.UserResp;
import com.qiuyundev.boot.mapper.SysUserMapper;
import com.qiuyundev.boot.service.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * 用户 服务实现类
 * </p>
 *
 * @author Qiuyun
 * @since 2024/04/12
 */
@Service
@RequiredArgsConstructor
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements ISysUserService {
    private final ISysDeptService deptService;
    private final ISysRoleService roleService;
    private final ISysUserRoleService userRoleService;
    private final ISysPostService postService;
    private final ISysUserPostService userPostService;
    private final SseService sseService;


    /**
     * 分页查询用户
     *
     * @param userPageReq 用户分页查询参数
     * @return 用户列表
     */
    @Override
    public PageResult<UserResp> page(UserPageReq userPageReq) {
        // 部门需要查询子级部门以及所有子级部门的数据
        List<Integer> deptIds = new ArrayList<>();
        if (null != userPageReq.getDeptId()) {
            deptIds = this.deptService.findSelfAndChildrenIds(userPageReq.getDeptId());
        }
        IPage<SysUser> sysUserIPage = this.lambdaQuery()
                .like(StringUtils.hasText(userPageReq.getUsername()), SysUser::getUsername, userPageReq.getUsername())
                .like(StringUtils.hasText(userPageReq.getNickname()), SysUser::getNickname, userPageReq.getNickname())
                .in(!ObjectUtils.isEmpty(deptIds), SysUser::getDeptId, deptIds)
                .like(StringUtils.hasText(userPageReq.getEmail()), SysUser::getEmail, userPageReq.getEmail())
                .like(StringUtils.hasText(userPageReq.getPhone()), SysUser::getPhone, userPageReq.getPhone())
                .eq(null != userPageReq.getStatus(), SysUser::getStatus, userPageReq.getStatus())
                .orderByDesc(SysUser::getUpdateTime)
                .page(userPageReq.page());
        // 查询部门数据
        List<DeptBriefResp> deptOptions = this.deptService.options();
        // 查询角色数据
        List<RoleBriefResp> roleOptions = this.roleService.options();
        // 查询岗位数据
        List<PostBriefResp> postOptions = this.postService.options();
        // 设置部门和橘色数据
        PageResult<UserResp> userRespPageResult = PageResult.of(sysUserIPage, UserResp.class);
        userRespPageResult.getRecords().stream().parallel().forEach(user -> {
            // 设置部门
            if (null != user.getDeptId()) {
                user.setDept(deptOptions.stream().filter(dept -> dept.getId().equals(user.getDeptId())).findFirst().orElse(null));
            }
            // 设置角色
            List<SysRole> roleList = this.roleService.findByUserId(user.getId())
                    .stream()
                    .filter(role -> roleOptions.stream().anyMatch(r -> r.getId().equals(role.getId())))
                    .toList();
            if (!roleList.isEmpty()) {
                user.setRoles(BeanUtils.copyList(roleList, RoleBriefResp.class));
            }
            // 设置岗位
            List<SysPost> postList = this.postService.findByUserId(user.getId())
                    .stream()
                    .filter(post -> postOptions.stream().anyMatch(p -> p.getId().equals(post.getId())))
                    .toList();
            if (!postList.isEmpty()) {
                user.setPosts(BeanUtils.copyList(postList, PostBriefResp.class));
            }
        });
        return userRespPageResult;
    }

    /**
     * 用户详情
     *
     * @param id 用户 ID
     * @return 用户详情
     */
    @Override
    public UserResp detail(Integer id) {
        UserResp userResp = BeanUtils.copy(this.getById(id), UserResp.class);
        if (null != userResp) {
            // 设置角色
            List<SysRole> roleList = this.roleService.findByUserId(userResp.getId());
            if (!roleList.isEmpty()) {
                userResp.setRoles(BeanUtils.copyList(roleList, RoleBriefResp.class));
            }
            // 设置岗位
            List<SysPost> postList = this.postService.findByUserId(userResp.getId());
            if (!postList.isEmpty()) {
                userResp.setPosts(BeanUtils.copyList(postList, PostBriefResp.class));
            }
        }
        return userResp;
    }

    /**
     * 新增用户
     *
     * @param userFormReq 用户新增参数
     * @return 用户新增结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean add(UserFormReq userFormReq) {
        SysUser sysUser = BeanUtils.copy(userFormReq, SysUser.class);
        boolean saved = this.save(sysUser);
        if (saved) {
            userFormReq.setId(sysUser.getId());
            updateUserRationInfo(userFormReq, false);
        }
        return saved;
    }

    private void updateUserRationInfo(UserFormReq userFormReq, boolean isUpdate) {
        // 更新的时候删除之前的对应关系
        if (isUpdate) {
            this.userRoleService.lambdaUpdate()
                    .eq(SysUserRole::getUserId, userFormReq.getId())
                    .remove();
            this.userPostService.lambdaUpdate()
                    .eq(SysUserPost::getUserId, userFormReq.getId())
                    .remove();
        }
        // 插入用户与角色的关系
        if (!ObjectUtils.isEmpty(userFormReq.getRoleIds())) {
            List<SysUserRole> userRoles = userFormReq.getRoleIds().stream().map(roleId -> {
                SysUserRole userRole = new SysUserRole();
                userRole.setRoleId(roleId);
                userRole.setUserId(userFormReq.getId());
                return userRole;
            }).toList();
            this.userRoleService.saveBatch(userRoles);
        }


        // 插入用户与岗位关系
        if (!ObjectUtils.isEmpty(userFormReq.getPostIds())) {
            List<SysUserPost> userPosts = userFormReq.getPostIds().stream().map(postId -> {
                SysUserPost userPost = new SysUserPost();
                userPost.setPostId(postId);
                userPost.setUserId(userFormReq.getId());
                return userPost;
            }).toList();
            if (!userPosts.isEmpty()) {
                this.userPostService.saveBatch(userPosts);
            }
        }

        if (isUpdate) {
            // 通知前端更新菜单和权限
            this.sseService.sendByType(SseMessage.SseMessageTypeEnum.updatePermsAndMenus);
        }
    }


    /**
     * 修改用户
     *
     * @param userFormReq 用户修改参数
     * @return 用户修改结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean update(UserFormReq userFormReq) {
        boolean update = this.updateById(BeanUtils.copy(userFormReq, SysUser.class));
        if (update) {
            updateUserRationInfo(userFormReq, true);
        }
        return update;
    }

    /**
     * 删除用户
     *
     * @param ids 用户 ID 列表
     * @return 用户删除结果
     */
    @Override
    @Transactional
    public Boolean delete(Long[] ids) {
        return this.removeByIds(Arrays.asList(ids));
    }

    /**
     * 更新登录信息
     *
     * @param loginInfo 登录信息
     */
    @Override
    public void updateLoginInfo(LoginUser.LoginInfo loginInfo) {
        this.lambdaUpdate()
                .set(SysUser::getLastLoginTime, loginInfo.getLoginTime())
                .set(SysUser::getDistrict, loginInfo.getLocation())
                .eq(SysUser::getUsername, loginInfo.getUsername())
                .update();
    }

}
