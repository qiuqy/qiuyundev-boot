package com.qiuyundev.boot.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qiuyundev.boot.domain.entity.SysGeneratorConfig;
import com.qiuyundev.boot.mapper.SysGeneratorConfigMapper;
import com.qiuyundev.boot.service.ISysGeneratorConfigService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 代码生成器配置 服务实现类
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-31
 */
@Service
public class SysGeneratorConfigServiceImpl extends ServiceImpl<SysGeneratorConfigMapper, SysGeneratorConfig> implements ISysGeneratorConfigService {
  

}
