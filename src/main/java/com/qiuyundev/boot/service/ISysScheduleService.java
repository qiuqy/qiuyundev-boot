package com.qiuyundev.boot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qiuyundev.boot.base.vo.PageResult;
import com.qiuyundev.boot.domain.entity.SysSchedule;
import com.qiuyundev.boot.domain.vo.schedule.SysScheduleFormReq;
import com.qiuyundev.boot.domain.vo.schedule.SysSchedulePageReq;
import com.qiuyundev.boot.domain.vo.schedule.SysScheduleResp;

/**
 * <p>
 * 任务服务类
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-31
 */
public interface ISysScheduleService extends IService<SysSchedule> {
    /**
     * 分页查询任务表
     *
     * @param pageReq 任务表分页查询参数
     * @return 任务表列表
     */
    PageResult<SysScheduleResp> page(SysSchedulePageReq pageReq);

    /**
     * 任务表详情
     *
     * @param id 任务ID
     * @return 任务表详情
     */
    SysScheduleResp detail(Integer id);

    /**
     * 新增任务表
     *
     * @param formReq 任务表新增参数
     * @return 任务表新增结果
     */
    Boolean add(SysScheduleFormReq formReq);

    /**
     * 修改任务表
     *
     * @param formReq 任务表修改参数
     * @return 任务表修改结果
     */
    Boolean update(SysScheduleFormReq formReq);


    /**
     * 删除任务表
     *
     * @param ids 任务ID 列表
     * @return 任务表删除结果
     */
    Boolean delete(Integer[] ids);

    Boolean once(Integer id);

    Boolean start(Integer id);

    Boolean stop(Integer id);
}
