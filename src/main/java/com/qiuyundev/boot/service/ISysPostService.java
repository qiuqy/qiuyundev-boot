package com.qiuyundev.boot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qiuyundev.boot.base.vo.PageResult;
import com.qiuyundev.boot.domain.entity.SysPost;
import com.qiuyundev.boot.domain.entity.SysRole;
import com.qiuyundev.boot.domain.vo.post.PostBriefResp;
import com.qiuyundev.boot.domain.vo.post.PostFormReq;
import com.qiuyundev.boot.domain.vo.post.PostPageReq;
import com.qiuyundev.boot.domain.vo.post.PostResp;

import java.util.List;

/**
 * <p>
 * 职位 服务类
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-07
 */
public interface ISysPostService extends IService<SysPost> {
    /**
     * 分页查询职位
     *
     * @param pageReq 职位分页查询参数
     * @return 职位列表
     */
    PageResult<PostResp> page(PostPageReq pageReq);

    /**
     * 职位详情
     *
     * @param id 职位 ID
     * @return 职位详情
     */
    PostResp detail(Integer id);

    /**
     * 新增职位
     *
     * @param formReq 职位新增参数
     * @return 职位新增结果
     */
    Boolean add(PostFormReq formReq);

    /**
     * 修改职位
     *
     * @param formReq 职位修改参数
     * @return 职位修改结果
     */
    Boolean update(PostFormReq formReq);


    /**
     * 删除职位
     *
     * @param ids 职位 ID 列表
     * @return 职位删除结果
     */
    Boolean delete(Long[] ids);

    /**
     * 岗位下拉选
     *
     * @return 岗位信息下拉选信息
     */
    List<PostBriefResp> options();

    List<SysPost> findByUserId(Integer userId);
}
