package com.qiuyundev.boot.domain.vo.generator;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TableColumnInfoReq {
    /**
     * 表名称
     */
    private String tableName;

    /**
     * 字段名称
     */
    private String columnName;

    /**
     * 是否表单字段
     */
    private Boolean isFormColumn = false;

    /**
     * 是否响应字段
     */
    private Boolean isRespColumn = false;

    /**
     * 是否查询字段
     */
    private Boolean isQueryColumn = false;
}
