package com.qiuyundev.boot.domain.vo.user;

import com.qiuyundev.boot.base.enums.StatusEnum;
import com.qiuyundev.boot.base.validation.ParamValidationGroup;
import com.qiuyundev.boot.domain.enums.user.GenderEnum;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 用户表单请求参数
 *
 * @author Qiuyun
 * @since 2024/04/22
 */
@Getter
@Setter
public class UserFormReq {
    /**
     * 用户名 ID
     */
    private Integer id;

    /**
     * 部门 ID
     */
    @NotNull(message = "部门ID不能为null")
    private Integer deptId;

    /**
     * 角色 ID 列表
     */
    @NotEmpty(message = "角色ID列表不能为空")
    private List<Integer> roleIds;

    /**
     * 岗位 ID 列表
     */
    @NotEmpty(message = "岗位ID列表不能为空")
    private List<Integer> postIds;

    /**
     * 用户名
     */
    @NotBlank(message = "用户名不能为空")
    private String username;

    /**
     * 密码
     */
    @NotBlank(message = "密码不能为空", groups = {ParamValidationGroup.Add.class})
    private String password;

    /**
     * 昵称
     */
    @NotBlank(message = "昵称不能为空")
    private String nickname;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 状态: DISABLE-锁定, ENABLE-有效
     */
    private StatusEnum status;

    /**
     * 性别: MALE-男,  FEMALE-女,  UNKNOWN-未知
     */
    private GenderEnum gender;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 联系电话
     */
    private String phone;

    /**
     * 描述
     */
    private String description;

    /**
     * 备注
     */
    private String remark;
}
