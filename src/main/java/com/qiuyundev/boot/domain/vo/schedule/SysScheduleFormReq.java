package com.qiuyundev.boot.domain.vo.schedule;

import com.qiuyundev.boot.domain.enums.schedule.ScheduleStatusEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 任务表单
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-31
 */
@Getter
@Setter
@Schema(name = "SysScheduleFormReq", description = "任务表")
public class SysScheduleFormReq {

    @Schema(description = "任务名称")
    private String name;

    @Schema(description = "调用目标")
    private String invokeTarget;

    @Schema(description = "cron表达式")
    private String cronExpression;

    @Schema(description = "状态")
    private ScheduleStatusEnum status;

    @Schema(description = "主键ID")
    private Integer id;


}
