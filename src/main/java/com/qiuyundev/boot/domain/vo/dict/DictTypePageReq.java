package com.qiuyundev.boot.domain.vo.dict;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import com.qiuyundev.boot.domain.entity.SysDictType;
import com.qiuyundev.boot.base.vo.PageParam;

/**
 * <p>
 * 字典类型 分页查询参数
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-13
 */
@Getter
@Setter
@Schema(name = "SysDictTypePageReq", description = "字典类型")
public class DictTypePageReq extends PageParam<SysDictType> {
    @Schema(description = "字典类型名称")
    private String name;

    @Schema(description = "字典类型编号")
    private String code;
}
