package com.qiuyundev.boot.domain.vo.account;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.qiuyundev.boot.domain.enums.menu.ExternalOpenModeEnum;
import com.qiuyundev.boot.domain.enums.menu.MenuTypeEnum;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class AccountMenuResp {

    /**
     * ID
     */
    private Integer id;
    /**
     * 上级ID （顶级 parentId 为 null）
     */
    @JsonIgnore
    private Integer parentId;

    /**
     * 排序
     */
    @JsonIgnore
    private Integer sort;

    /**
     * 子节点
     */
    private List<AccountMenuResp> children;


    /**
     * 名称
     */
    private String name;

    /**
     * 节点路由
     */
    private String path;

    /**
     * 组件路径
     */
    private String component;

    /**
     * 重定向 - 暂时不需要重定向
     */
    private String redirect;


    private MenuMetaResp meta;


    @Getter
    @Setter
    public static class MenuMetaResp {
        private String title;
        /**
         * 图标
         */
        private String icon;
        /**
         * 类型
         */
        private MenuTypeEnum type;
        /**
         * 是否外链
         */
        private Boolean isExternal;

        /**
         * 外部打开方式
         */
        private ExternalOpenModeEnum externalOpenMode;
        /**
         * 是否显示
         */
        private Boolean isShow;

        /**
         * 是否路由缓存
         */
        private Boolean isKeepAlive;

        /**
         * 设置当前路由高亮的菜单项，一般用于详情页
         */
        private String activeMenu;
    }

}
