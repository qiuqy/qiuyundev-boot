package com.qiuyundev.boot.domain.vo.menu;

import com.qiuyundev.boot.base.enums.StatusEnum;
import com.qiuyundev.boot.base.enums.serialize.EnumExtraInfo;
import com.qiuyundev.boot.domain.enums.menu.ExternalOpenModeEnum;
import com.qiuyundev.boot.domain.enums.menu.MenuTypeEnum;
import com.qiuyundev.boot.domain.vo.TreeVO;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class MenuListResp extends TreeVO.TreeResp {

    /**
     * 图标
     */
    private String icon;

    /**
     * 类型
     */
    @EnumExtraInfo
    private MenuTypeEnum type;

    /**
     * 节点路由
     */
    private String path;

    /**
     * 组件路径
     */
    private String component;

    /**
     * 权限标识
     */
    private String permission;

    /**
     * 是否路由缓存
     */
    private Boolean isKeepAlive;

    /**
     * 是否显示
     */
    private Boolean isShow;

    /**
     * 设置当前路由高亮的菜单项，一般用于详情页
     */
    private String activeMenu;

    /**
     * 是否外链
     */
    private Boolean isExternal;

    /**
     * 外部打开方式
     */
    @EnumExtraInfo
    private ExternalOpenModeEnum externalOpenMode;

    /**
     * 状态
     */
    @EnumExtraInfo
    private StatusEnum status;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;
}
