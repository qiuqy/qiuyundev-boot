package com.qiuyundev.boot.domain.vo.post;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import com.qiuyundev.boot.domain.entity.SysPost;
import com.qiuyundev.boot.base.vo.PageParam;

/**
 * <p>
 * 职位 分页查询参数
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-07
 */
@Getter
@Setter
@Schema(name = "PostPageReq", description = "职位")
public class PostPageReq extends PageParam<SysPost> {

    @Schema(description = "职位名称")
    private String name;

}
