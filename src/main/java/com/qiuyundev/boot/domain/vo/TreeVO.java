package com.qiuyundev.boot.domain.vo;

import lombok.Getter;
import lombok.Setter;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class TreeVO {
    /**
     * 树查询参数
     *
     * @author Qiuyun
     * @since 2024/04/18
     */
    @Getter
    @Setter
    public static class TreeReq {
        /**
         * 名称
         */
        private String name;

    }

    @Getter
    @Setter
    public static class TreeResp {
        /**
         * ID
         */
        private Integer id;
        /**
         * 上级ID （顶级 parentId 为 null）
         */
        private Integer parentId;

        /**
         * 名称
         */
        private String name;

        /**
         * 排序
         */
        private Integer sort;

        /**
         * 子节点
         */
        private List<TreeResp> children;

        /**
         * 重写 equals 方法
         * <p>
         * 如果是基于哈希的集合，应重写 hashCode 方法
         *
         * @param obj 对象
         * @return boolean
         */
        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            TreeResp other = (TreeResp) obj;
            // 只比较 ID
            return Objects.equals(id, other.id);
        }
    }

    /**
     * 递归获取父节点
     *
     * @param childTreeResp 子节点
     * @param targetList    目标列表
     * @param all           所有数据
     */
    private static void reversedRecursion(TreeResp childTreeResp, List<TreeResp> targetList, List<TreeResp> all) {
        if (null == childTreeResp.getParentId()) {
            return;
        }
        all.stream()
                .filter(treeResp -> childTreeResp.getParentId().equals(treeResp.getId()))
                .findFirst()
                .ifPresent(treeResp -> {
                    if (!targetList.contains(treeResp)) {
                        targetList.add(treeResp);
                        reversedRecursion(treeResp, targetList, all);
                    }
                });
    }

    /**
     * 搜索树
     *
     * @param treeReq 请求参数
     * @param all     全部数据
     * @return 树状搜索结果
     */
    public static List<TreeVO.TreeResp> searchTree(TreeVO.TreeReq treeReq, List<TreeVO.TreeResp> all) {
        if (!StringUtils.hasText(treeReq.getName())) {
            // 先找出顶级，然后递归获取所有孩子
            return TreeVO.buildTree(all);
        }

        // 传入名称搜索之后,根据名称搜索目标
        List<TreeVO.TreeResp> targetList = new ArrayList<>();
        all.stream()
                .filter(tree -> tree.getName().contains(treeReq.getName()))
                .forEach(tree -> {
                    targetList.add(tree);
                    TreeVO.reversedRecursion(tree, targetList, all);
                });

        // 先找出顶级，然后递归获取所有孩子
        return TreeVO.buildTree(targetList);
    }


    /**
     * 递归获取子节点
     *
     * @param parentTreeResp 父节点
     * @param all            所有数据
     * @return 子节点数据
     */
    private static List<TreeResp> recursion(TreeResp parentTreeResp, List<TreeResp> all) {
        return all.stream()
                .filter(treeResp -> parentTreeResp.getId().equals(treeResp.getParentId()))
                .peek(treeResp -> {
                    List<TreeResp> children = recursion(treeResp, all);
                    if (!children.isEmpty()) {
                        treeResp.setChildren(children);
                    }
                })
                .sorted(Comparator.comparingInt(treeResp -> (treeResp.getSort() == null ? 0 : treeResp.getSort())))
                .collect(Collectors.toList());
    }


    public static List<TreeResp> buildTree(List<TreeResp> treeRespList) {
        return treeRespList.stream()
                .filter(treeResp -> null == treeResp.getParentId())
                .peek(treeResp -> {
                    List<TreeResp> children = recursion(treeResp, treeRespList);
                    if (!children.isEmpty()) {
                        treeResp.setChildren(children);
                    }
                })
                .sorted(Comparator.comparingInt(c -> (c.getSort() == null ? 0 : c.getSort())))
                .toList();
    }
}
