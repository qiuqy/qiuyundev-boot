package com.qiuyundev.boot.domain.vo.post;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PostBriefResp {
    /**
     * 岗位 ID
     */
    private Integer id;
    /**
     * 角色名称
     */
    private String name;
}
