package com.qiuyundev.boot.domain.vo.operation;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import com.qiuyundev.boot.domain.entity.SysOperationLog;
import com.qiuyundev.boot.base.vo.PageParam;

import java.time.LocalDateTime;

/**
 * <p>
 * 操作日志 分页查询参数
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-21
 */
@Getter
@Setter
@Schema(name = "SysOperationLogPageReq", description = "操作日志")
public class OperationLogPageReq extends PageParam<SysOperationLog> {
    @Schema(description = "标签")
    private String tag;

    @Schema(description = "概要")
    private String summary;

    @Schema(description = "请求地址")
    private String requestUrl;

    @Schema(description = "状态")
    private String status;

    @Schema(description = "操作人")
    private String operator;

    @Schema(description = "操作时间范围-开始，格式 yyyy-MM-dd")
    private String startTime;

    @Schema(description = "操作时间范围-结束，格式 yyyy-MM-dd")
    private String endTime;

}
