package com.qiuyundev.boot.domain.vo.dept;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DeptResp {
    /**
     * 部门 ID
     */
    private Integer id;

    /**
     * 上级部门 ID （根部门 parentId 为 null）
     */
    private Integer parentId;

    /**
     * 部门名称
     */
    private String name;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 备注
     */
    private String remark;
}
