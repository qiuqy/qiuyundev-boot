package com.qiuyundev.boot.domain.vo.dict;

import com.qiuyundev.boot.base.enums.StatusEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 字典类型 表单
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-13
 */
@Getter
@Setter
@Schema(name = "SysDictTypeFormReq", description = "字典类型")
public class DictTypeFormReq {
    @Schema(description = "id")
    private Integer id;

    @Schema(description = "字典名称")
    private String name;

    @Schema(description = "字典类型")
    private String code;

    @Schema(description = "字典状态")
    private StatusEnum status;

    @Schema(description = "字典备注")
    private String remark;
}
