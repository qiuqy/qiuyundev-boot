package com.qiuyundev.boot.domain.vo.generator;

import com.qiuyundev.boot.base.vo.PageParam;
import com.qiuyundev.boot.core.db.domain.TableInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CodeGeneratorReq extends PageParam<TableInfo> {
    private String tableName;
}
