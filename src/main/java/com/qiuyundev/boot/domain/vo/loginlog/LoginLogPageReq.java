package com.qiuyundev.boot.domain.vo.loginlog;

import com.qiuyundev.boot.base.vo.PageParam;
import com.qiuyundev.boot.domain.entity.SysLoginLog;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginLogPageReq extends PageParam<SysLoginLog> {
    /**
     * 用户名
     */
    private String username;

    /**
     * 登录地点
     */
    private String location;

    /**
     * 登录 IP
     */
    private String ip;

    /**
     * 登录时间范围-开始，格式 yyyy-MM-dd
     */
    private String startTime;

    /**
     * 登录时间范围-结束，格式 yyyy-MM-dd
     */
    private String endTime;
}
