package com.qiuyundev.boot.domain.vo.dict;

import com.qiuyundev.boot.base.enums.StatusEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 字典项表 表单
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-13
 */
@Getter
@Setter
@Schema(name = "SysDictItemFormReq", description = "字典项表")
public class DictItemFormReq {

    @Schema(description = "id")
    private Integer id;

    @Schema(description = "字典类型")
    private String dictTypeCode;

    @Schema(description = "字典项键名")
    private String label;

    @Schema(description = "字典项值")
    private String value;

    @Schema(description = "排序")
    private Integer sort;

    @Schema(description = "状态")
    private StatusEnum status;

    @Schema(description = "备注")
    private String remark;

}
