package com.qiuyundev.boot.domain.vo.dept;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DeptFormReq {
    /**
     * 部门 ID
     */
    private Integer id;

    /**
     * 上级部门 ID （根部门 parentId 为 null）
     */
    private Integer parentId;

    /**
     * 部门名称
     */
    @NotBlank(message = "部门名称不能为空")
    private String name;

    /**
     * 排序 - 数据库默认0
     */
    private Integer sort;

    /**
     * 备注
     */
    private String remark;
}
