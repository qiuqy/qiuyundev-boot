package com.qiuyundev.boot.domain.vo.menu;

import lombok.Getter;
import lombok.Setter;

/**
 * 菜单列表查询参数
 *
 * @author Qiuyun
 * @since 2024/04/18
 */
@Getter
@Setter
public class MenuListReq {
    /**
     * 菜单名称
     */
    private String name;

    /**
     * 节点路由
     */
    private String path;

    /**
     * 组件路径
     */
    private String component;
}
