package com.qiuyundev.boot.domain.vo.schedule;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import com.qiuyundev.boot.domain.entity.SysSchedule;
import com.qiuyundev.boot.base.vo.PageParam;

/**
 * <p>
 * 任务分页查询参数
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-31
 */
@Getter
@Setter
@Schema(name = "SysSchedulePageReq", description = "任务表")
public class SysSchedulePageReq extends PageParam<SysSchedule> {

    @Schema(description = "任务名称")
    private String name;


}
