package com.qiuyundev.boot.domain.vo.dict;

import com.qiuyundev.boot.base.enums.StatusEnum;
import com.qiuyundev.boot.base.enums.serialize.EnumExtraInfo;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * <p>
 * 字典类型 响应结果
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-13
 */
@Getter
@Setter
@Schema(name = "SysDictTypeResp", description = "字典类型")
public class DictTypeResp {
    @Schema(description = "字典类型名称")
    private String name;

    @Schema(description = "字典类型编号")
    private String code;

    @EnumExtraInfo
    @Schema(description = "字典状态")
    private StatusEnum status;

    @Schema(description = "字典备注")
    private String remark;

    @Schema(description = "字典备注")
    private Integer id;

    @Schema(description = "创建时间")
    private LocalDateTime createTime;

    @Schema(description = "更新时间")
    private LocalDateTime updateTime;
}
