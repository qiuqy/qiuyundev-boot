package com.qiuyundev.boot.domain.vo.file;

import com.qiuyundev.boot.base.enums.serialize.EnumExtraInfo;
import com.qiuyundev.boot.domain.enums.file.FileCategoryEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import jdk.jfr.Category;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * <p>
 * 文件 响应结果
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-20
 */
@Getter
@Setter
@Schema(name = "SysFileResp", description = "文件")
public class FileResp {
    @Schema(description = "id")
    private Integer id;

    @Schema(description = "文件名")
    private String name;

    @Schema(description = "文件后缀")
    private String extension;

    @EnumExtraInfo
    @Schema(description = "类别")
    private FileCategoryEnum category;

    @Schema(description = "内容类型")
    private String contentType;

    @Schema(description = "大小")
    private Long size;

    @Schema(description = "路径")
    private String path;

    @Schema(description = "创建时间")
    private LocalDateTime createTime;

    @Schema(description = "上传用户名")
    private String uploadUsername;

}
