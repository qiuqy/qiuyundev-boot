package com.qiuyundev.boot.domain.vo.serve;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ServerStatInfo {
    private Runtime runtime;
    private Memory memory;
    private Cpu cpu;
    private List<FileStore> fileStores;
}
