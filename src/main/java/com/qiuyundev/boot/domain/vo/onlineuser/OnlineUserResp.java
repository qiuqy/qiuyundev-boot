package com.qiuyundev.boot.domain.vo.onlineuser;


import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class OnlineUserResp {
    /**
     * 是否时当前登录用户
     */
    private Boolean isCurrent;

    /**
     * 不允许踢当前用户或超级管理员下线
     */
    private Boolean disable;

    /**
     * 用户缓存标识
     */
    private String userKey;

    /**
     * 用户名
     */
    private String username;
    /**
     * 部门 id
     */
    private Integer deptId;
    /**
     * 部门
     */
    private String deptName;
    /**
     * 登录 IP
     */
    private String ip;
    /**
     * 登录地点
     */
    private String location;
    /**
     * 浏览器
     */
    private String browser;

    /**
     * 引擎
     */
    private String engine;

    /**
     * 平台
     */
    private String platform;
    /**
     * 操作系统
     */
    private String os;
    /**
     * 登录时间
     */
    private LocalDateTime loginTime;
}
