package com.qiuyundev.boot.domain.vo.operation;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;


/**
 * <p>
 * 操作日志 表单
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-21
 */
@Getter
@Setter
@Schema(name = "SysOperationLogFormReq", description = "操作日志")
public class OperationLogFormReq {

    @Schema(description = "id")
    private Integer id;
    @Schema(description = "模块")
    private String module;
    @Schema(description = "标签")
    private String tag;
    @Schema(description = "概要")
    private String summary;
    @Schema(description = "请求类")
    private String clazz;
    @Schema(description = "请求方法")
    private String method;
    @Schema(description = "请求方式")
    private String requestMethod;
    @Schema(description = "操作类型")
    private String type;
    @Schema(description = "请求地址")
    private String requestUrl;
    @Schema(description = "请求IP")
    private String requestIp;
    @Schema(description = "请求地点")
    private String requestLocation;
    @Schema(description = "请求参数")
    private String requestParam;
    @Schema(description = "返回结果")
    private String responseJson;
    @Schema(description = "状态")
    private String status;
    @Schema(description = "异常信息")
    private String exceptionInfo;
    @Schema(description = "操作耗时(毫秒)")
    private Long costTime;
    @Schema(description = "操作人")
    private String operator;
    @Schema(description = "操作时间")
    private LocalDateTime operationTime;

}
