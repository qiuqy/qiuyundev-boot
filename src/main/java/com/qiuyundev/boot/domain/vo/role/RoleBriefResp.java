package com.qiuyundev.boot.domain.vo.role;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoleBriefResp {
    /**
     * 角色 ID
     */
    private Integer id;
    /**
     * 角色名称
     */
    private String name;
}
