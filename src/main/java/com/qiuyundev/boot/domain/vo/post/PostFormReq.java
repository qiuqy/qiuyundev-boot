package com.qiuyundev.boot.domain.vo.post;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 职位 表单
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-07
 */
@Getter
@Setter
@Schema(name = "PostFormReq", description = "职位")
public class PostFormReq {
    @Schema(description = "id")
    private Integer id;

    @Schema(description = "职位名称")
    private String name;

}
