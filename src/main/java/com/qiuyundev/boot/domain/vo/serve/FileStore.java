package com.qiuyundev.boot.domain.vo.serve;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FileStore {
    /**
     * 名称
     */
    private String name;
    /**
     * 标签
     */
    private String label;
    /**
     * 挂载点
     */
    private String mount;
    /**
     * 描述
     */
    private String description;
    /**
     * 类型
     */
    private String fsType;
    /**
     * 可用容量
     */
    private double freeSpace;
    /**
     * 总容量
     */
    private double totalSpace;
}
