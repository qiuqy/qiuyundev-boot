package com.qiuyundev.boot.domain.vo.serve;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Runtime {
    /**
     * 服务器架构
     */
    private String arch;
    /**
     * 系统
     */
    private String os;
    private String javaVendor;
    private String javaVersion;
}
