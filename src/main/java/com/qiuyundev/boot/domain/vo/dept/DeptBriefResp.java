package com.qiuyundev.boot.domain.vo.dept;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DeptBriefResp {
    /**
     * 部门 ID
     */
    private Integer id;

    /**
     * 部门名称
     */
    private String name;
}
