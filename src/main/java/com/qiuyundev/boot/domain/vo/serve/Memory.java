package com.qiuyundev.boot.domain.vo.serve;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Memory {
    private long total;
    private long available;
}
