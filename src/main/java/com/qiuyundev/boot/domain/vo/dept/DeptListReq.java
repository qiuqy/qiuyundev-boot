package com.qiuyundev.boot.domain.vo.dept;

import lombok.Getter;
import lombok.Setter;

/**
 * 部门列表查询参数
 *
 * @author Qiuyun
 * @since 2024/04/18
 */
@Getter
@Setter
public class DeptListReq {
    /**
     * 部门名称
     */
    private String name;

}
