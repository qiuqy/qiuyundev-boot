package com.qiuyundev.boot.domain.vo.config;

import com.qiuyundev.boot.base.vo.PageParam;
import com.qiuyundev.boot.domain.entity.SysConfig;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 参数配置分页查询参数
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-06
 */
@Getter
@Setter
@Schema(name = "ConfigPageReq", description = "参数配置分页查询参数")
public class ConfigPageReq extends PageParam<SysConfig> {

    @Schema(description = "参数名称")
    private String name;

}
