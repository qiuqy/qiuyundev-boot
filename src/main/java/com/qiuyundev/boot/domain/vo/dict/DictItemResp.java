package com.qiuyundev.boot.domain.vo.dict;

import com.qiuyundev.boot.base.enums.StatusEnum;
import com.qiuyundev.boot.base.enums.serialize.EnumExtraInfo;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * <p>
 * 字典项表 响应结果
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-13
 */
@Getter
@Setter
@Schema(name = "SysDictItemResp", description = "字典项表")
public class DictItemResp {
    @Schema(description = "字典类型")
    private String dictTypeCode;

    @Schema(description = "字典项键名")
    private String label;

    @Schema(description = "字典项值")
    private String value;

    @Schema(description = "排序")
    private Integer sort;

    @EnumExtraInfo
    @Schema(description = "状态")
    private StatusEnum status;

    @Schema(description = "备注")
    private String remark;

    @Schema(description = "字典备注")
    private Integer id;

    @Schema(description = "创建时间")
    private LocalDateTime createTime;

    @Schema(description = "更新时间")
    private LocalDateTime updateTime;

}
