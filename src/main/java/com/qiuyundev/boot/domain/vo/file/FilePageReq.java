package com.qiuyundev.boot.domain.vo.file;

import com.qiuyundev.boot.base.vo.PageParam;
import com.qiuyundev.boot.domain.entity.SysFile;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 文件 分页查询参数
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-20
 */
@Getter
@Setter
@Schema(name = "SysFilePageReq", description = "文件")
public class FilePageReq extends PageParam<SysFile> {

    @Schema(description = "文件名")
    private String name;
}
