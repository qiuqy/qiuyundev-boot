package com.qiuyundev.boot.domain.vo.role;


import com.qiuyundev.boot.base.enums.StatusEnum;
import com.qiuyundev.boot.base.enums.serialize.EnumExtraInfo;
import com.qiuyundev.boot.core.datascope.DataScopeEnum;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 角色响应结果
 *
 * @author Qiuyun
 * @since 2024/04/18
 */
@Getter
@Setter
public class RoleResp {
    /**
     * 角色 ID
     */
    private Integer id;
    /**
     * 角色编号
     */
    private String code;

    /**
     * 角色名称
     */
    private String name;

    /**
     * 角色状态
     */
    @EnumExtraInfo
    private StatusEnum status;

    /**
     * 数据权限编号
     */
    @EnumExtraInfo
    private DataScopeEnum dataScope;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 菜单 ID 列表
     */
    private List<Integer> menuIds;


    /**
     * 自定义部门权限的部门 ID 列表
     */
    private List<Integer> deptIds;
}
