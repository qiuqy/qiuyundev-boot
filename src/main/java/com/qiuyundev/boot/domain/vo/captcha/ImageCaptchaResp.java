package com.qiuyundev.boot.domain.vo.captcha;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ImageCaptchaResp {
    /**
     * 验证码标识 - 缓存用的 key
     */
    private String captchaKey;

    /**
     * 验证码图片Base64字符串
     */
    private String captchaBase64;
}
