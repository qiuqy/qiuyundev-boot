package com.qiuyundev.boot.domain.vo.generator;


import cn.hutool.core.lang.Pair;
import cn.hutool.core.util.ReflectUtil;
import lombok.Getter;
import lombok.Setter;

import java.lang.reflect.Field;
import java.util.Map;

@Getter
@Setter
public class PreviewResp {
    private Pair<String, String> controller;
    private Pair<String, String> service;
    private Pair<String, String> serviceImpl;
    private Pair<String, String> mapper;
    private Pair<String, String> mapperXml;
    private Pair<String, String> entity;
    private Pair<String, String> formReq;
    private Pair<String, String> pageReq;
    private Pair<String, String> resp;

    public static PreviewResp of(Map<String, String> codeMap) {
        PreviewResp previewResp = new PreviewResp();
        Field[] fields = ReflectUtil.getFields(PreviewResp.class);
        codeMap.forEach((k, v) -> {
            k = k.replace("/templates/ftl/", "")
                    .replace("vo/", "")
                    .replace(".ftl", "");
            for (Field field : fields) {
                if (k.contains("mapper")) {
                    if (k.contains("xml")) {
                        ReflectUtil.setFieldValue(previewResp, "mapperXml", Pair.of(k, v));
                    } else {
                        ReflectUtil.setFieldValue(previewResp, "mapper", Pair.of(k, v));
                    }
                    break;
                }  else if (k.contains(field.getName() + ".")) {
                    ReflectUtil.setFieldValue(previewResp, field, Pair.of(k, v));
                    break;
                }
            }
        });

        return previewResp;
    }
}
