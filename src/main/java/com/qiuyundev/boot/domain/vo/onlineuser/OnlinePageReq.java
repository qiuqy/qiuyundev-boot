package com.qiuyundev.boot.domain.vo.onlineuser;

import com.qiuyundev.boot.base.vo.PageParam;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OnlinePageReq extends PageParam<OnlineUserResp> {
    /**
     * 用户名
     */
    private String username;
    /**
     * 登录 IP
     */
    private String ip;
    /**
     * 登录地点
     */
    private String location;
}
