package com.qiuyundev.boot.domain.vo.onlineuser;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OnlineKickReq {
    String userKey;
}
