package com.qiuyundev.boot.domain.vo.schedule;

import com.qiuyundev.boot.base.enums.serialize.EnumExtraInfo;
import com.qiuyundev.boot.domain.enums.schedule.ScheduleStatusEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * <p>
 * 任务响应结果
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-31
 */
@Getter
@Setter
@Schema(name = "SysScheduleResp", description = "任务表")
public class SysScheduleResp {

    @Schema(description = "任务名称")
    private String name;

    @Schema(description = "调用目标")
    private String invokeTarget;

    @Schema(description = "cron表达式")
    private String cronExpression;

    @EnumExtraInfo
    @Schema(description = "状态")
    private ScheduleStatusEnum status;

    @Schema(description = "主键ID")
    private Integer id;

    @Schema(description = "创建时间")
    private LocalDateTime createTime;

    @Schema(description = "修改时间")
    private LocalDateTime updateTime;


}
