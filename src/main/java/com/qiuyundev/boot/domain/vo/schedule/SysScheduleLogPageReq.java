package com.qiuyundev.boot.domain.vo.schedule;

import com.qiuyundev.boot.base.vo.PageParam;
import com.qiuyundev.boot.domain.entity.SysScheduleLog;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * <p>
 * 任务日志表 分页查询参数
 * </p>
 *
 * @author Qiuyun
 * @since 2024-06-03
 */
@Getter
@Setter
@Schema(name = "SysScheduleLogPageReq", description = "任务日志表")
public class SysScheduleLogPageReq extends PageParam<SysScheduleLog> {

    @Schema(description = "任务名称")
    private String scheduleName;

    @Schema(description = "状态")
    private String status;

    @Schema(description = "执行时间开始 - yyyy-MM-dd")
    private String startTime;

    @Schema(description = "执行时间结束 - yyyy-MM-dd")
    private String endTime;

}
