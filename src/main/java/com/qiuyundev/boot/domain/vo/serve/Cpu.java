package com.qiuyundev.boot.domain.vo.serve;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Cpu {
    /**
     * CPU核心数
     */
    private Integer cpuNum;

    /**
     * CPU总的使用率
     */
    private double toTal;

    /**
     * CPU系统使用率
     */
    private double sys;

    /**
     * CPU用户使用率
     */
    private double user;

    /**
     * CPU当前等待率
     */
    private double wait;

    /**
     * CPU当前空闲率
     */
    private double free;

    /**
     * 获取用户+系统的总的CPU使用率
     * Returns:
     * 总CPU使用率
     */
    private double used;

    /**
     * CPU型号信息
     */
    private String cpuModel;
}
