package com.qiuyundev.boot.domain.vo.post;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * <p>
 * 职位 响应结果
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-07
 */
@Getter
@Setter
@Schema(name = "PostResp", description = "职位")
public class PostResp {

    @Schema(description = "职位名称")
    private String name;

    @Schema(description = "id")
    private Integer id;

    @Schema(description = "创建时间")
    private LocalDateTime createTime;

    @Schema(description = "更新时间")
    private LocalDateTime updateTime;

}
