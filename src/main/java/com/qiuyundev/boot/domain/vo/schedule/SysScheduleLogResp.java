package com.qiuyundev.boot.domain.vo.schedule;

import com.qiuyundev.boot.base.enums.ApiStatusEnum;
import com.qiuyundev.boot.base.enums.serialize.EnumExtraInfo;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * <p>
 * 任务日志表 响应结果
 * </p>
 *
 * @author Qiuyun
 * @since 2024-06-03
 */
@Getter
@Setter
@Schema(name = "SysScheduleLogResp", description = "任务日志表")
public class SysScheduleLogResp {

    @Schema(description = "任务ID")
    private Integer scheduleId;

    @Schema(description = "任务名称")
    private String scheduleName;

    @Schema(description = "异常信息")
    private String exceptionInfo;

    @Schema(description = "耗时")
    private Long costTime;

    @EnumExtraInfo
    @Schema(description = "状态")
    private ApiStatusEnum status;

    @Schema(description = "执行时间")
    private LocalDateTime executeTime;

    @Schema(description = "主键ID")
    private Integer id;

}
