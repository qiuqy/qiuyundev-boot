package com.qiuyundev.boot.domain.vo.role;

import com.qiuyundev.boot.base.enums.StatusEnum;
import com.qiuyundev.boot.core.datascope.DataScopeEnum;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RoleFormReq {
    /**
     * 角色 ID
     */
    private Integer id;
    /**
     * 角色编号
     */
    private String code;

    /**
     * 角色名称
     */
    private String name;

    /**
     * 角色状态
     */
    private StatusEnum status;

    /**
     * 数据权限编号
     */
    private DataScopeEnum dataScope;

    /**
     * 备注
     */
    private String remark;

    /**
     * 菜单 ID 列表
     */
    private List<Integer> menuIds;

    /**
     * 自定义权限 ID 列表
     */
    private List<Integer> deptIds;
}
