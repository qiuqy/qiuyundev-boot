package com.qiuyundev.boot.domain.vo.role;

import com.qiuyundev.boot.base.enums.StatusEnum;
import com.qiuyundev.boot.base.vo.PageParam;
import com.qiuyundev.boot.domain.entity.SysRole;
import lombok.Getter;
import lombok.Setter;

/**
 * 角色分页查询参数
 *
 * @author Qiuyun
 * @since 2024/04/18
 */
@Getter
@Setter
public class RolePageReq extends PageParam<SysRole> {
    /**
     * 角色编号
     */
    private String code;

    /**
     * 角色名称
     */
    private String name;

    /**
     * 角色状态
     */
    private StatusEnum status;

    /**
     * 备注
     */
    private String remark;
}
