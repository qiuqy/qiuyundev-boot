package com.qiuyundev.boot.domain.vo.account;

import cn.hutool.system.SystemUtil;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * 用户信息响应
 *
 * @author Qiuyun
 * @since 2024/04/18
 */
@Getter
@Setter
@Builder
public class AccountInfoResp {
    /**
     * 用户id
     */
    private Integer id;

    /**
     * 用户名
     */
    private String username;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 手机号
     */
    private String phone;
    /**
     * ip地址
     */
    private String ip;
}
