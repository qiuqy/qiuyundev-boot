package com.qiuyundev.boot.domain.vo.auth;


import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * 登录响应
 *
 * @author Qiuyun
 * @since 2024/04/17
 */
@Getter
@Setter
@Builder
public class LoginResp {
    /**
     * token
     */
    private String token;
}
