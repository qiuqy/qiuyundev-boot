package com.qiuyundev.boot.domain.vo.dict;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import com.qiuyundev.boot.domain.entity.SysDictItem;
import com.qiuyundev.boot.base.vo.PageParam;

/**
 * <p>
 * 字典项表 分页查询参数
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-13
 */
@Getter
@Setter
@Schema(name = "SysDictItemPageReq", description = "字典项表")
public class DictItemPageReq extends PageParam<SysDictItem> {
    @Schema(description = "字典类型")
    private String dictTypeCode;

    @Schema(description = "字典项键名")
    private String label;

    @Schema(description = "字典项值")
    private String value;

    @Schema(description = "排序")
    private Integer sort;

    @Schema(description = "状态")
    private String status;

    @Schema(description = "备注")
    private String remark;

}
