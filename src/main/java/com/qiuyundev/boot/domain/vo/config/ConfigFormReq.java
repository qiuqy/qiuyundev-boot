package com.qiuyundev.boot.domain.vo.config;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 参数配置 表单
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-06
 */
@Getter
@Setter
@Schema(name = "ConfigFormReq", description = "参数配置单")
public class ConfigFormReq {
    @Schema(description = "id")
    private Integer id;

    @Schema(description = "参数名称")
    private String name;

    @Schema(description = "参数键名")
    private String key;

    @Schema(description = "参数值")
    private String value;

    @Schema(description = "备注")
    private String remark;

}
