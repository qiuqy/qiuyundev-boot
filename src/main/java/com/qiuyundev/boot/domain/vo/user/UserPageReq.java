package com.qiuyundev.boot.domain.vo.user;

import com.qiuyundev.boot.base.enums.StatusEnum;
import com.qiuyundev.boot.base.vo.PageParam;
import com.qiuyundev.boot.domain.entity.SysUser;
import com.qiuyundev.boot.domain.enums.user.GenderEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * 用户查询参数
 *
 * @author Qiuyun
 * @since 2024/04/18
 */
@Getter
@Setter
public class UserPageReq extends PageParam<SysUser> {
    /**
     * 部门 ID
     */
    private Integer deptId;
    /**
     * 用户名
     */
    private String username;

    /**
     * 昵称
     */
    private String nickname;
    /**
     * 邮箱
     */
    private String email;

    /**
     * 联系电话
     */
    private String phone;

    /**
     * 性别: MALE-男,  FEMALE-女,  UNKNOWN-未知
     */
    private GenderEnum gender;

    /**
     * 状态: DISABLE-锁定, ENABLE-有效
     */
    private StatusEnum status;
}
