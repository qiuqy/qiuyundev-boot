package com.qiuyundev.boot.domain.vo.user;


import com.qiuyundev.boot.base.enums.StatusEnum;
import com.qiuyundev.boot.base.enums.serialize.EnumExtraInfo;
import com.qiuyundev.boot.domain.enums.user.GenderEnum;
import com.qiuyundev.boot.domain.vo.dept.DeptBriefResp;
import com.qiuyundev.boot.domain.vo.post.PostBriefResp;
import com.qiuyundev.boot.domain.vo.role.RoleBriefResp;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 用户响应结果
 *
 * @author Qiuyun
 * @since 2024/04/18
 */
@Getter
@Setter
public class UserResp {
    /**
     * 用户名 ID
     */
    private Integer id;

    /**
     * 部门 ID
     */
    private Integer deptId;
    /**
     * 用户名
     */
    private String username;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 状态: DISABLE-锁定, ENABLE-有效
     */
    @EnumExtraInfo
    private StatusEnum status;

    /**
     * 性别: MALE-男,  FEMALE-女,  UNKNOWN-未知
     */
    @EnumExtraInfo
    private GenderEnum gender;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 联系电话
     */
    private String phone;

    /**
     * 最近登录时间
     */
    private LocalDateTime lastLoginTime;

    /**
     * 地区
     */
    private String district;

    /**
     * 描述
     */
    private String description;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    /**
     * 部门简洁对象
     */
    private DeptBriefResp dept;

    /**
     * 角色简洁列表
     */
    private List<RoleBriefResp> roles;

    /**
     * 岗位简洁列表
     */
    private List<PostBriefResp> posts;
}
