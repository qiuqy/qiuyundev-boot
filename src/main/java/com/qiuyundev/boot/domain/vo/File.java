package com.qiuyundev.boot.domain.vo;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class File {
    /**
     * 文件地址 - 完整地址
     */
    private String path;
}
