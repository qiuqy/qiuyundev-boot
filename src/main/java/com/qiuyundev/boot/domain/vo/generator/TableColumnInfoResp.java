package com.qiuyundev.boot.domain.vo.generator;

import com.qiuyundev.boot.core.db.domain.TableColumnInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TableColumnInfoResp extends TableColumnInfo {
    /**
     * 是否表单字段
     */
    private Boolean isFormColumn = false;

    /**
     * 是否响应字段
     */
    private Boolean isRespColumn = false;

    /**
     * 是否查询字段
     */
    private Boolean isQueryColumn = false;
}
