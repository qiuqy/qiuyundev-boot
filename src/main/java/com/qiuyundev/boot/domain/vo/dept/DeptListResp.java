package com.qiuyundev.boot.domain.vo.dept;

import com.qiuyundev.boot.domain.vo.TreeVO;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class DeptListResp extends TreeVO.TreeResp {
    /**
     * 备注
     */
    private String remark;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;
}
