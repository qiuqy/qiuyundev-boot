package com.qiuyundev.boot.domain.vo.file;

import com.qiuyundev.boot.domain.enums.file.FileCategoryEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;


/**
 * <p>
 * 文件 表单
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-20
 */
@Getter
@Setter
@Schema(name = "SysFileFormReq", description = "文件")
public class FileFormReq {

    @Schema(description = "id")
    private Integer id;

    @Schema(description = "文件名")
    private String name;

    @Schema(description = "文件后缀")
    private String extension;

    @Schema(description = "类别")
    private FileCategoryEnum category;

    @Schema(description = "内容类型")
    private String contentType;

    @Schema(description = "大小")
    private Long size;

    @Schema(description = "路径")
    private String path;

    @Schema(description = "上传用户名")
    private String uploadUsername;
}
