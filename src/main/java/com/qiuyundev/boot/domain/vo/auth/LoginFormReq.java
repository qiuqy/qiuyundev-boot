package com.qiuyundev.boot.domain.vo.auth;


import lombok.Getter;
import lombok.Setter;

/**
 * 登录请求表单
 */
@Getter
@Setter
public class LoginFormReq {

    /**
     * 手机号/邮箱
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 验证码标识
     */
    private String captchaKey;
    /**
     * 用户输入的验证码
     */
    private String captchaCode;
}
