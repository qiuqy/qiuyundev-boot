package com.qiuyundev.boot.domain.vo.config;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * <p>
 * 参数配置 响应结果
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-06
 */
@Getter
@Setter
@Schema(name = "ConfigResp", description = "参数配置查询结果")
public class ConfigResp {

    @Schema(description = "参数名称")
    private String name;

    @Schema(description = "参数键名")
    private String key;

    @Schema(description = "参数值")
    private String value;

    @Schema(description = "备注")
    private String remark;

    @Schema(description = "id")
    private Integer id;

    @Schema(description = "创建时间")
    private LocalDateTime createTime;

    @Schema(description = "更新时间")
    private LocalDateTime updateTime;
}
