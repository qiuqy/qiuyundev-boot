package com.qiuyundev.boot.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.qiuyundev.boot.core.mybatisplus.BaseBriefEntity;
import com.qiuyundev.boot.domain.enums.schedule.ScheduleStatusEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 任务表
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-31
 */
@Getter
@Setter
@TableName("sys_schedule")
public class SysSchedule extends BaseBriefEntity {

    /**
     * 任务名称
     */
    private String name;

    /**
     * 调用目标
     */
    private String invokeTarget;

    /**
     * cron表达式
     */
    private String cronExpression;

    /**
     * 状态
     */
    private ScheduleStatusEnum status;
}
