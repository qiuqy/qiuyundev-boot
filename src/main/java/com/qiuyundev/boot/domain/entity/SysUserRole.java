package com.qiuyundev.boot.domain.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * <p>
 * 用户角色
 * </p>
 *
 * @author Qiuyun
 * @since 2024/04/11
 */
@Getter
@Setter
public class SysUserRole implements Serializable {

    /**
     * 用户 ID
     */
    private Integer userId;

    /**
     * 角色 ID
     */
    private Integer roleId;
}
