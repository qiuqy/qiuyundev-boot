package com.qiuyundev.boot.domain.entity;

import com.qiuyundev.boot.base.enums.StatusEnum;
import com.qiuyundev.boot.core.datascope.DataScopeEnum;
import com.qiuyundev.boot.core.mybatisplus.BaseBriefEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 角色
 * </p>
 *
 * @author Qiuyun
 * @since 2024/04/11
 */
@Getter
@Setter
public class SysRole extends BaseBriefEntity {

    /**
     * 角色编号
     */
    private String code;

    /**
     * 角色名称
     */
    private String name;

    /**
     * 角色状态
     */
    private StatusEnum status;

    /**
     * 数据权限
     */
    private DataScopeEnum dataScope;

    /**
     * 备注
     */
    private String remark;
}
