package com.qiuyundev.boot.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.qiuyundev.boot.base.enums.StatusEnum;
import com.qiuyundev.boot.core.mybatisplus.BaseBriefEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 字典项表
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-13
 */
@Getter
@Setter
@TableName("sys_dict_item")
@Schema(name = "SysDictItem", description = "字典项表")
public class SysDictItem extends BaseBriefEntity {

    /**
     * 字典类型编号
     */
    private String dictTypeCode;

    /**
     * 字典项键名
     */
    private String label;

    /**
     * 字典项值
     */
    @TableField(value = "`value`")
    private String value;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 状态
     */
    private StatusEnum status;

    /**
     * 备注
     */
    private String remark;
}
