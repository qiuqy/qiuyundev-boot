package com.qiuyundev.boot.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.qiuyundev.boot.base.enums.StatusEnum;
import com.qiuyundev.boot.core.mybatisplus.BaseBriefEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 字典类型
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-13
 */
@Getter
@Setter
@TableName("sys_dict_type")
@Schema(name = "SysDictType", description = "字典类型")
public class SysDictType extends BaseBriefEntity {

    /**
     * 字典名称
     */
    private String name;

    /**
     * 字典类型
     */
    private String code;

    /**
     * 字典状态
     */
    private StatusEnum status;

    /**
     * 字典备注
     */
    private String remark;
}
