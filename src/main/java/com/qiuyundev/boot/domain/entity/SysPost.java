package com.qiuyundev.boot.domain.entity;

import com.qiuyundev.boot.core.mybatisplus.BaseBriefEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 职位
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-07
 */
@Getter
@Setter
public class SysPost extends BaseBriefEntity {


    /**
     * 职位名称
     */
    private String name;
}
