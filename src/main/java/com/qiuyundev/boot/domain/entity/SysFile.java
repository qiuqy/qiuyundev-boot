package com.qiuyundev.boot.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.qiuyundev.boot.core.mybatisplus.BaseBriefEntity;
import com.qiuyundev.boot.domain.enums.file.FileCategoryEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 文件
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-20
 */
@Getter
@Setter
@TableName("sys_file")
public class SysFile extends BaseBriefEntity {

    /**
     * 文件名
     */
    private String name;

    /**
     * 文件后缀
     */
    private String extension;

    /**
     * 类别
     */
    private FileCategoryEnum category;

    /**
     * 大小 - 单位：字节
     */
    private Long size;

    /**
     * 路径
     */
    private String path;

    /**
     * 内容类型
     */
    private String contentType;

    /**
     * 上传用户名
     */
    private String uploadUsername;
}
