package com.qiuyundev.boot.domain.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serial;
import java.io.Serializable;

/**
 * <p>
 * 角色部门
 * </p>
 *
 * @author Qiuyun
 * @since 2024/04/11
 */
@Getter
@Setter
@Accessors(chain = true)
public class SysRoleDept implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 角色 ID
     */
    private Integer roleId;

    /**
     * 部门 ID
     */
    private Integer deptId;
}
