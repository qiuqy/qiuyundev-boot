package com.qiuyundev.boot.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.qiuyundev.boot.base.enums.ApiStatusEnum;
import com.qiuyundev.boot.core.mybatisplus.BaseBriefEntity;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 任务日志表
 * </p>
 *
 * @author Qiuyun
 * @since 2024-06-03
 */
@Getter
@Setter
@TableName("sys_schedule_log")
public class SysScheduleLog {
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 任务ID
     */
    private Integer scheduleId;

    /**
     * 任务名称
     */
    private String scheduleName;

    /**
     * 异常信息
     */
    private String exceptionInfo;

    /**
     * 耗时
     */
    private Long costTime;

    /**
     * 状态
     */
    private ApiStatusEnum status;

    /**
     * 执行时间
     */
    private LocalDateTime executeTime;
}
