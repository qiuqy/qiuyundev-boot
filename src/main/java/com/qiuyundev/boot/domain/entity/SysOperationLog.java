package com.qiuyundev.boot.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.qiuyundev.boot.base.enums.ApiStatusEnum;
import com.qiuyundev.boot.core.log.ModuleEnum;
import com.qiuyundev.boot.core.log.OperationTypeEnum;

import java.time.LocalDateTime;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 操作日志
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-21
 */
@Getter
@Setter
@TableName("sys_operation_log")
@Schema(name = "SysOperationLog", description = "操作日志")
public class SysOperationLog {

    @Schema(description = "id")
    @TableId(type = IdType.AUTO)
    private Integer id;

    @Schema(description = "模块")
    private ModuleEnum module;

    @Schema(description = "标签")
    private String tag;

    @Schema(description = "接口概要")
    private String summary;

    @Schema(description = "请求类")
    private String clazz;

    @Schema(description = "请求方法")
    private String method;

    @Schema(description = "请求方式")
    private String requestMethod;

    @Schema(description = "操作类型")
    private OperationTypeEnum type;

    @Schema(description = "请求地址")
    private String requestUrl;

    @Schema(description = "请求IP")
    private String requestIp;

    @Schema(description = "请求地点")
    private String requestLocation;

    @Schema(description = "请求参数")
    private String requestParam;

    @Schema(description = "返回结果")
    private String responseJson;

    @Schema(description = "状态")
    private ApiStatusEnum status;

    @Schema(description = "异常信息")
    private String exceptionInfo;

    @Schema(description = "操作耗时(毫秒)")
    private Long costTime;

    @Schema(description = "操作人")
    private String operator;

    @Schema(description = "操作时间")
    private LocalDateTime operationTime;
}
