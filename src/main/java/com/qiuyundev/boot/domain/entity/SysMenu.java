package com.qiuyundev.boot.domain.entity;


import com.qiuyundev.boot.base.enums.StatusEnum;
import com.qiuyundev.boot.core.mybatisplus.BaseBriefEntity;
import com.qiuyundev.boot.domain.enums.menu.ExternalOpenModeEnum;
import com.qiuyundev.boot.domain.enums.menu.MenuTypeEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 菜单
 * </p>
 *
 * @author Qiuyun
 * @since 2024/04/11
 */
@Getter
@Setter
public class SysMenu extends BaseBriefEntity {

    /**
     * 上级菜单 ID
     */
    private Integer parentId;

    /**
     * 名称
     */
    private String name;

    /**
     * 图标
     */
    private String icon;

    /**
     * 类型
     */
    private MenuTypeEnum type;

    /**
     * 节点路由
     */
    private String path;

    /**
     * 组件路径
     */
    private String component;

    /**
     * 权限标识
     */
    private String permission;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 是否路由缓存
     */
    private Boolean isKeepAlive;

    /**
     * 是否显示
     */
    private Boolean isShow;


    /**
     * 设置当前路由高亮的菜单项，一般用于详情页
     */
    private String activeMenu;

    /**
     * 是否外链
     */
    private Boolean isExternal;

    /**
     * 外部打开方式
     */
    private ExternalOpenModeEnum externalOpenMode;

    /**
     * 状态
     */
    private StatusEnum status;
}
