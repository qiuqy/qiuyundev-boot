package com.qiuyundev.boot.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * 登录日志
 *
 * @author Qiuyun
 * @since 2024/04/26
 */
@Getter
@Setter
public class SysLoginLog {

    /**
     * 主键 ID
     */
    @TableId(type = IdType.AUTO)
    private String id;
    /**
     * 用户名
     */
    private String username;
    /**
     * 登录 IP
     */
    private String ip;
    /**
     * 登录地点
     */
    private String location;
    /**
     * 浏览器
     */
    private String browser;
    /**
     * 引擎
     */
    private String engine;
    /**
     * 平台
     */
    private String platform;
    /**
     * 操作系统
     */
    private String os;
    /**
     * 登录时间
     */
    private LocalDateTime loginTime;

}
