package com.qiuyundev.boot.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.qiuyundev.boot.base.enums.StatusEnum;
import com.qiuyundev.boot.core.mybatisplus.BaseBriefEntity;
import com.qiuyundev.boot.domain.enums.user.GenderEnum;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * <p>
 * 用户
 * </p>
 *
 * @author Qiuyun
 * @since 2024/04/11
 */
@Getter
@Setter
public class SysUser extends BaseBriefEntity {

    /**
     * 用户名 - 只能是英文字母和数字，不允许有特殊符号
     */
    private String username;

    /**
     * 密码
     */
    @JsonIgnore
    private String password;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 部门 ID
     */
    private Integer deptId;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 联系电话
     */
    private String phone;

    /**
     * 状态: DISABLE-锁定, ENABLE-有效
     */
    private StatusEnum status;

    /**
     * 最后一次登录时间
     */
    private LocalDateTime lastLoginTime;

    /**
     * 性别: MALE-男,  FEMALE-女,  UNKNOWN-未知
     */
    private GenderEnum gender;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 地区
     */
    private String district;

    /**
     * 描述
     */
    private String description;

    /**
     * 备注
     */
    private String remark;

}
