package com.qiuyundev.boot.domain.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * 用户职位
 *
 * @author Qiuyun
 * @since 2024/04/28
 */
@Getter
@Setter
public class SysUserPost {
    /**
     * 用户ID
     */
    private Integer userId;

    /**
     * 职位ID
     */
    private Integer postId;
}
