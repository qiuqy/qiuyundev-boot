package com.qiuyundev.boot.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.qiuyundev.boot.core.mybatisplus.BaseBriefEntity;
import com.qiuyundev.boot.core.mybatisplus.handler.StringListTypeHandler;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * <p>
 * 代码生成器配置
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-31
 */
@Getter
@Setter
@TableName(value = "sys_generator_config", autoResultMap = true)
public class SysGeneratorConfig {

    @TableId(type = IdType.INPUT)
    private String tableName;

    /**
     * 表单字段
     */
    @TableField(typeHandler = StringListTypeHandler.class, javaType = true)
    private List<String> formColumn;

    /**
     * 响应字段
     */
    @TableField(typeHandler = StringListTypeHandler.class, javaType = true)
    private List<String> respColumn;

    /**
     * 查询字段
     */
    @TableField(typeHandler = StringListTypeHandler.class, javaType = true)
    private List<String> queryColumn;
}
