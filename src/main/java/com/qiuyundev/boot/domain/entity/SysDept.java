package com.qiuyundev.boot.domain.entity;

import com.qiuyundev.boot.core.mybatisplus.BaseBriefEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 部门
 * </p>
 *
 * @author Qiuyun
 * @since 2024/04/11
 */
@Getter
@Setter
@Accessors(chain = true)
public class SysDept extends BaseBriefEntity {

    /**
     * 上级部门 ID （根部门 parentId 为 null）
     */
    private Integer parentId;

    /**
     * 部门祖先节点
     */
    private String nodes;

    /**
     * 部门名称
     */
    private String name;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 备注
     */
    private String remark;
}
