package com.qiuyundev.boot.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.qiuyundev.boot.core.mybatisplus.BaseBriefEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 配置
 *
 * @author Qiuyun
 * @since 2024/04/28
 */
@Getter
@Setter
public class SysConfig extends BaseBriefEntity {
    /**
     * 参数名称
     */
    private String name;
    /**
     * 参数键名
     */
    @TableField(value = "`key`")
    private String key;
    /**
     * 参数值
     */
    @TableField(value = "`value`")
    private String value;

    /**
     * 备注
     */
    private String remark;
}
