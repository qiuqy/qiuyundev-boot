package com.qiuyundev.boot.domain.handler;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;
import org.springframework.util.ObjectUtils;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

// 数据库类型
@MappedJdbcTypes(JdbcType.VARCHAR)
// java数据类型
@MappedTypes({List.class})
public abstract class AbstractListTypeHandler<T> extends BaseTypeHandler<List<T>> {

    private final ObjectMapper objectMapper = new ObjectMapper();
    @Override
    @SneakyThrows
    public void setNonNullParameter(PreparedStatement ps, int i, List<T> parameter, JdbcType jdbcType) {
        String content = ObjectUtils.isEmpty(parameter) ? null : objectMapper.writeValueAsString(parameter);
        ps.setString(i, content);
    }
    @Override
    public List<T> getNullableResult(ResultSet rs, String columnName) throws SQLException {
        return this.getListByJsonArrayString(rs.getString(columnName));
    }

    @Override
    public List<T> getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        return this.getListByJsonArrayString(rs.getString(columnIndex));
    }

    @Override
    public List<T> getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        return this.getListByJsonArrayString(cs.getString(columnIndex));
    }

    @SneakyThrows
    private List<T> getListByJsonArrayString(String content) {
        return ObjectUtils.isEmpty(content) ? new ArrayList<>() : objectMapper.readValue(content, this.specificType());
    }

    /**
     * 具体类型，由子类提供
     * @return 具体类型
     */

    protected abstract TypeReference<List<T>> specificType();
}
