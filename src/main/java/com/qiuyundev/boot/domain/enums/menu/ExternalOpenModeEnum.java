package com.qiuyundev.boot.domain.enums.menu;

import com.qiuyundev.boot.base.enums.serialize.IEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 外部打开方式枚举
 *
 * @author Qiuyun
 * @since 2024/04/18
 */
@Getter
@AllArgsConstructor
public enum ExternalOpenModeEnum implements IEnum {
    NEW_WINDOW("新窗口打开", "green"),
    EMBEDDED("内嵌页打开", "red");
    private final String text;
    private final String color;
}
