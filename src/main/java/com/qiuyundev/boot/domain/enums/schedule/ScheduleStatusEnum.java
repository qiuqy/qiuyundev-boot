package com.qiuyundev.boot.domain.enums.schedule;

import com.qiuyundev.boot.base.enums.serialize.IEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ScheduleStatusEnum implements IEnum {

    RUNNING("运行", "green"),
    STOP("停止", "red");

    private final String text;
    private final String color;
}
