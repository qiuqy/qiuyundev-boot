package com.qiuyundev.boot.domain.enums.file;


import com.qiuyundev.boot.base.enums.serialize.IEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum FileCategoryEnum implements IEnum {
    DOCUMENT("文档", "", new String[]{".doc", ".docx", ".pdf", ".txt", ".rtf", ".odt", ".wps", ".xls", ".xlsx", ".ppt", ".pptx", ".md"}),
    IMAGE("图片", "", new String[]{".jpg", ".jpeg", ".png", "vgif", ".bmp", ".tiff", ".webp", ".ico", ".svg"}),
    VIDEO("视频", "", new String[]{".mp4", ".avi", ".mov", ".wmv", ".mkv", ".flv", ".3gp", ".webm"}),
    AUDIO("音频", "", new String[]{".mp3", ".wav", ".aac", ".flac", ".ogg", ".m4a", ".wma"}),
    OTHER("其他", "", new String[]{".zip", ".rar", ".7z", ".tar", ".gz", ".bz2", ".iso", ".exe", ".dll", ".jar", ".csv", ".xml", ".json", ".html", ".htm", ".css", ".js"});

    private final String text;
    private final String color;
    private final String[] extensions;


    /**
     * 根据文件后缀判断文件类型
     *
     * @param extension 文件后缀（不包含点号）
     * @return 对应的文件类型，如果找不到则返回OTHER
     */
    public static FileCategoryEnum fromExtension(String extension) {
        for (FileCategoryEnum categoryEnum : FileCategoryEnum.values()) {
            for (String ext : categoryEnum.extensions) {
                if (ext.equalsIgnoreCase(extension)) {
                    return categoryEnum;
                }
            }
        }

        return OTHER;
    }
}




