package com.qiuyundev.boot.domain.enums.menu;

import com.qiuyundev.boot.base.enums.serialize.IEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 菜单类型枚举
 *
 * @author Qiuyun
 * @since 2024/04/18
 */
@Getter
@AllArgsConstructor
public enum MenuTypeEnum implements IEnum {
    DIRECTORY(0, "目录", "cyan"),
    MENU(1, "菜单", "green"),
    BUTTON(2, "按钮", "red");

    private final Integer code;
    private final String text;
    private final String color;
}
