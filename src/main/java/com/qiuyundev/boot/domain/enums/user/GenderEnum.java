package com.qiuyundev.boot.domain.enums.user;

import com.qiuyundev.boot.base.enums.serialize.IEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 性别
 *
 * @author Qiuyun
 * @since 2024/04/11
 */
@Getter
@AllArgsConstructor
public enum GenderEnum implements IEnum {
    /**
     * 男
     */
    MALE(1, "男", "green"),
    /**
     * 女
     */
    FEMALE(2, "女", "red"),
    /**
     * 未知
     */
    UNKNOWN(0, "未知", "default");
    /**
     * 枚举值
     */
    private final Integer code;
    private final String text;
    private final String color;
}
