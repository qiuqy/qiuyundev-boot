package com.qiuyundev.boot.domain.enums.auth;

import com.qiuyundev.boot.base.enums.serialize.IEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum LoginStatus implements IEnum {

    SUCCESS(0, "登录成功", "success"),
    FAILURE(1, "登录失败", "error");

    private final Integer code;
    private final String text;
    private final String color;
}
