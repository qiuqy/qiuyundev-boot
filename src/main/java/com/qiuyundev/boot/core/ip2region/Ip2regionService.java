package com.qiuyundev.boot.core.ip2region;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import lombok.extern.slf4j.Slf4j;
import org.lionsoul.ip2region.xdb.Searcher;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.stream.Collectors;

@Slf4j
@Service
public class Ip2regionService implements IpService {
    private Searcher searcher = null;

    @PostConstruct
    public void init() {
        // 1、从 dbPath 加载整个 xdb 到内存。
        byte[] cBuff;
        String dbFilePath;
        try {
            // 默认使用类路径下的数据文件
            Resource resource = new ClassPathResource("ip2region.xdb");
            File dbFile = resource.getFile();
            dbFilePath = dbFile.getAbsolutePath();
            cBuff = Searcher.loadContentFromFile(dbFilePath);
            // 2、使用上述的 cBuff 创建一个完全基于内存的查询对象。
            searcher = Searcher.newWithBuffer(cBuff);
        } catch (Exception e) {
            throw new RuntimeException("failed to create content cached searcher", e);
        }
    }

    @PreDestroy
    void destroy() {
        if (searcher != null) {
            try {
                searcher.close();
            } catch (IOException e) {
                log.error("关闭 Ip2region searcher 失败", e);
            }
        }
    }


    @Override
    public String search(String ipAddress) {
        String region;
        try {
            region = searcher.search(ipAddress);
            if (StringUtils.hasText(region)) {
                String[] regionArray = region.split("\\|");
                if (regionArray.length == 5) {
                    region = Arrays.stream(regionArray)
                            .limit(4)
                            .filter(e -> !"0".equals(e))
                            .collect(Collectors.joining());
                }
            }
        } catch (Exception e) {
           log.error("查询IP归属地失败", e);
           region = null;
        }

        return "内网IP".equals(region) ? null : region;
    }
}
