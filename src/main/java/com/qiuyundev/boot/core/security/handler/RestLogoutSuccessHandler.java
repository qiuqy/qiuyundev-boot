package com.qiuyundev.boot.core.security.handler;

import com.qiuyundev.boot.base.util.ResponseUtils;
import com.qiuyundev.boot.base.vo.Response;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;
import org.springframework.stereotype.Component;

/**
 * 注销成功处理程序
 *
 * @author Qiuyun
 * @since 2024/04/19
 */
@Component
@RequiredArgsConstructor
public class  RestLogoutSuccessHandler extends SimpleUrlLogoutSuccessHandler {
    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        ResponseUtils.renderToString(response, Response.success());
    }
}
