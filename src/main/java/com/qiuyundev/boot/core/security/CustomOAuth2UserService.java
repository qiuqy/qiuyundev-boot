//package com.qiuyundev.boot.core.security;
//
//import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
//import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
//import org.springframework.security.oauth2.client.userinfo.OAuth2UserService;
//import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
//import org.springframework.security.oauth2.core.user.OAuth2User;
//import org.springframework.stereotype.Service;
//
//@Service
//public class CustomOAuth2UserService implements OAuth2UserService<OAuth2UserRequest, OAuth2User> {
//
//    @Override
//    public OAuth2User loadUser(OAuth2UserRequest userRequest) throws OAuth2AuthenticationException {
//        OAuth2UserService<OAuth2UserRequest, OAuth2User> delegate = new DefaultOAuth2UserService();
//        OAuth2User oAuth2User = delegate.loadUser(userRequest);
//        // 根据OAuth2User中的信息（如email、id等），查找或创建本地用户
//        LoginUser loginUser = new LoginUser();
//        LoginUser.User user = new LoginUser.User();
//        user.setUsername(oAuth2User.getName());
//
//        // 然后，你可以返回一个自定义的Principal对象，该对象可以包含更多你希望在SecurityContext中使用的用户信息
//        return loginUser;
//    }
//}
