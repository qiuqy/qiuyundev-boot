package com.qiuyundev.boot.core.security;


/**
 * Security 中 Redis key 常数
 *
 * @author Qiuyun
 * @since 2024/04/17
 */
public interface SecurityConstants {

    /**
     * jwt claim 中用户信息缓存的 key
     */
    String CLAIM_USER_KEY = "USER_KEY";

    /**
     * 登录用户缓存前缀
     */
    String LOGIN_USER_CACHE_KEY_PREFIX = "LOGIN:USER:";


    /**
     * 登录处理 url
     */
    String LOGIN_PROCESSING_URL = "/auth/login";

    /**
     * 退出登录处理 url
     */
    String LOGOUT_URL = "/account/logout";


    /**
     * 超级管理员角色
     */
    String ROLE_ADMIN = "admin";


    /**
     * 请求头： Authorization
     */
    String HEADER_AUTHORIZATION = "Authorization";

    /**
     * 请求头： token
     */
    String HEADER_TOKEN = "token";
}
