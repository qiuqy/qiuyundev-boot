package com.qiuyundev.boot.core.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.qiuyundev.boot.core.datascope.DataScopeEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * 登录用户
 *
 * @author Qiuyun
 * @since 2024/04/17
 */
@Getter
@Setter
@NoArgsConstructor
public class LoginUser implements UserDetails {
    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 用户信息
     */
    private User user;
    /**
     * 角色列表
     */
    private List<Role> roles;
    /**
     * 权限列表
     */
    private List<String> permissions;
    /**
     * 登录信息
     */
    private LoginInfo loginInfo;
    /**
     * 过期时间 - 时间戳
     */
    private Long expireTime;

    @JsonIgnore
    private List<SimpleGrantedAuthority> authorities;

    @Override
    public Collection<SimpleGrantedAuthority> getAuthorities() {
        authorities = new ArrayList<>();
        // 角色
        if (!ObjectUtils.isEmpty(this.roles)) {
            authorities.addAll(roles.stream()
                    .map(role -> new SimpleGrantedAuthority("ROLE_" + role.getCode()))
                    .toList());
        }
        // 权限
        if (!ObjectUtils.isEmpty(this.permissions)) {
            authorities.addAll(this.permissions.stream()
                    .filter(StringUtils::hasText)
                    .map(SimpleGrantedAuthority::new)
                    .toList());
        }

        return authorities;
    }

    @Override
    @JsonIgnore
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    @JsonIgnore
    public String getUsername() {
        return user.getUsername();
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    @JsonIgnore
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    @JsonIgnore
    public boolean isEnabled() {
        return true;
    }

    @Getter
    @Setter
    public static class User implements Serializable {
        @Serial
        private static final long serialVersionUID = 1L;

        /**
         * 用户 ID
         */
        private Integer id;

        /**
         * 部门 ID
         */
        private Integer deptId;

        /**
         * 部门
         */
        private String deptName;

        /**
         * 用户名
         */
        private String username;

        /**
         * 密码
         */
        @JsonIgnore
        private String password;

        /**
         * 昵称
         */
        private String nickname;

        /**
         * 头像
         */
        private String avatar;

        /**
         * 邮箱
         */
        private String email;

        /**
         * 手机号
         */
        private String phone;
    }

    @Getter
    @Setter
    public static class Role implements Serializable {
        @Serial
        private static final long serialVersionUID = 1L;
        /**
         * 角色 ID
         */
        private Integer id;

        /**
         * 角色编号
         */
        private String code;
        /**
         * 数据权限
         */
        private DataScopeEnum dataScope;
    }

    /**
     * 登录信息
     *
     * @author Qiuyun
     * @since 2024/04/26
     */
    @Getter
    @Setter
    public static class LoginInfo implements Serializable {
        @Serial
        private static final long serialVersionUID = 1L;
        /**
         * 用户缓存标识
         */
        private String userKey;
        /**
         * 用户名
         */
        private String username;
        /**
         * 部门 id
         */
        private Integer deptId;
        /**
         * 部门
         */
        private String deptName;
        /**
         * 登录 IP
         */
        private String ip;
        /**
         * 登录地点
         */
        private String location;
        /**
         * 浏览器
         */
        private String browser;

        /**
         * 引擎
         */
        private String engine;

        /**
         * 平台
         */
        private String platform;
        /**
         * 操作系统
         */
        private String os;
        /**
         * 登录时间
         */
        private LocalDateTime loginTime;
    }

}
