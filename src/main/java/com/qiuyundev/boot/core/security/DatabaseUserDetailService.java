package com.qiuyundev.boot.core.security;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.qiuyundev.boot.base.enums.response.AuthResponseEnum;
import com.qiuyundev.boot.domain.entity.SysUser;
import com.qiuyundev.boot.service.ISysUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;


/**
 * UserDetailsService 实现类
 *
 * @author Qiuyun
 * @since 2024/04/17
 */
@Service
@RequiredArgsConstructor
public class DatabaseUserDetailService implements UserDetailsService {
    private final ISysUserService userService;
    private final PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // 根据用户名或邮箱查询用户
        // 如果包含 @ 符号，根据邮箱查询，否则根据用户名查询
        SysUser sysUser = userService.getOne(Wrappers.<SysUser>lambdaQuery()
                        .eq(!username.contains("@"), SysUser::getUsername, username)
                        .eq(username.contains("@"), SysUser::getEmail, username),
                false);
        Assert.notNull(sysUser, AuthResponseEnum.ACCOUNT_ERROR.getMessage());
        LoginUser loginUser = new LoginUser();
        LoginUser.User user = new LoginUser.User();
        user.setId(sysUser.getId());
        user.setUsername(sysUser.getUsername());
        user.setPassword(this.passwordEncoder.encode(sysUser.getPassword()));
        user.setDeptId(sysUser.getDeptId());
        user.setNickname(sysUser.getNickname());
        user.setAvatar(sysUser.getAvatar());
        user.setEmail(sysUser.getEmail());
        user.setPhone(sysUser.getPhone());
        loginUser.setUser(user);
        return loginUser;
    }
}
