package com.qiuyundev.boot.core.security.event;

import org.springframework.context.event.EventListener;
import org.springframework.security.authentication.event.LogoutSuccessEvent;
import org.springframework.stereotype.Component;


@Component
public class LogoutEvents {
    @EventListener
    public void onLogout(LogoutSuccessEvent logout) {
    }
}
