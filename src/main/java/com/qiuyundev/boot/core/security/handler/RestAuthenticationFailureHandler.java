package com.qiuyundev.boot.core.security.handler;

import com.qiuyundev.boot.base.enums.response.AuthResponseEnum;
import com.qiuyundev.boot.base.util.ResponseUtils;
import com.qiuyundev.boot.base.vo.Response;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.*;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;


/**
 * 身份验证失败处理程序
 *
 * @author Qiuyun
 * @since 2024/04/17
 */
@Component
@RequiredArgsConstructor
public class RestAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
                                        AuthenticationException exception) {
//        exception.getLocalizedMessage();
        if (exception instanceof UsernameNotFoundException || exception instanceof BadCredentialsException) {
            ResponseUtils.renderToString(response, Response.failed(AuthResponseEnum.ACCOUNT_ERROR));
        } else if (exception instanceof DisabledException) {
            ResponseUtils.renderToString(response, Response.failed(AuthResponseEnum.ACCOUNT_DISABLED_ERROR));
        } else if (exception instanceof LockedException) {
            ResponseUtils.renderToString(response, Response.failed(AuthResponseEnum.ACCOUNT_LOCKED_ERROR));
        } else if (exception instanceof AccountExpiredException) {
            ResponseUtils.renderToString(response, Response.failed(AuthResponseEnum.ACCOUNT_EXPIRED_ERROR));
        } else if (exception instanceof CredentialsExpiredException) {
            ResponseUtils.renderToString(response, Response.failed(AuthResponseEnum.CREDENTIALS_EXPIRED_ERROR));
        }

        ResponseUtils.renderToString(response, Response.failed(exception.getMessage()));
    }
}
