package com.qiuyundev.boot.core.security.event.custom;

import com.qiuyundev.boot.core.security.LoginUser;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
public class OnlineUserEventPublisher {
    private final ApplicationEventPublisher eventPublisher;

    public OnlineUserEventPublisher(ApplicationEventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }

    public void publish(OnlineUserEventTypeEnum type, LoginUser.LoginInfo loginInfo, Long expireTime) {
        this.eventPublisher.publishEvent(new OnlineUserEventPublisher.OnlineUserEvent(this, type, loginInfo, expireTime));
    }

    @Getter
    public static class OnlineUserEvent extends ApplicationEvent {
        private final OnlineUserEventTypeEnum type;
        private final LoginUser.LoginInfo loginInfo;
        private final Long expireTime;

        public OnlineUserEvent(Object source,OnlineUserEventTypeEnum type, LoginUser.LoginInfo loginInfo, Long expireTime) {
            super(source);
            this.type = type;
            this.loginInfo = loginInfo;
            this.expireTime = expireTime;
        }

    }

    public enum OnlineUserEventTypeEnum {
        LOGIN,
        LOGOUT,
        UPDATE_EXPIRE_TIME,
    }
}
