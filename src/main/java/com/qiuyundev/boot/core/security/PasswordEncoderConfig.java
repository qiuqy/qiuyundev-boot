package com.qiuyundev.boot.core.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class PasswordEncoderConfig {

    @Bean
    public PasswordEncoder passwordEncoder() {
        // 不加密
//        return NoOpPasswordEncoder.getInstance();
        // 工作因子，默认 10，范围 4 - 31，值越大，加密越慢，安全越高
        return new BCryptPasswordEncoder(4);
    }

}
