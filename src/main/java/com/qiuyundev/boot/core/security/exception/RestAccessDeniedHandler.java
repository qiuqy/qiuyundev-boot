package com.qiuyundev.boot.core.security.exception;

import com.qiuyundev.boot.base.enums.response.AuthResponseEnum;
import com.qiuyundev.boot.base.util.ResponseUtils;
import com.qiuyundev.boot.base.vo.Response;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * 拒绝访问处理器
 *
 * @author Qiuyun
 * @since 2024/04/17
 */
@Component
public class RestAccessDeniedHandler implements AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        ResponseUtils.renderToString(response, Response.failed(AuthResponseEnum.FORBIDDEN));
    }
}
