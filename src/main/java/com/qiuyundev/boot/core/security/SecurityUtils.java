package com.qiuyundev.boot.core.security;

/**
 * SecurityUtils
 *
 * @author Qiuyun
 * @date 2023/1/19
 */

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Collection;

/**
 * Security工具类
 *
 * @author Qiuyun
 * @since 2024/04/17
 */

public class SecurityUtils {

    /**
     * 私有化构造器
     */
    private SecurityUtils() {
    }

    /**
     * 获取当前用户信息
     */
    public static LoginUser getUserInfo() {
        return (LoginUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    /**
     * 获取当前用户权限
     */
    public static Collection<? extends GrantedAuthority> getUserAuthorities() {
        return SecurityContextHolder.getContext().getAuthentication().getAuthorities();
    }

    /**
     * 获取当前用户 ID
     */
    public static Integer getUserId() {
        return getUserInfo().getUser().getId();
    }

    /**
     * 获取当前用户账号
     */
    public static String getUsername() {
        return getUserInfo().getUsername();
    }

    /**
     * 是否超级管理员
     * <p>
     * 超级管理员忽视任何权限判断
     */
    public static boolean isAdmin() {
        return getUserInfo().getRoles()
                .stream()
                .anyMatch(role -> role.getCode().equals(SecurityConstants.ROLE_ADMIN));
    }

}
