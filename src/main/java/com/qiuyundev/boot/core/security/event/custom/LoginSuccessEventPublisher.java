package com.qiuyundev.boot.core.security.event.custom;

import cn.hutool.http.useragent.UserAgent;
import cn.hutool.http.useragent.UserAgentUtil;
import com.qiuyundev.boot.base.util.IPUtils;
import com.qiuyundev.boot.core.ip2region.IpService;
import com.qiuyundev.boot.core.security.LoginUser;
import jakarta.servlet.http.HttpServletRequest;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;

@Component
@RequiredArgsConstructor
public class LoginSuccessEventPublisher {
    private final ApplicationEventPublisher eventPublisher;
    private final IpService ipService;

    public void publish(LoginUser.LoginInfo loginInfo) {
        this.eventPublisher.publishEvent(new LoginSuccessEvent(this, loginInfo));
    }

    @Getter
    public static class LoginSuccessEvent extends ApplicationEvent {
        private final LoginUser.LoginInfo loginInfo;

        public LoginSuccessEvent(Object source, LoginUser.LoginInfo loginInfo) {
            super(source);
            this.loginInfo = loginInfo;
        }

    }


    /**
     * 获取登录信息
     * <p>
     * 用于保存登录日志和在线用户展示时使用
     *
     * @param request   请求
     * @param loginUser 登录用户
     * @return 登录信息
     */
    public LoginUser.LoginInfo getLoginInfo(HttpServletRequest request, LoginUser loginUser) {
        LoginUser.LoginInfo loginInfo = new LoginUser.LoginInfo();
        loginInfo.setUsername(loginUser.getUsername());
        loginInfo.setDeptId(loginUser.getUser().getDeptId());
        loginInfo.setDeptName(loginUser.getUser().getDeptName());
        String clientIP = IPUtils.getIp(request);
        if (StringUtils.hasText(clientIP)) {
            loginInfo.setIp(clientIP);
            loginInfo.setLocation(ipService.search(clientIP));
        }
        UserAgent userAgent = UserAgentUtil.parse(request.getHeader("User-Agent"));
        if (!userAgent.getBrowser().isUnknown()) {
            loginInfo.setBrowser(userAgent.getBrowser().getName() + " " + userAgent.getVersion());
        }
        if (!userAgent.getEngine().isUnknown()) {
            loginInfo.setBrowser(userAgent.getEngine().getName() + " " + userAgent.getEngineVersion());
        }
        if (!userAgent.getPlatform().isUnknown()) {
            loginInfo.setPlatform(userAgent.getPlatform().getName());
        }
        if (!userAgent.getOs().isUnknown()) {
            loginInfo.setOs(userAgent.getOs().getName() + " " + userAgent.getOsVersion());
        }

        loginInfo.setLoginTime(LocalDateTime.now());
        return loginInfo;
    }
}
