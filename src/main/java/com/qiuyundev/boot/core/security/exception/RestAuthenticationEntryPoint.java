package com.qiuyundev.boot.core.security.exception;

import com.qiuyundev.boot.base.enums.response.AuthResponseEnum;
import com.qiuyundev.boot.base.util.ResponseUtils;
import com.qiuyundev.boot.base.vo.Response;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;


/**
 * 认证失败的处理
 *<p>
 * 尚未登录或者登录过期的时候，返回 401
 *
 * @author Qiuyun
 * @since 2024/04/17
 */
@Component
public class RestAuthenticationEntryPoint implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) {
        ResponseUtils.renderToString(response, Response.failed(AuthResponseEnum.UNAUTHORIZED));
    }
}
