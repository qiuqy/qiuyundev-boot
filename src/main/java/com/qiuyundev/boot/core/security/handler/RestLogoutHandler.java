package com.qiuyundev.boot.core.security.handler;

import com.qiuyundev.boot.core.security.JwtService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Component;

/**
 * 注销处理程序
 * <p>
 * 未过 token filter
 *
 * @author Qiuyun
 * @since 2024/04/19
 */
@Component
@RequiredArgsConstructor
public class RestLogoutHandler implements LogoutHandler {
    private final JwtService jwtService;

    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        // 清除用户信息缓存
        this.jwtService.clearUserInfoCache(request);
    }
}
