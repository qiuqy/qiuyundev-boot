package com.qiuyundev.boot.core.security.handler;

import cn.hutool.core.lang.Pair;
import com.qiuyundev.boot.base.util.MultiThreadUtils;
import com.qiuyundev.boot.base.util.ResponseUtils;
import com.qiuyundev.boot.base.vo.Response;
import com.qiuyundev.boot.core.security.JwtService;
import com.qiuyundev.boot.core.security.LoginUser;
import com.qiuyundev.boot.core.security.event.custom.LoginSuccessEventPublisher;
import com.qiuyundev.boot.domain.vo.account.AccountMenuResp;
import com.qiuyundev.boot.domain.vo.auth.LoginResp;
import com.qiuyundev.boot.domain.vo.dept.DeptResp;
import com.qiuyundev.boot.service.ISysDeptService;
import com.qiuyundev.boot.service.ISysMenuService;
import com.qiuyundev.boot.service.ISysRoleService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;


/**
 * 身份验证成功处理程序
 *
 * @author Qiuyun
 * @since 2024/04/17
 */
@Component
@RequiredArgsConstructor
public class RestAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {
    private final JwtService jwtService;
    private final ISysRoleService roleService;
    private final ISysMenuService menuService;
    private final LoginSuccessEventPublisher loginSuccessEventPublisher;
    private final ISysDeptService deptService;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) {
        LoginUser loginUser = (LoginUser) authentication.getPrincipal();
        // 多线程执行 - 响应速度快点
        MultiThreadUtils.executeAsyncTasks(() -> {
            List<String> perms = this.menuService.selectPermsByUserId(loginUser.getUser().getId());
            loginUser.setPermissions(perms);
        }, () -> {
            // 查询角色
            List<LoginUser.Role> roles = roleService.findByUserId(loginUser.getUser().getId())
                    .stream()
                    .map(sysRole -> {
                        LoginUser.Role role = new LoginUser.Role();
                        role.setId(sysRole.getId());
                        role.setCode(sysRole.getCode());
                        role.setDataScope(sysRole.getDataScope());
                        return role;
                    })
                    .collect(Collectors.toList());
            loginUser.setRoles(roles);
        }, () -> {
            // 查询部门信息
            if (null != loginUser.getUser().getDeptId()) {
                DeptResp deptResp = this.deptService.detail(loginUser.getUser().getDeptId());
                if (null != deptResp) {
                    loginUser.getUser().setDeptName(deptResp.getName());
                }
            }
            // 设置登录用户信息
            LoginUser.LoginInfo loginInfo = this.loginSuccessEventPublisher.getLoginInfo(request, loginUser);
            loginUser.setLoginInfo(loginInfo);
        });

        // 创建 token 并缓存用户信息
        String token = this.jwtService.createTokenAndCacheUserInfo(loginUser);
        // 登录日志
        this.loginSuccessEventPublisher.publish(loginUser.getLoginInfo());
        // 封装返回结果
        ResponseUtils.renderToString(response, Response.success(LoginResp.builder().token(token).build()));
    }


}
