//package com.qiuyundev.boot.core.security.login.email;
//
//import org.springframework.security.authentication.AuthenticationProvider;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.AuthenticationException;
//import org.springframework.security.core.userdetails.UserDetails;
//
//public class EmailAuthenticationProvider implements AuthenticationProvider {
//    @Override
//    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
//            String email = authentication.getPrincipal().toString();
//            String emailCode = authentication.getCredentials().toString();
//
//            // 在这里实现邮箱验证的逻辑，根据邮箱和验证码进行验证
//            UserDetails userDetails = userDetailsService.loadUserByUsername(email);
//
//            // 验证通过，创建认证后的Token
//            EmailAuthenticationToken authenticatedToken = new EmailAuthenticationToken(email, emailCode, userDetails.getAuthorities());
//            authenticatedToken.setDetails(authentication.getDetails());
//            return authenticatedToken;
//    }
//
//    @Override
//    public boolean supports(Class<?> authentication) {
//            return EmailAuthenticationToken.class.isAssignableFrom(authentication);
//    }
//}
