package com.qiuyundev.boot.core.security.filter;

import com.qiuyundev.boot.base.util.ResponseUtils;
import com.qiuyundev.boot.base.vo.Response;
import com.qiuyundev.boot.config.prpperties.CaptchaProperties;
import com.qiuyundev.boot.core.captcha.AbstractCaptchaService;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

import static com.qiuyundev.boot.core.security.SecurityConstants.LOGIN_PROCESSING_URL;

/**
 * 验证码验证过滤器
 *
 * @author Qiuyun
 * @since 2024/04/17
 */
@Component
@RequiredArgsConstructor
public class CaptchaValidationFilter extends OncePerRequestFilter {
    private final AbstractCaptchaService captchaService;
    private final CaptchaProperties captchaProperties;
    private final AntPathRequestMatcher loginRequestMatcher = new AntPathRequestMatcher(LOGIN_PROCESSING_URL, "POST");

    @Override
    protected void doFilterInternal(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull FilterChain filterChain) throws ServletException, IOException {
        if (captchaProperties.isEnabled() && loginRequestMatcher.matches(request)) {
            // 请求中的验证码 CODE
            String captchaCode = request.getParameter(AbstractCaptchaService.CAPTCHA_CODE_FIELD_NAME);
            // 请求中的验证码 KEY
            String captchaKey = request.getParameter(AbstractCaptchaService.CAPTCHA_KEY_FIELD_NAME);
            if (StringUtils.hasText(captchaCode) && StringUtils.hasText(captchaKey)) {
                if (this.captchaService.verifyCaptcha(captchaCode, captchaKey)) {
                    filterChain.doFilter(request, response);
                } else {
                    ResponseUtils.renderToString(response, Response.failed("验证码错误或验证码已过期"));
                }
            } else {
                ResponseUtils.renderToString(response, Response.failed("验证码错误"));
            }
        } else {
            filterChain.doFilter(request, response);
        }
    }
}
