//package com.qiuyundev.boot.core.security.login.email;
//
//import jakarta.servlet.ServletException;
//import jakarta.servlet.http.HttpServletRequest;
//import jakarta.servlet.http.HttpServletResponse;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.AuthenticationException;
//import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
//import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
//
//import java.io.IOException;
//
//public class EmailAuthenticationFilter extends AbstractAuthenticationProcessingFilter {
//    private static final AntPathRequestMatcher DEFAULT_ANT_PATH_REQUEST_MATCHER = new AntPathRequestMatcher("/login/email",
//            "POST");
//
//    public EmailAuthenticationFilter() {
//        super(DEFAULT_ANT_PATH_REQUEST_MATCHER);
//    }
//
//    @Override
//    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
//        String email = obtainEmail(request);
//        String emailCode = obtainCode(request);
//
//        if (email == null) {
//            email = "";
//        }
//        if (emailCode == null) {
//            emailCode = "";
//        }
//
//        email = email.trim();
//        EmailAuthenticationToken authRequest = new EmailAuthenticationToken(email, emailCode);
//        setDetails(request, authRequest);
//        return this.getAuthenticationManager().authenticate(authRequest);
//    }
//
//    private String obtainEmail(HttpServletRequest request) {
//        // 从请求中获取邮箱
//        return request.getParameter("email");
//    }
//
//    private String obtainCode(HttpServletRequest request) {
//        // 从请求中获取邮箱验证码
//        return request.getParameter("code");
//    }
//
//    private void setDetails(HttpServletRequest request, EmailAuthenticationToken authRequest) {
//        authRequest.setDetails(authenticationDetailsSource.buildDetails(request));
//    }
//
//
//}
