package com.qiuyundev.boot.core.security;

import cn.hutool.core.util.IdUtil;
import com.qiuyundev.boot.config.prpperties.JwtProperties;
import com.qiuyundev.boot.core.security.event.custom.OnlineUserEventPublisher;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.crypto.SecretKey;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.qiuyundev.boot.core.security.SecurityConstants.*;
import static java.lang.System.currentTimeMillis;


/**
 * JWT 工具类
 */
@Component
public class JwtService {
    /**
     * jwt参数
     */
    @Resource
    private JwtProperties jwtProperties;

    @Resource
    private RedisTemplate<String, LoginUser> redisTemplate;

    @Resource
    private OnlineUserEventPublisher onlineUserEventPublisher;

    /**
     * 密钥
     */
    private SecretKey secretKey;

    /**
     * 初始化密钥
     */
    @PostConstruct
    public void init() {
        this.secretKey = Keys.hmacShaKeyFor(jwtProperties.getSecret().getBytes());
    }

    public List<LoginUser> getLoginUserList(List<String> userKeys) {
        return this.redisTemplate.opsForValue().multiGet(userKeys);
    }

    /**
     * 创建 token 并缓存用户信息
     *
     * @param loginUser 登录用户数据
     * @return 令牌
     */
    public String createTokenAndCacheUserInfo(LoginUser loginUser) {
        Map<String, Object> claims = new HashMap<>(1);
        String userKey = IdUtil.getSnowflake().nextIdStr();
        claims.put(CLAIM_USER_KEY, userKey);
        String token = createToken(claims);
        // 缓存 token
        String loginUserCacheKey = LOGIN_USER_CACHE_KEY_PREFIX + userKey;
        loginUser.setExpireTime(currentTimeMillis() + jwtProperties.getTtl());
        loginUser.getLoginInfo().setUserKey(userKey);
        this.redisTemplate.opsForValue().set(loginUserCacheKey, loginUser, jwtProperties.getTtl(), TimeUnit.MILLISECONDS);
        // 在线用户统计
        this.onlineUserEventPublisher.publish(OnlineUserEventPublisher.OnlineUserEventTypeEnum.LOGIN,
                loginUser.getLoginInfo(),
                loginUser.getExpireTime());
        return token;
    }

    /**
     * 从令牌中获取用户信息以及刷新有效时间
     *
     * @param request http 请求
     * @return 用户信息
     */
    public LoginUser parseTokenAndRefresh(HttpServletRequest request) {
        String token = request.getHeader(HEADER_AUTHORIZATION);
        if (!StringUtils.hasText(token)) {
            // 从请求路径参数中获取token
            token = request.getParameter(HEADER_TOKEN);
        }
        if (StringUtils.hasText(token)) {
            Claims claims = parseToken(token);
            // 解析对应的权限以及用户信息
            String userKey = (String) claims.get(CLAIM_USER_KEY);
            String loginUserCacheKey = LOGIN_USER_CACHE_KEY_PREFIX + userKey;
            LoginUser loginUser = this.redisTemplate.opsForValue().get(loginUserCacheKey);
            if (null != loginUser) {
                // 过期时间小于 token有效期 * 0.66 毫秒，刷新 token 时间
                if (loginUser.getExpireTime() - currentTimeMillis() < jwtProperties.getTtl() * 0.66) {
                    loginUser.setExpireTime(currentTimeMillis() + jwtProperties.getTtl());
                    // 覆盖之前的缓存信息
                    this.redisTemplate.opsForValue().set(loginUserCacheKey, loginUser, jwtProperties.getTtl(), TimeUnit.MILLISECONDS);
                    // 在线用户统计
                    this.onlineUserEventPublisher.publish(
                            OnlineUserEventPublisher.OnlineUserEventTypeEnum.UPDATE_EXPIRE_TIME,
                            loginUser.getLoginInfo(),
                            loginUser.getExpireTime());
                }
            }
            return loginUser;
        }

        return null;
    }


    /**
     * 清除用户信息缓存
     *
     * @param request http 请求
     */
    public void clearUserInfoCache(HttpServletRequest request) {
        String token = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (StringUtils.hasText(token)) {
            Claims claims = parseToken(token);
            // 解析对应的权限以及用户信息
            String userKey = (String) claims.get(CLAIM_USER_KEY);
            String loginUserCacheKey = LOGIN_USER_CACHE_KEY_PREFIX + userKey;
            // 在线用户统计
            this.onlineUserEventPublisher.publish(
                    OnlineUserEventPublisher.OnlineUserEventTypeEnum.LOGOUT,
                    SecurityUtils.getUserInfo().getLoginInfo(),
                    null);
            this.redisTemplate.delete(loginUserCacheKey);


        }
    }

    /**
     * 下线用户
     *
     * @param userKey 用户缓存key
     * @return {@link Boolean}
     */
    public Boolean kickUser(String userKey) {
        String loginUserCacheKey = LOGIN_USER_CACHE_KEY_PREFIX + userKey;
        return this.redisTemplate.delete(loginUserCacheKey);
    }

    /**
     * 从数据声明生成令牌
     *
     * @param claims 数据声明
     * @return 令牌
     */
    public String createToken(Map<String, Object> claims) {
        return Jwts.builder()
                // 签发者
                .issuer(jwtProperties.getIssuer())
                .claims(claims)
                .signWith(secretKey)
                .compact();
    }

    /**
     * 从令牌中获取数据声明
     *
     * @param token 令牌
     * @return 数据声明
     */
    public Claims parseToken(String token) {
        return Jwts.parser()
                .verifyWith(secretKey)
                .build()
                .parseSignedClaims(token)
                .getPayload();
    }

}
