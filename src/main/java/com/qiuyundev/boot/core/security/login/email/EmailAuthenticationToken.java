//package com.qiuyundev.boot.core.security.login.email;
//
//import org.springframework.security.authentication.AbstractAuthenticationToken;
//
//public class EmailAuthenticationToken extends AbstractAuthenticationToken {
//
//    private final String email;
//    private final String code;
//
//    public EmailAuthenticationToken(String email, String code) {
//        super(null);
//        this.email = email;
//        this.code = code;
//        setAuthenticated(false);
//    }
//
//
//    @Override
//    public Object getCredentials() {
//        return this.code;
//    }
//
//    @Override
//    public Object getPrincipal() {
//        return this.email;
//    }
//}
