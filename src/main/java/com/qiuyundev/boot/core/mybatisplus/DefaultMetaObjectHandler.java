package com.qiuyundev.boot.core.mybatisplus;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.qiuyundev.boot.core.security.SecurityUtils;
import org.apache.ibatis.reflection.MetaObject;

import java.time.LocalDateTime;

/**
 * 字段默认值填充处理
 *
 * @author Qiuyun
 * @date 2023/07/17
 */
public class DefaultMetaObjectHandler implements MetaObjectHandler {

    private static final String CREATE_TIME_FIELD = "createTime";
    private static final String CREATE_BY_FIELD = "createBy";
    private static final String UPDATE_TIME_FIELD = "updateTime";
    private static final String UPDATE_BY_FIELD = "updateBy";
    private static final String IS_DELETED_FIELD = "isDeleted";

    @Override
    public void insertFill(MetaObject metaObject) {
        if (metaObject.hasGetter(CREATE_TIME_FIELD)) {
            if (null == metaObject.getValue(CREATE_TIME_FIELD)) {
                this.setFieldValByName(CREATE_TIME_FIELD, LocalDateTime.now(), metaObject);
            }
        }

        if (metaObject.hasGetter(CREATE_BY_FIELD)) {
            if (null == metaObject.getValue(CREATE_BY_FIELD)) {
                this.setFieldValByName(CREATE_BY_FIELD, SecurityUtils.getUsername(), metaObject);
            }
        }

        this.updateFill(metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        if (metaObject.hasGetter(UPDATE_TIME_FIELD)) {
            if (null == metaObject.getValue(UPDATE_TIME_FIELD)) {
                this.setFieldValByName(UPDATE_TIME_FIELD, LocalDateTime.now(), metaObject);
            }
        }
        if (metaObject.hasGetter(UPDATE_BY_FIELD)) {
            if (null == metaObject.getValue(UPDATE_BY_FIELD)) {
                this.setFieldValByName(UPDATE_BY_FIELD, SecurityUtils.getUsername(), metaObject);
            }
        }

    }
}
