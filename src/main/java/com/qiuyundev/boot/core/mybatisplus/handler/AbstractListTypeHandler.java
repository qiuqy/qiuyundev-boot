package com.qiuyundev.boot.core.mybatisplus.handler;

import cn.hutool.core.util.StrUtil;
import com.qiuyundev.boot.base.util.JacksonUtils;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;
import org.springframework.util.ObjectUtils;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

// 数据库类型
@MappedJdbcTypes(JdbcType.VARCHAR)
// java数据类型
@MappedTypes({List.class})
public abstract class AbstractListTypeHandler<T> extends BaseTypeHandler<List<T>> {
    /**
     * 具体类型,由子类实现
     *
     * @return 具体类型
     */
    protected abstract Class<T> getType();

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, List<T> parameter, JdbcType jdbcType) throws SQLException {
        String context = ObjectUtils.isEmpty(parameter) ? null : JacksonUtils.toJson(parameter);
        ps.setString(i, context);
    }

    @Override
    public List<T> getNullableResult(ResultSet rs, String columnName) throws SQLException {
        return getResult(rs.getString(columnName));
    }

    @Override
    public List<T> getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        return getResult(rs.getString(columnIndex));
    }

    @Override
    public List<T> getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        return getResult(cs.getString(columnIndex));
    }

    /**
     * 根据json字符串格式化成List
     */
    private List<T> getResult(String context) {
        return StrUtil.isBlank(context) ? new ArrayList<>() : JacksonUtils.toList(context, getType());
    }
}
