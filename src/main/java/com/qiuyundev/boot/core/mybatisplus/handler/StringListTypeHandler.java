package com.qiuyundev.boot.core.mybatisplus.handler;


public class StringListTypeHandler extends AbstractListTypeHandler<String> {
    @Override
    protected Class<String> getType() {
        return String.class;
    }
}
