package com.qiuyundev.boot.core.mybatisplus.handler;


public class LongListTypeHandler extends AbstractListTypeHandler<Long> {
    @Override
    protected Class<Long> getType() {
        return Long.class;
    }
}
