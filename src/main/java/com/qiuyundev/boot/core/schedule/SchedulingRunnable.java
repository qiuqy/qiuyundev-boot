package com.qiuyundev.boot.core.schedule;

import com.qiuyundev.boot.base.enums.ApiStatusEnum;
import com.qiuyundev.boot.base.util.SpringUtils;
import com.qiuyundev.boot.domain.entity.SysScheduleLog;
import com.qiuyundev.boot.service.ISysScheduleLogService;
import com.qiuyundev.boot.service.ISysScheduleService;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Objects;

@Slf4j
public class SchedulingRunnable implements Runnable {

    private final Integer scheduleId;
    private final String scheduleName;
    private final String invokeTarget;
    private final ISysScheduleLogService scheduleLogService = SpringUtils.getBean(ISysScheduleLogService.class);


    public SchedulingRunnable(Integer scheduleId, String scheduleName, String invokeTarget) {
        this.scheduleId = scheduleId;
        this.scheduleName = scheduleName;
        this.invokeTarget = invokeTarget;
    }

    @Override
    public void run() {
        log.info("定时任务开始执行 - invokeTarget：{}", invokeTarget);
        LocalDateTime startTime = LocalDateTime.now();
        Exception exception = null;
        try {
            MethodInvokerUtils.invokeMethod(invokeTarget);
        } catch (Exception ex) {
            log.error(String.format("定时任务执行异常 - invokeTarget: %s", invokeTarget), ex);
            exception = ex;
        } finally {
            long costTime = System.currentTimeMillis() - startTime.toInstant(ZoneOffset.of("+8")).toEpochMilli();
            log.info("定时任务执行结束 - invokeTarget：{}，耗时：{} 毫秒", invokeTarget,  costTime);
            // 保存任务执行日志
            this.scheduleLogService.saveScheduleLog(exception, costTime, startTime, scheduleId, scheduleName);
        }
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SchedulingRunnable other = (SchedulingRunnable) o;
        return this.scheduleId.equals(other.scheduleId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.scheduleId);
    }
}
