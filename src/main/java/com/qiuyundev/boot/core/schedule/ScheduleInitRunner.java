package com.qiuyundev.boot.core.schedule;

import com.qiuyundev.boot.domain.entity.SysSchedule;
import com.qiuyundev.boot.domain.enums.schedule.ScheduleStatusEnum;
import com.qiuyundev.boot.service.ISysScheduleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class ScheduleInitRunner implements CommandLineRunner {
    private final CronTaskRegistrar cronTaskRegistrar;
    private final ISysScheduleService scheduleService;
    @Override
    public void run(String... args) {
        // 初始加载数据库里状态为正常的定时任务
        List<SysSchedule> schedules = this.scheduleService.lambdaQuery().eq(SysSchedule::getStatus, ScheduleStatusEnum.RUNNING).list();
        if (!ObjectUtils.isEmpty(schedules)) {
            for (SysSchedule schedule : schedules) {
                SchedulingRunnable task = new SchedulingRunnable(schedule.getId(), schedule.getName(), schedule.getInvokeTarget());
                cronTaskRegistrar.addCronTask(task, schedule.getCronExpression());
            }
            log.info("定时任务已加载完毕...");
        }
    }
}
