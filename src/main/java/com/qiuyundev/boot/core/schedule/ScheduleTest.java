package com.qiuyundev.boot.core.schedule;

import org.springframework.stereotype.Component;

@Component("scheduleTest")
public class ScheduleTest {

    public void test1() {
        // 输出时间还有方法名
        System.out.println("test1: " + System.currentTimeMillis() + " " + Thread.currentThread().getStackTrace()[1].getMethodName());
    }
}
