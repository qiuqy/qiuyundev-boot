package com.qiuyundev.boot.core.email;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Map;

@Data
@EqualsAndHashCode(callSuper = true)
public class TemplateMail extends Mail {
    /**
     * 模板路径
     */
    private String template;

    /**
     * 模板变量
     */
    private Map<String, Object> variables;

}
