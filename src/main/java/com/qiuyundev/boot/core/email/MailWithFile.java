package com.qiuyundev.boot.core.email;


import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.File;

@Data
@EqualsAndHashCode(callSuper = true)
public class MailWithFile extends Mail{
   /**
    * 文件
    */
   private File file;
}
