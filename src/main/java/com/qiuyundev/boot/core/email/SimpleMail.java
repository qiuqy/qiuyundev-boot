package com.qiuyundev.boot.core.email;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SimpleMail {
    /**
     * 收件人邮箱
     */
    private String to;

    /**
     * 邮件主题
     */
    private String subject;

    /**
     * 邮件内容
     */
    private String content;
}
