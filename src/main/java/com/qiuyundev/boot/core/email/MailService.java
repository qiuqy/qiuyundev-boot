package com.qiuyundev.boot.core.email;

import com.qiuyundev.boot.base.vo.ApiResult;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Service
@RequiredArgsConstructor
public class MailService {
    private final JavaMailSender javaMailSender;
    private final TemplateEngine templateEngine;

    /**
     * 发送简单邮件
     *
     * @param mail 简单邮件
     * @return {@link ApiResult}
     */
    public ApiResult sendSimpleMailMessage(Mail mail) {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        if (!StringUtils.hasText(mail.getFrom())) {
            simpleMailMessage.setFrom("97208294@qq.com");
        }
        simpleMailMessage.setTo(mail.getTo());
        if (null != mail.getCc()) {
            simpleMailMessage.setCc(mail.getCc());
        }
        simpleMailMessage.setSubject(mail.getSubject());
        simpleMailMessage.setText(mail.getText());
        try {
            this.javaMailSender.send(simpleMailMessage);
        } catch (MailException mailException) {
            return ApiResult.failed(mailException.getMessage());
        }

        return ApiResult.success();
    }

    /**
     * 发送带有附件邮件
     *
     * @param mail 邮件
     * @return {@link ApiResult}
     */
    public ApiResult sendMailWithFile(MailWithFile mail) {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
            mimeMessageHelper.setFrom(mail.getFrom());
            mimeMessageHelper.setTo(mail.getTo());
            mimeMessageHelper.setCc(mail.getCc());
            mimeMessageHelper.setSubject(mail.getSubject());
            mimeMessageHelper.setText(mail.getText());

            //  添加附件
            mimeMessageHelper.addAttachment(mail.getFile().getName(), mail.getFile());
            this.javaMailSender.send(mimeMessage);
        } catch (MessagingException e) {
            return ApiResult.failed(e.getMessage());
        }

        return ApiResult.success();
    }

    /**
     * 发送模板邮件
     *
     * @param mail 邮件
     * @return {@link ApiResult}
     */
    public ApiResult sendTemplateMail(TemplateMail mail) {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
            mimeMessageHelper.setFrom(mail.getFrom());
            mimeMessageHelper.setTo(mail.getTo());
            mimeMessageHelper.setCc(mail.getCc());
            mimeMessageHelper.setSubject(mail.getSubject());
            mimeMessageHelper.setText(mail.getText());

            Context context = new Context();
            context.setVariables(mail.getVariables());
            String text = templateEngine.process(mail.getTemplate(), context);
            mimeMessageHelper.setText(text, true);
            this.javaMailSender.send(mimeMessage);
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }


        return ApiResult.success();
    }

}
