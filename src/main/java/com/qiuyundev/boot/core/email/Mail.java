package com.qiuyundev.boot.core.email;


import lombok.Data;

@Data
public class Mail {
    /**
     * 发送者邮箱
     */
    private String from;

    /**
     * 收件人邮箱
     */
    private String[] to;

    /**
     * 抄送人邮箱
     */
    private String[] cc;

    /**
     * 邮件主题
     */
    private String subject;

    /**
     * 邮件内容
     */
    private String text;

}
