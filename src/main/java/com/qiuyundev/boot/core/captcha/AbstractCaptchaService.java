package com.qiuyundev.boot.core.captcha;

import cn.hutool.core.lang.TypeReference;
import com.qiuyundev.boot.base.util.SpringUtils;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.concurrent.TimeUnit;

/**
 * 验证码服务抽象类
 *
 * @author Qiuyun
 * @since 2024/04/16
 */
public abstract class AbstractCaptchaService {
    private final RedisTemplate<String, String> redisTemplate;

    protected AbstractCaptchaService() {
        TypeReference<RedisTemplate<String, String>> redisTemplateTypeReference = new TypeReference<>() {
        };
        this.redisTemplate = SpringUtils.getBean(redisTemplateTypeReference);
    }

    /**
     * 验证码缓存前缀
     */
    public static final String CAPTCHA_CACHE_PREFIX_KEY = "CAPTCHA:";

    /**
     * 验证码 CODE参数名称
     */
    public static final String CAPTCHA_CODE_FIELD_NAME = "captchaCode";

    /**
     * 验证码 KEY 参数名称
     */
    public static final String CAPTCHA_KEY_FIELD_NAME = "captchaKey";

    /**
     * 验证码缓存时间（分钟）
     */
    public static Long CAPTCHA_CACHE_TIME = 5L;

    /**
     * 生成验证码并返回验证码标识 和 Base64 编码的图片
     *
     * @return 验证码
     */
    public abstract Captcha generateCaptcha(Integer width, Integer height);

    /**
     * 验证输入的验证码是否正确
     */
    public boolean verifyCaptcha(String captchaCode, String captchaKey) {
        String key = CAPTCHA_CACHE_PREFIX_KEY + captchaKey;
        String code = this.redisTemplate.opsForValue().get(key);
        this.redisTemplate.delete(key);
        return captchaCode.equalsIgnoreCase(code);
    }

    /**
     * 缓存验证码的 code
     *
     * @param code       验证码 code
     * @param captchaKey 验证码 key
     */
    public void cacheCode(String code, String captchaKey) {
        this.redisTemplate.opsForValue().set(CAPTCHA_CACHE_PREFIX_KEY + captchaKey, code, CAPTCHA_CACHE_TIME, TimeUnit.MINUTES);
    }


    @Getter
    @Setter
    @Builder
    public static class Captcha {
        /**
         * 验证码标识 - 缓存用的 key
         */
        private String captchaKey;

        /**
         * 验证码图片Base64字符串
         */
        private String captchaBase64;
    }


}
