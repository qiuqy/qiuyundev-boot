package com.qiuyundev.boot.core.captcha;

import cn.hutool.captcha.generator.CodeGenerator;
import cn.hutool.captcha.generator.MathGenerator;
import cn.hutool.core.math.Calculator;
import cn.hutool.core.util.CharUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import org.springframework.stereotype.Component;


@Component
public class HutoolMathCaptchaService extends HutoolCaptchaService {
    /**
     * 自定义四则运算生成器
     */
    private static final MathGenerator MATH_GENERATOR = new MathGenerator() {
        private static final int numberLength = 1;
        private static final String operators = "+-*";

        @Override
        public String generate() {
            final int limit = Integer.parseInt("1" + StrUtil.repeat('0', numberLength));
            String number1 = Integer.toString(RandomUtil.randomInt(limit));
            String number2 = Integer.toString(RandomUtil.randomInt(limit));
            number1 = StrUtil.padAfter(number1, numberLength, CharUtil.SPACE);
            number2 = StrUtil.padAfter(number2, numberLength, CharUtil.SPACE);
            // 互换位置，避免出现负数
            if (Integer.parseInt(number1) < Integer.parseInt(number2)) {
                String temp = number1;
                number1 = number2;
                number2 = temp;
            }
            return StrUtil.builder()
                    .append(number1)
                    .append(RandomUtil.randomChar(operators))
                    .append(number2)
                    .append('=').toString();
        }
    };

    @Override
    public CodeGenerator getGenerator() {
        return MATH_GENERATOR;
    }

    @Override
    public String getCacheCode(String code) {
        return String.valueOf(Calculator.conversion(code));
    }
}
