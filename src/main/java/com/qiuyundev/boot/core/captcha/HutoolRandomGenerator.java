package com.qiuyundev.boot.core.captcha;

import cn.hutool.captcha.generator.CodeGenerator;
import cn.hutool.captcha.generator.RandomGenerator;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Primary
@Component
public class HutoolRandomGenerator extends HutoolCaptchaService{
    @Override
    public CodeGenerator getGenerator() {
        return new RandomGenerator(4);
    }

    @Override
    public String getCacheCode(String code) {
        return code;
    }
}
