package com.qiuyundev.boot.core.captcha;

import cn.hutool.captcha.AbstractCaptcha;
import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.generator.CodeGenerator;
import cn.hutool.captcha.generator.RandomGenerator;
import cn.hutool.core.util.IdUtil;

import java.awt.*;

/**
 * 验证码实现类（使用 hutool 生成验证码）
 *
 * @author Qiuyun
 * @since 2024/04/12
 */
public abstract class HutoolCaptchaService extends AbstractCaptchaService {
    /**
     * 验证码-宽度
     */
    public static final int WIDTH = 150;
    /**
     * 验证码-高度
     */
    public static final int HEIGHT = 50;

    /**
     * 验证码-字符个数
     */
    public static final int CODE_COUNT = 3;

    /**
     * 验证码-干扰圆圈条数
     */
    public static final int CIRCLE_COUNT = 9;

    /**
     * 随机字符验证码生成器
     */
    private static final RandomGenerator randomGenerator = new RandomGenerator(4);

    /**
     * 获取验证码文字生成器
     */
    public abstract CodeGenerator getGenerator();

    /**
     * 获取验证码对应的缓存字符
     */
    public abstract String getCacheCode(String code);

    @Override
    public Captcha generateCaptcha(Integer width, Integer height) {
        CodeGenerator generator = getGenerator();
        // 验证码标识
        String captchaKey = IdUtil.simpleUUID();
        // 生成验证码
        AbstractCaptcha captcha = CaptchaUtil.createCircleCaptcha(
                null != width ? width : WIDTH,
                null != height ? height : HEIGHT,
                CODE_COUNT,
                CIRCLE_COUNT);
        // 自定义验证码内容为四则运算方式
        captcha.setGenerator(generator);
        captcha.setBackground(Color.WHITE);
        // 生成 code
        captcha.createCode();

        // 缓存验证码 CODE
        super.cacheCode(getCacheCode(captcha.getCode()), captchaKey);
        return Captcha.builder()
                .captchaKey(captchaKey)
                .captchaBase64(captcha.getImageBase64Data())
                .build();
    }



}
