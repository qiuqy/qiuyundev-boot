package com.qiuyundev.boot.core.file.storage;

import com.qiuyundev.boot.base.exception.BusinessException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class LocalStorageStrategy implements StorageStrategy {

    @Value("${upload.path:}")
    private String uploadPath;

    @Override
    public String store(MultipartFile file) {
        try {
            String originalFilename = file.getOriginalFilename();
            String filePath = uploadPath + File.separator + originalFilename;
            File destinationFile = new File(filePath);
            file.transferTo(destinationFile);
            return originalFilename;
        } catch (IOException e) {
            throw new BusinessException("Could not store file " + file.getOriginalFilename(), e);
        }
    }

    @Override
    public Resource loadAsResource(String fileName) {
        try {
            Path path = Paths.get(uploadPath).resolve(fileName).normalize();
            Resource resource = new UrlResource(path.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new BusinessException("Could not read file: " + fileName);
            }
        } catch (MalformedURLException e) {
            throw new BusinessException("Could not read file: " + fileName, e);
        }
    }

    @Override
    public void delete(String fileName) {
        Path path = Paths.get(uploadPath).resolve(fileName).normalize();
        try {
            Files.delete(path);
        } catch (IOException e) {
            throw new BusinessException("Could not delete file: " + fileName, e);
        }
    }
}
