package com.qiuyundev.boot.core.file;

import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.multipart.MultipartFile;

public interface FileService {

    /**
     * 上传文件
     * @param file 上传的文件
     * @return 上传后的文件路径（或唯一标识符）
     */
    String uploadFile(MultipartFile file);

    /**
     * 下载文件
     * @param path 文件路径
     * @param response HttpServletResponse对象，用于写入文件数据
     */
    void downloadFile(String path, HttpServletResponse response);

    /**
     * 预览文件（这里假设为图片预览，可以通过HTTP GET请求返回图片）
     * @param path 文件路径
     * @param contentType 内容类型
     * @param response HttpServletResponse对象，用于写入图片数据
     */
    void previewFile(String path, String contentType, HttpServletResponse response);

    /**
     * 删除文件
     * @param path 文件路径
     * @return 是否删除成功
     */
    boolean deleteFile(String path);

    // 可能还需要其他与文件操作相关的接口...
}
