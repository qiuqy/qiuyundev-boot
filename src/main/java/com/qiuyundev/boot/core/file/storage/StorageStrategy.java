package com.qiuyundev.boot.core.file.storage;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface StorageStrategy {
    String store(MultipartFile file);

    Resource loadAsResource(String fileName);

    void delete(String fileName);
}
