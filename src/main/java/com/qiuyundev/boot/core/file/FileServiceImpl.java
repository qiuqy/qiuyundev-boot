package com.qiuyundev.boot.core.file;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import com.qiuyundev.boot.domain.enums.file.FileCategoryEnum;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;

@Slf4j
@Service
public class FileServiceImpl implements FileService {

    // 假设你在 application.properties 或 application.yml 中配置了文件上传目录
    @Value("${file.upload.dir}")
    private String uploadDir;

    /**
     * 上传文件
     *
     * @param file 文件
     * @return 文件相对路径
     */
    @Override
    public String uploadFile(MultipartFile file) {
        // 后缀名称
        String extension = Objects.requireNonNull(file.getOriginalFilename())
                .substring(file.getOriginalFilename().lastIndexOf("."));
        // 每一个月的文件存一个文件夹
        String path =  "/"
                + getMonthFolderName(uploadDir)
                + "/"
                + IdUtil.getSnowflake().nextIdStr()
                +  extension;
        String filePath = uploadDir + path;
        File dest = new File(filePath);
        try {
            file.transferTo(dest);
            // 返回文件路径
            return path;
        } catch (IOException e) {
            log.error("上传文件失败：{}", e.getMessage());
            return null;
        }
    }

    /**
     * 获取月份文件夹名称
     * <p>
     *  按照月份归档存文件
     * @param uploadDir 上传目录
     * @return {@link String}
     */
    private synchronized String getMonthFolderName(String uploadDir) {
        String monthFormat = DateUtil.format(new Date(), "yyyyMM");
        // 获取文件夹是否存在
        File monthFolder = new File(uploadDir + File.separator + monthFormat);
        // 判断文件夹是否存在
        if (!monthFolder.exists()) {
            try {
                monthFolder.mkdirs();
            } catch (Exception e) {
                log.error("创建文件夹失败：{}", e.getMessage());
            }
        }

        return monthFormat;
    }

    @Override
    public void downloadFile(String path, HttpServletResponse response) {
        String filePath = uploadDir +  path;
        File file = new File(filePath);
        if (file.exists()) {
            try (FileInputStream inputStream = new FileInputStream(file)) {
                response.setContentType("application/octet-stream");
                response.setHeader("Content-Disposition", "attachment;filename=\"" + file.getName() + "\"");
                FileCopyUtils.copy(inputStream, response.getOutputStream());
            } catch (IOException e) {
                log.error("文件下载失败：{}", path);
            }
        }
    }

    @Override
    public void previewFile(String path, String contentType, HttpServletResponse response) {
        // 假设只预览图片，可以添加其他类型的检查
        boolean isImage = Arrays.stream(FileCategoryEnum.IMAGE.getExtensions()).anyMatch(path::endsWith);
        if (isImage) {
            String filePath = uploadDir +  path;
            File file = new File(filePath);
            if (file.exists()) {
                try (FileInputStream inputStream = new FileInputStream(file)) {
                    response.setContentType(contentType); // 或image/png，根据文件类型
                    FileCopyUtils.copy(inputStream, response.getOutputStream());
                } catch (IOException e) {
                    log.error("文件预览失败：{}", path);
                }
            }
        }
    }

    @Override
    public boolean deleteFile(String path) {
        String filePath = uploadDir +  path;
        File file = new File(filePath);
        if (file.exists()) {
            return file.delete();
        }
        log.error("文件删除失败：{}", path);
        return false;
    }


}
