package com.qiuyundev.boot.core.datascope;

import cn.hutool.core.annotation.AnnotationUtil;
import cn.hutool.core.collection.ConcurrentHashSet;
import cn.hutool.core.util.ClassUtil;
import com.baomidou.mybatisplus.extension.plugins.handler.DataPermissionHandler;
import com.qiuyundev.boot.base.exception.BusinessException;
import com.qiuyundev.boot.base.util.SpringUtils;
import com.qiuyundev.boot.core.security.LoginUser;
import com.qiuyundev.boot.core.security.SecurityUtils;
import lombok.SneakyThrows;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.Parenthesis;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import org.springframework.context.expression.BeanFactoryResolver;
import org.springframework.expression.BeanResolver;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.ParserContext;
import org.springframework.expression.common.TemplateParserContext;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.util.StringUtils;

import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * CustomDataPermissionHandler
 *
 * @author Qiuyun
 * @since 2024/04/12
 */
public class MyBatisPlusDataPermissionHandler implements DataPermissionHandler {

    /**
     * 方法或类(名称)与注解的映射关系缓存
     */
    private final Map<String, DataScope> dataPermissionCacheMap = new ConcurrentHashMap<>();

    /**
     * 无效注解方法缓存用于快速返回
     */
    private final Set<String> invalidCacheSet = new ConcurrentHashSet<>();


    private final ExpressionParser parser = new SpelExpressionParser();
    private final ParserContext parserContext = new TemplateParserContext();
    private final BeanResolver beanResolver = new BeanFactoryResolver(SpringUtils.getBeanFactory());


    @Override
    public Expression getSqlSegment(Expression where, String mappedStatementId) {
        return where;
    }

    @SneakyThrows
    public Expression getSqlSegment(Expression where, String mappedStatementId, boolean isSelect) {
        DataColumn[] dataColumns = this.findDataColumns(mappedStatementId);
        if (null == dataColumns || dataColumns.length == 0) {
            invalidCacheSet.add(mappedStatementId);
            return where;
        }

        String dataScopeSql = buildDataPermissionSql(dataColumns, isSelect);
        if (StringUtils.hasText(dataScopeSql)) {
            Expression expression = CCJSqlParserUtil.parseExpression(dataScopeSql);
            // 数据权限使用单独的括号 防止与其他条件冲突
            Parenthesis parenthesis = new Parenthesis(expression);
            if (null == where) {
                return parenthesis;
            }
            return new AndExpression(where, parenthesis);
        }
        return where;
    }

    private String buildDataPermissionSql(DataColumn[] dataColumns, boolean isSelect) {
        StandardEvaluationContext context = new StandardEvaluationContext();
        context.setBeanResolver(beanResolver);

        String joinStr = isSelect ? " OR " : " AND ";
        LoginUser loginUser = SecurityUtils.getUserInfo();
        DataScopeEnum.DataScopeTemplateParam param = new DataScopeEnum.DataScopeTemplateParam();
        param.setUsername(loginUser.getUser().getUsername());
        param.setDeptId(loginUser.getUser().getDeptId());

        Collection<String> conditions = new ArrayList<>();
        for (LoginUser.Role role : loginUser.getRoles()) {
            param.setRoleId(role.getId());
            DataScopeEnum typeEnum = role.getDataScope();
            for (DataColumn dataColumn : dataColumns) {
                if (!StringUtils.hasText(dataColumn.value())) {
                    throw new BusinessException("角色数据范围异常");
                }
                param.setColumnName(dataColumn.value());
                context.setVariable(DataScopeEnum.DataScopeTemplateParam.PARAM_NAME, param);
                // 解析 SQL 模板
                String sql = parser.parseExpression(typeEnum.getSqlTemplate(), parserContext).getValue(context, String.class);
                conditions.add(joinStr + sql);
            }
        }

        return String.join("", conditions).substring(joinStr.length());
    }


    private DataColumn[] findDataColumns(String mappedStatementId) {
        int index = mappedStatementId.lastIndexOf(".");
        String clazzName = mappedStatementId.substring(0, index);
        String methodName = mappedStatementId.substring(index + 1);
        Class<?> clazz = ClassUtil.loadClass(clazzName);
        List<Method> methods = Arrays.stream(ClassUtil.getDeclaredMethods(clazz))
                .filter(method -> method.getName().equals(methodName)).toList();
        DataScope dataScope;
        // 获取方法注解
        for (Method method : methods) {
            dataScope = dataPermissionCacheMap.get(mappedStatementId);
            if (null != dataScope) {
                return dataScope.value();
            }
            if (AnnotationUtil.hasAnnotation(method, DataScope.class)) {
                dataScope = AnnotationUtil.getAnnotation(method, DataScope.class);
                dataPermissionCacheMap.put(mappedStatementId, dataScope);
                return dataScope.value();
            }
        }

        // 获取类注解
        dataScope = dataPermissionCacheMap.get(clazz.getName());
        if (null != dataScope) {
            return dataScope.value();
        }
        if (AnnotationUtil.hasAnnotation(clazz, DataScope.class)) {
            dataScope = AnnotationUtil.getAnnotation(clazz, DataScope.class);
            dataPermissionCacheMap.put(clazz.getName(), dataScope);
            return dataScope.value();
        }
        return null;
    }

    public boolean isInvalid(String mappedStatementId) {
        return invalidCacheSet.contains(mappedStatementId);
    }
}
