package com.qiuyundev.boot.core.datascope;

import com.qiuyundev.boot.base.enums.serialize.IEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * 数据权限范围枚举
 *
 * @author Qiuyun
 * @since 2024/04/23
 */
@Getter
@AllArgsConstructor
public enum DataScopeEnum implements IEnum {
    /**
     * 全部数据权限
     */
    ALL("全部数据权限", ""),

    /**
     * 自定义部门数据权限
     */
    CUSTOM("自定义部门数据权限", " #{#param.columnName} IN ( #{@dps.getRoleDeptIds( #param.roleId )} ) "),

    /**
     * 本人部门数据权限
     */
    SELF_DEPT( "本人部门数据权限", " #{#param.columnName} = #{#param.deptId} "),

    /**
     * 本人部门及下级部门数据权限
     */
    SELF_DEPT_AND_CHILDREN( "本人部门及下级部门数据权限", " #{#param.columnName} IN ( #{@dps.getDeptAndChildrenIds( #param.deptId )} ) "),

    /**
     * 仅本人数据权限
     */
    SELF("仅本人数据权限", " #{#param.columnName} = '#{#param.username}' ");


    private final String text;

    /**
     * SQL 模板，使用 SpEL 表达式
     */
    private final String sqlTemplate;



    /**
     * 用户数据权限信息
     *
     * @author Qiuyun
     * @since 2024/04/12
     */
    @Getter
    @Setter
    @Accessors(chain = true)
    public static class DataScopeTemplateParam {

        public static final String PARAM_NAME = "param";
        /**
         * 用户 ID
         */
        private String username;

        /**
         * 部门 ID
         */
        private Integer deptId;

        /**
         * 角色 ID
         */
        private Integer roleId;

        /**
         * 列名
         */
        private String columnName;
    }
}
