package com.qiuyundev.boot.core.datascope;

import com.baomidou.mybatisplus.extension.plugins.handler.DataPermissionHandler;
import com.baomidou.mybatisplus.extension.plugins.inner.DataPermissionInterceptor;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.select.PlainSelect;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;

import java.sql.SQLException;

/**
 * MyBatisPlusDataPermissionInterceptor
 *
 * @author Qiuyun
 * @since 2024/04/12
 */
public class MyBatisPlusDataPermissionInterceptor extends DataPermissionInterceptor {
    private MyBatisPlusDataPermissionHandler myBatisPlusDataPermissionHandler;

    @Override
    public void setDataPermissionHandler(DataPermissionHandler dataPermissionHandler) {
        super.setDataPermissionHandler(dataPermissionHandler);
        if (dataPermissionHandler instanceof MyBatisPlusDataPermissionHandler) {
            this.myBatisPlusDataPermissionHandler = (MyBatisPlusDataPermissionHandler) dataPermissionHandler;
        }
    }

    @Override
    public void beforeQuery(Executor executor, MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler resultHandler, BoundSql boundSql) throws SQLException {
        // 无数据权限注解直接返回
        if (this.myBatisPlusDataPermissionHandler.isInvalid(ms.getId())) {
            return;
        }
        super.beforeQuery(executor, ms, parameter, rowBounds, resultHandler, boundSql);
    }

    @Override
    public void setWhere(PlainSelect plainSelect, String whereSegment) {
        if (null != this.myBatisPlusDataPermissionHandler) {
            final Expression sqlSegment = myBatisPlusDataPermissionHandler.getSqlSegment(plainSelect.getWhere(), whereSegment, true);
            if (null != sqlSegment) {
                plainSelect.setWhere(sqlSegment);
            }
        }
    }

    @Override
    public Expression getUpdateOrDeleteExpression(final Table table, final Expression where, final String whereSegment) {
        if (null != myBatisPlusDataPermissionHandler) {
            return this.myBatisPlusDataPermissionHandler.getSqlSegment(where, whereSegment, false);
        }
        return where;
    }

}
