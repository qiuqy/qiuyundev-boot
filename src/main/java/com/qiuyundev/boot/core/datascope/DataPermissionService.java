package com.qiuyundev.boot.core.datascope;

/**
 * DataPermissionService
 *
 * @author Qiuyun
 * @since 2024/04/12
 */
public interface DataPermissionService {
    String beanName = "dps";

    String getRoleDeptIds(String roleId);

    String getDeptAndChildrenIds(String deptId);
}
