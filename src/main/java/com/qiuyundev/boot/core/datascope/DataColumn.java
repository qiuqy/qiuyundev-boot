package com.qiuyundev.boot.core.datascope;


import com.qiuyundev.boot.base.enums.serialize.IEnumSerializer;

import java.lang.annotation.*;

/**
 * 数据权限字段
 *
 * @author Qiuyun
 * @since 2024/04/12
 */
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
@Repeatable(DataScope.class)
public @interface DataColumn {

    /**
     * 字段名
     * <p>
     * 1. 如果有表的别名前缀，则需要加上： 如 t.id， 无则直接使用字段名：如： id
     * 2. 该值会替换 @code {@link DataScopeEnum#getSqlTemplate()} 中的 #{#columnName}
     *
     * @see DataScopeEnum
     */
    String value() default "";
}
