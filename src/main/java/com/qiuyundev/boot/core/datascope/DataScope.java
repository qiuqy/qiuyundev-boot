package com.qiuyundev.boot.core.datascope;


import java.lang.annotation.*;

/**
 * 数据权限注解
 *
 * @author Qiuyun
 * @since 2024/04/12
 */
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface DataScope {
    DataColumn[] value() default {};
}
