package com.qiuyundev.boot.core.sse;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SseMessage<T> {
    private T data;
    private SseMessageTypeEnum type;

    public enum SseMessageTypeEnum {
        message,
        ping,
        close,
        updatePermsAndMenus,
        updateOnlineUserCount
    }
}
