package com.qiuyundev.boot.core.sse;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

@RestController
@RequestMapping("sse")
@RequiredArgsConstructor
public class SseController {
    private final SseService sseService;

    @GetMapping(value = "/{uid}")
    public SseEmitter connect(@PathVariable("uid") String uid) {
        return sseService.createSse(uid);
    }

    @GetMapping("/sendMessage")
    public boolean sendMessage(@RequestParam("uid") String uid,@RequestParam("message")  String message) {
        SseMessage<String> stringSseMessage = new SseMessage<>();
        stringSseMessage.setType(SseMessage.SseMessageTypeEnum.message);
        stringSseMessage.setData(message);
        return this.sseService.sendMessage(uid, stringSseMessage);
    }

    /**
     * 关闭连接
     */
    @GetMapping("/closeSse")
    public void closeConnect(String uid ){
        sseService.closeSse(uid);
    }

}
