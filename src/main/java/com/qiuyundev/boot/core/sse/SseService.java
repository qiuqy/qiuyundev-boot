package com.qiuyundev.boot.core.sse;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Component
@RequiredArgsConstructor
public class SseService {
    private static final Map<String, SseEmitter> sseEmitterMap = new ConcurrentHashMap<>();

    /**
     * 创建连接
     */
    public SseEmitter createSse(String uid) {
        // 默认30秒超时, 设置为0L则永不超时
        SseEmitter sseEmitter = new SseEmitter(0L);

        // 连接断开
        sseEmitter.onCompletion(() -> {
            sseEmitterMap.remove(uid);
            log.info("SSE: [{}]结束连接", uid);
        });
        //超时回调
        sseEmitter.onTimeout(() -> {
            sseEmitterMap.remove(uid);
            log.info("SSE: [{}]连接超时", uid);
        });
        //异常回调
        sseEmitter.onError(throwable -> {
            sseEmitterMap.remove(uid);
            log.error("SSE: [{}]连接异常,{}", uid, throwable.toString());
        });

        sseEmitterMap.put(uid, sseEmitter);
        log.info("SSE: [{}]创建sse连接成功！", uid);
        // 连接成功返回提示
        try {
            sseEmitter.send(SseEmitter.event().comment("welcome"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return sseEmitter;
    }

    /**
     * 广播消息
     */
    public <T> void sendMessage(SseMessage<T> message) {
        sseEmitterMap.forEach((uid, sseEmitter) -> {
            try {
                sseEmitter.send(message, MediaType.APPLICATION_JSON);
            } catch (IOException e) {
                log.error("SSE: [{}]广播消息发送消息失败,{}", uid, e.toString());
                sseEmitterMap.remove(uid);
            }
        });
    }

    /**
     * 给指定用户发送消息
     */
    public <T> boolean sendMessage(String uid, SseMessage<T> message) {
        SseEmitter sseEmitter = sseEmitterMap.get(uid);
        if (sseEmitter == null) {
            return false;
        }
        try {
            sseEmitter.send(message, MediaType.APPLICATION_JSON);
        } catch (IOException e) {
            log.error("SSE: [{}]给指定用户发送消息失败,{}", uid, e.toString());
        }

        return true;
    }

    /**
     * 断开连接
     *
     * @param uid 用户ID
     */
    public void closeSse(String uid) {
        if (sseEmitterMap.containsKey(uid)) {
            SseEmitter sseEmitter = sseEmitterMap.get(uid);
            sseEmitter.complete();
            sseEmitterMap.remove(uid);
        } else {
            log.info("用户[{}]连接已关闭", uid);
        }
    }



    /**
     * sse 发送指定类型
     */
    @Async
    public void sendByType(SseMessage.SseMessageTypeEnum type) {
        SseMessage<Void> sseMessage = new SseMessage<>();
        sseMessage.setType(type);
        this.sendMessage(sseMessage);
    }

}
