package com.qiuyundev.boot.core.db.provider;

public interface SqlProviderStrategy {
    String getQueryTablesSql(String tableSchema, String tableName);

    String getQueryTableColumnsSql(String tableSchema, String tableName);
}
