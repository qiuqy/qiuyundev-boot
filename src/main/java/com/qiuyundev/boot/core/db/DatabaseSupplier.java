package com.qiuyundev.boot.core.db;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;

@Component
@RequiredArgsConstructor
@ConditionalOnClass(DataSource.class)
public class DatabaseSupplier {
    private final DataSource dataSource;

    public String getDatabaseType() {
        try (Connection connection = dataSource.getConnection()) {
            DatabaseMetaData metaData = connection.getMetaData();
            return metaData.getDatabaseProductName();
        } catch (SQLException e) {
            // 处理异常
            e.printStackTrace();
            return "Unknown";
        }
    }

    public String getDatabaseName() {
        try (Connection connection = dataSource.getConnection()) {
            // 注意：这通常不是一个标准的方法，因为DatabaseMetaData没有直接提供获取数据库名称的API
            // 你可能需要解析连接URL或者执行特定的SQL查询
            String url = connection.getMetaData().getURL();
            // 这里只是一个示例，你可能需要根据具体的数据库和连接URL格式来解析数据库名称
            int indexOfDB = url.lastIndexOf('/'); // 假设URL格式为jdbc:mysql://localhost:3306/mydb, 实际情况是mydb?useUnicode=true&characterEncoding=utf8&useSSL=false&serverTimezone=UTC
            if (indexOfDB != -1) {
                String database = url.substring(indexOfDB + 1);
                if (database.contains("?")) {
                    database = database.substring(0, database.indexOf("?"));
                }
                return database;
            }
            // 或者，你可以执行一个SQL查询来获取数据库名称（这取决于数据库）
            // 例如，对于MySQL，你可以执行SELECT DATABASE();
        } catch (SQLException e) {
            // 处理异常
            e.printStackTrace();
            return "Unknown";
        }
        return "Unknown";
    }
}
