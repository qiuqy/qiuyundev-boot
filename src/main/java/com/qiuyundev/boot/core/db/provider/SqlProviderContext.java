package com.qiuyundev.boot.core.db.provider;

public class SqlProviderContext {

    public String getQueryTablesSql(String tableSchema, String tableName) {
        DatabaseEnum databaseEnum = DatabaseEnum.getByDefaultDatasource();
        return databaseEnum.getQueryTablesSql(tableSchema, tableName);
    }

    public String getQueryTableColumnsSql(String tableSchema, String tableName) {
        DatabaseEnum databaseEnum = DatabaseEnum.getByDefaultDatasource();
        return databaseEnum.getQueryTableColumnsSql(tableSchema, tableName);
    }
}
