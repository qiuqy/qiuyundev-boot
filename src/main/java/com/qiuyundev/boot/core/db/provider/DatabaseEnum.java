package com.qiuyundev.boot.core.db.provider;

import com.qiuyundev.boot.base.exception.BusinessException;
import com.qiuyundev.boot.base.util.SpringUtils;
import com.qiuyundev.boot.core.db.DatabaseSupplier;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum DatabaseEnum {
    MySql(new MySqlProviderStrategy()),
    ;

    private final SqlProviderStrategy sqlProviderStrategy;


    public static DatabaseEnum getByName(String name) {
        return Arrays.stream(DatabaseEnum.values())
                .findFirst()
                .orElse(null);
    }

    public static DatabaseEnum getByDefaultDatasource() {
        DatabaseSupplier databaseSupplier = SpringUtils.getBean(DatabaseSupplier.class);
        if (null != databaseSupplier) {
            DatabaseEnum databaseEnum = DatabaseEnum.getByName(databaseSupplier.getDatabaseType());
            if (null != databaseEnum) {
               return databaseEnum;
            }
        }
        throw new BusinessException("未找到数据源Bean");
    }

    public String getQueryTablesSql(String tableSchema, String tableName) {
        return this.getSqlProviderStrategy().getQueryTablesSql(tableSchema, tableName);
    }

    public String getQueryTableColumnsSql(String tableSchema, String tableName) {
        return this.getSqlProviderStrategy().getQueryTableColumnsSql(tableSchema, tableName);
    }
}
