package com.qiuyundev.boot.core.db.provider;

import org.apache.ibatis.jdbc.SQL;
import org.springframework.util.StringUtils;

public class MySqlProviderStrategy implements SqlProviderStrategy {
    @Override
    public String getQueryTablesSql(String tableSchema, String tableName) {
        SQL sql = new SQL().SELECT("""
                        TABLE_SCHEMA as tableSchema,
                        TABLE_NAME as tableName,
                        TABLE_COMMENT as tableComment,
                        CREATE_TIME as createTime,
                        UPDATE_TIME as updateTime""")
                .FROM("information_schema.TABLES");
        if (StringUtils.hasText(tableSchema)) {
            sql.WHERE("TABLE_SCHEMA = '" + tableSchema + "'");
        }
        if (StringUtils.hasText(tableName)) {
            sql.WHERE("TABLE_NAME LIKE CONCAT('%', '" + tableName + "', '%')");
        }
        sql.ORDER_BY("CREATE_TIME DESC");
        return sql.toString();
    }

    @Override
    public String getQueryTableColumnsSql(String tableSchema, String tableName) {
        SQL sql = new SQL().SELECT("""
                        TABLE_SCHEMA as tableSchema,
                               TABLE_NAME   as tableName,
                               COLUMN_NAME as columnName,
                               COLUMN_TYPE as columnType,
                               COLUMN_COMMENT as columnComment,
                               IS_NULLABLE as isNullable,
                               COLUMN_DEFAULT as columnDefault,
                               ORDINAL_POSITION as ordinalPosition""")
                .FROM("information_schema.COLUMNS");
        if (StringUtils.hasText(tableSchema)) {
            sql.WHERE("TABLE_SCHEMA = '" + tableSchema + "'");
        }
        if (StringUtils.hasText(tableName)) {
            sql.WHERE("TABLE_NAME = '" + tableName + "'");
        }
        sql.ORDER_BY("ORDINAL_POSITION ASC");
        return sql.toString();
    }
}
