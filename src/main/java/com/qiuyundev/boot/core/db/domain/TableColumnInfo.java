package com.qiuyundev.boot.core.db.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TableColumnInfo {
    /**
     * 数据库名称
     */
    private String tableSchema;

    /**
     * 表名称
     */
    private String tableName;


    /**
     * 字段名称
     */
    private String columnName;

    /**
     * 字段类型
     */
    private String columnType;

    /**
     * 字段注释
     */
    private String columnComment;

    /**
     * 是否为null
     */
    private String isNullable;

    /**
     * 字段默认值
     */
    private String columnDefault;

    /**
     * 排序
     */
    private String ordinalPosition;
}
