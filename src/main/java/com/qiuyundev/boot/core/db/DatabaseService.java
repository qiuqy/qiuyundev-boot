package com.qiuyundev.boot.core.db;


import com.qiuyundev.boot.core.db.domain.TableColumnInfo;
import com.qiuyundev.boot.core.db.domain.TableInfo;
import com.qiuyundev.boot.mapper.DatabaseMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DatabaseService {
    private final DatabaseMapper databaseMapper;

    /**
     * 查询数据库有哪些表
     *
     * @return 数据库所有表
     */
    public List<TableInfo> selectTables(String tableSchema, String tableName) {
        return this.databaseMapper.selectTables(tableSchema, tableName);
    }

    /**
     * 查询表字段信息
     *
     * @return 数据库所有表
     */
    public List<TableColumnInfo> selectTableColumns(String tableSchema, String tableName) {
        return this.databaseMapper.selectTableColumns(tableSchema, tableName);
    }

}
