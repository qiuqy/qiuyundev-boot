package com.qiuyundev.boot.core.generator;

import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.generator.config.ConstVal;
import com.baomidou.mybatisplus.generator.config.builder.ConfigBuilder;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.qiuyundev.boot.base.exception.BusinessException;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import jakarta.validation.constraints.NotNull;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class EnhanceFreemarkerTemplateEngine extends FreemarkerTemplateEngine {
    /**
     * 是否写入文件
     */
    private final boolean isPreview;
    private Configuration configuration;

    EnhanceFreemarkerTemplateEngine(boolean isPreview) {
        this.isPreview = isPreview;
    }


    @Override
    public @NotNull EnhanceFreemarkerTemplateEngine init(@NotNull ConfigBuilder configBuilder) {
        configuration = new Configuration(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);
        configuration.setDefaultEncoding(ConstVal.UTF8);
        configuration.setClassForTemplateLoading(FreemarkerTemplateEngine.class, StringPool.SLASH);
        return this;
    }

    @Override
    public void writer(@NotNull Map<String, Object> objectMap, @NotNull String templatePath, @NotNull File outputFile) throws Exception {
        Template template = configuration.getTemplate(templatePath);
        // 写入文件的情况
        if (!isPreview) {
            try (FileOutputStream fileOutputStream = new FileOutputStream(outputFile)) {
                template.process(objectMap, new OutputStreamWriter(fileOutputStream, ConstVal.UTF8));
            }
            log.debug("模板:{};  文件:{}", templatePath, outputFile);
        } else {
            // 不写入文件
            StringWriter writer = new StringWriter();
            try {
                template.process(objectMap, writer);
                String output = writer.toString();
                CodeGeneratorContextHolder.setContext(templatePath, output);
            } catch (TemplateException | IOException e) {
                log.error("将模板转化成为文件异常: {}", e.getMessage());
                throw new BusinessException("将模板转化成为文件异常" + e.getMessage());
            }
        }

    }


    public static class CodeGeneratorContextHolder {
        private static final ThreadLocal<Map<String, String>> contextHolder = ThreadLocal.withInitial(HashMap::new);

        public static void clearContext() {
            contextHolder.remove();
        }

        public static Map<String, String> getContext() {
            return contextHolder.get();
        }

        public static void setContext(String key, String value) {
            contextHolder.get().put(key, value);
        }
    }
}
