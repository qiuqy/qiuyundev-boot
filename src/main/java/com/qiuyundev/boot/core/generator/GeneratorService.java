package com.qiuyundev.boot.core.generator;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.builder.CustomFile;
import com.baomidou.mybatisplus.generator.config.po.TableField;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.DbColumnType;
import com.qiuyundev.boot.core.mybatisplus.BaseBriefEntity;
import com.qiuyundev.boot.domain.entity.SysGeneratorConfig;
import com.qiuyundev.boot.service.ISysGeneratorConfigService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.sql.Types;
import java.util.*;

/**
 * 代码生成器服务
 *
 * @author Qiuyun
 * @since 2024/04/30
 */
@Service
@RequiredArgsConstructor
public class GeneratorService {
    private final DataSourceProperties dataSourceProperties;
    private final ISysGeneratorConfigService generatorConfigService;

    public void codeGenerator(boolean isPreview, String tableName) {
        // 当前项目路径
        String projectPath = System.getProperty("user.dir"); // = "d:";
        // 设置父包名
        String parentPackage = "com.qiuyundev.boot";
        // 模块名称
        String moduleName = "";
        // 名称
        String name = tableName.split("_")[1];

        FastAutoGenerator.create(dataSourceProperties.getUrl(),
                        dataSourceProperties.getUsername(),
                        dataSourceProperties.getPassword())
                .globalConfig(builder -> {
                    // 设置作者
                    builder.author("Qiuyun")
//                            .enableSpringdoc()
                            // 指定输出目录
                            .outputDir(projectPath + "/src/main/java")
                            .dateType(DateType.TIME_PACK)
                            .disableOpenDir();
                })
                .dataSourceConfig(builder -> builder.typeConvertHandler((globalConfig, typeRegistry, metaInfo) -> {
                    int typeCode = metaInfo.getJdbcType().TYPE_CODE;
                    if (typeCode == Types.SMALLINT) {
                        // 自定义类型转换
                        return DbColumnType.INTEGER;
                    }
                    return typeRegistry.getColumnType(metaInfo);

                }))
                .packageConfig(builder -> {
                    builder.parent(parentPackage)
                            // 设置父包模块名
                            .moduleName(moduleName)
                            .entity("domain.entity")
                            .pathInfo(Collections.singletonMap(OutputFile.xml, projectPath + "/src/main/resources/mapper"));
                })
                .strategyConfig(builder -> {
                    builder
                            .addInclude(tableName) // 设置需要生成的表名
//                            .addTablePrefix("sys_", "t_", "c_") // 设置过滤表前缀.entityBuilder()

                            .entityBuilder()
                            .enableFileOverride()
                            .enableLombok()
                            .superClass(BaseBriefEntity.class)
                            .addSuperEntityColumns("id", "create_by", "create_time", "update_by", "update_time", "is_deleted")
                            .idType(IdType.AUTO)
                            .javaTemplate("/templates/ftl/entity.java")

                            .mapperBuilder()
                            .enableFileOverride()
                            .mapperTemplate("/templates/ftl/mapper.java")
                            .mapperXmlTemplate("/templates/ftl/mapper.xml")

                            .serviceBuilder()
                            .enableFileOverride()
                            .formatServiceFileName("I%sService")
                            .serviceTemplate("/templates/ftl/service.java")
                            .serviceImplTemplate("/templates/ftl/serviceImpl.java")

                            .controllerBuilder()
                            .enableFileOverride()
                            .enableRestStyle()
                            .template("/templates/ftl/controller.java");

                })
                .injectionConfig(consumer -> {
                    consumer.beforeOutputFile((tableInfo, objectMap) -> {
                        String entity = objectMap.get("entity").toString();
                        //.replace("Sys", "");
                        objectMap.put("packageVo", ((Map<String, String>) objectMap.get("package")).get("Parent")
                                + ".domain.vo."
                                + name);
                        objectMap.put("formReqName", entity + "FormReq");
                        objectMap.put("pageReqName", entity + "PageReq");
                        objectMap.put("respName", entity + "Resp");
                        objectMap.put("parentPackage", parentPackage);

                        // vo字段处理
                        // 1. 默认的处理
                        TableInfo tableInfoTemp = (TableInfo) objectMap.get("table");
                        List<TableField> allField = new ArrayList<>();
                        allField.addAll(tableInfoTemp.getFields());
                        allField.addAll(tableInfoTemp.getCommonFields());
                        objectMap.put("formVoFields", allField);
                        objectMap.put("respVoFields", allField);
                        objectMap.put("pageVoFields", allField);
                        // 2. 有代码生成配置时候的处理
                        this.generatorConfigService.lambdaQuery()
                                .eq(SysGeneratorConfig::getTableName, tableName)
                                .oneOpt().ifPresent(config -> {
                                    objectMap.put("formVoFields", allField.stream().filter(e -> !ObjectUtils.isEmpty(config.getFormColumn()) && config.getFormColumn().contains(e.getName())).toList());
                                    objectMap.put("respVoFields", allField.stream().filter(e -> !ObjectUtils.isEmpty(config.getRespColumn()) && config.getRespColumn().contains(e.getName())).toList());
                                    objectMap.put("pageVoFields", allField.stream().filter(e -> !ObjectUtils.isEmpty(config.getQueryColumn()) && config.getQueryColumn().contains(e.getName())).toList());
                                });

                    });


                    Map<String, String> customFile = new HashMap<>();
                    // vo
                    customFile.put("FormReq.java", "/templates/ftl/vo/formReq.java.ftl");
                    customFile.put("PageReq.java", "/templates/ftl/vo/pageReq.java.ftl");
                    customFile.put("Resp.java", "/templates/ftl/vo/resp.java.ftl");

                    customFile.forEach((k, v) -> {
                        consumer.customFile(new CustomFile.Builder()
                                .fileName(k)
                                .templatePath(v)
                                .enableFileOverride()
                                .filePath(projectPath + "/src/main/java/"
                                        + parentPackage.replace(".", "/")
                                        + "/" + moduleName + "/domain/vo/" + name)
                                .build());
                    });
                })
                // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .templateEngine(new EnhanceFreemarkerTemplateEngine(isPreview))
                .execute();
    }

}
