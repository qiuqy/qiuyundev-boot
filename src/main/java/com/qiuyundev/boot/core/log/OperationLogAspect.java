package com.qiuyundev.boot.core.log;

import cn.hutool.core.date.LocalDateTimeUtil;
import cn.hutool.core.exceptions.ExceptionUtil;
import com.qiuyundev.boot.base.enums.ApiStatusEnum;
import com.qiuyundev.boot.base.util.IPUtils;
import com.qiuyundev.boot.base.util.JacksonUtils;
import com.qiuyundev.boot.base.util.MultiThreadUtils;
import com.qiuyundev.boot.base.util.ServletUtils;
import com.qiuyundev.boot.core.ip2region.Ip2regionService;
import com.qiuyundev.boot.core.security.SecurityUtils;
import com.qiuyundev.boot.domain.entity.SysOperationLog;
import com.qiuyundev.boot.service.ISysOperationLogService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Objects;

/**
 * 操作日志切面
 *
 * @author Qiuyun
 * @since 2024/05/21
 */
@Slf4j
@Aspect
@Component
@RequiredArgsConstructor
public class OperationLogAspect {
    private final ISysOperationLogService operationLogService;
    private final Ip2regionService ip2regionService;

    @Around("@annotation(operationLog)")
    public Object logOperation(ProceedingJoinPoint joinPoint, OperationLog operationLog) throws Throwable {
        LocalDateTime startTime = LocalDateTime.now();
        Object result = null;
        Exception exception = null;
        try {
            result = joinPoint.proceed();
            log.info("Method executed successfully.");
            return result;
        } catch (Exception e) {
            String methodName = joinPoint.getSignature().getName();
            String className = joinPoint.getSignature().getDeclaringTypeName();
            log.error("Error executing method {}.{}()", className, methodName, e);
            exception = e;
            throw e;
        } finally {
            writeOperationLog(joinPoint, operationLog, result, startTime, exception);
        }
    }


    /**
     * 写入操作日志表
     *
     * @param joinPoint     切点信息
     * @param operationLog  操作日志注解元数据
     * @param result        接口返回结果
     * @param startTime     开始时间
     * @param exception     异常信息
     */
    public void writeOperationLog(JoinPoint joinPoint, OperationLog operationLog, Object result, LocalDateTime startTime, Exception exception) {
        String methodName = joinPoint.getSignature().getName();
        String className = joinPoint.getSignature().getDeclaringTypeName();
        // 创建操作日志对象
        SysOperationLog sysOperationLog = new SysOperationLog();
        sysOperationLog.setModule(operationLog.module());
        sysOperationLog.setTag(operationLog.tag());
        sysOperationLog.setSummary(operationLog.summary());
        sysOperationLog.setClazz(className);
        sysOperationLog.setMethod(methodName);
        sysOperationLog.setType(operationLog.type());
        // 接口请求方式
        HttpServletRequest currentRequest = ServletUtils.getCurrentRequest();
        if (Objects.nonNull(currentRequest)) {
            sysOperationLog.setRequestMethod(currentRequest.getMethod());
            sysOperationLog.setRequestUrl(currentRequest.getRequestURI());
            sysOperationLog.setRequestIp(IPUtils.getIp(currentRequest));
            sysOperationLog.setRequestLocation(ip2regionService.search(sysOperationLog.getRequestIp()));
        }
        sysOperationLog.setRequestParam(ServletUtils.getParams(currentRequest));
        if (Objects.nonNull(result)) {
           sysOperationLog.setResponseJson(JacksonUtils.toJson(result));
        }
        if (null == exception) {
            sysOperationLog.setStatus(ApiStatusEnum.SUCCESS);
        } else {
            sysOperationLog.setStatus(ApiStatusEnum.FAILURE);
            sysOperationLog.setExceptionInfo(ExceptionUtil.stacktraceToString(exception));
        }
        LocalDateTime endTime = LocalDateTime.now();
        sysOperationLog.setCostTime(LocalDateTimeUtil.between(startTime, endTime, ChronoUnit.MILLIS));
        String username = SecurityUtils.getUsername();
        if (StringUtils.hasText(username)) {
            sysOperationLog.setOperator(username);
        }
        sysOperationLog.setOperationTime(endTime);

        log.info("Execution time: {}ms", sysOperationLog.getCostTime());
        MultiThreadUtils.executeAsyncTasks(() -> {
            log.info("Saving operation log: {}", sysOperationLog);
            // 保存操作日志到数据库
             operationLogService.save(sysOperationLog);
        });
    }



}
