package com.qiuyundev.boot.core.log;

import com.qiuyundev.boot.base.enums.serialize.IEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 模块枚举
 *
 * @author Qiuyun
 * @since 2024/05/21
 */
@Getter
@AllArgsConstructor
public enum ModuleEnum implements IEnum {
    SYSTEM("系统模块", "orange"),
    TOOL("工具模块", "gold"),
    // 未定义模块
    UNDEFINED("未定义模块", "default");

    private final String text;
    private final String color;
}
