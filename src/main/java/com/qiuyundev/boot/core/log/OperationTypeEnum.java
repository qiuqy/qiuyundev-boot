package com.qiuyundev.boot.core.log;

import com.qiuyundev.boot.base.enums.serialize.IEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 操作类型枚举
 *
 * @author Qiuyun
 * @since 2024/05/21
 */
@Getter
@AllArgsConstructor
public enum OperationTypeEnum implements IEnum {
    ADD("新增", "orange"),
    UPDATE("修改", "gold"),
    DELETE("删除", "lime"),
    IMPORT("导入", "green"),
    EXPORT("导出", "cyan"),
    DOWNLOAD("下载", "blue"),
    UPLOAD("上传", "geekblue"),
    QUERY("查询", "purple"),
    OTHER("其他", "default");

    private final String text;
    private final String color;

}
