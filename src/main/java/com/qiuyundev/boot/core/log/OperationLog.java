package com.qiuyundev.boot.core.log;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 操作日志注解
 *
 * @author Qiuyun
 * @since 2024/05/14
 */
// 该注解只能用于方法级别
@Target(ElementType.METHOD)
// 运行时保留，这样可以在运行时通过反射获取到注解信息
@Retention(RetentionPolicy.RUNTIME)
public @interface OperationLog {
    /**
     * 模块
     */
    ModuleEnum module() default ModuleEnum.UNDEFINED;

    /**
     * 标签
     */
    String tag() default "";


    /**
     * 类型
     */
    OperationTypeEnum type() default OperationTypeEnum.OTHER;


    @AliasFor("summary")
    String value() default "";

    /**
     * 接口概要
     *
     * @return 接口概要
     */
    @AliasFor("value")
    String summary() default "";
}
