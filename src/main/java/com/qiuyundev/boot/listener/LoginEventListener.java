package com.qiuyundev.boot.listener;

import com.qiuyundev.boot.core.security.LoginUser;
import com.qiuyundev.boot.core.security.event.custom.LoginSuccessEventPublisher;
import com.qiuyundev.boot.service.ISysLoginLogService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class LoginEventListener {
    private final ISysLoginLogService loginLogService;

    @EventListener
    public void handleLoginSuccess(LoginSuccessEventPublisher.LoginSuccessEvent event) {
        LoginUser.LoginInfo loginInfo = event.getLoginInfo();
        // 保存日志到数据库
        this.loginLogService.saveLoginLog(loginInfo);
    }
}
