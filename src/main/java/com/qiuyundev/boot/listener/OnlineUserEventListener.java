package com.qiuyundev.boot.listener;

import com.qiuyundev.boot.core.security.LoginUser;
import com.qiuyundev.boot.core.security.event.custom.OnlineUserEventPublisher;
import com.qiuyundev.boot.core.sse.SseMessage;
import com.qiuyundev.boot.core.sse.SseService;
import com.qiuyundev.boot.service.IOnlineUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class OnlineUserEventListener {
    private final IOnlineUserService onlineUserService;
    private final SseService sseService;
    @EventListener
    public void handleLoginSuccess(OnlineUserEventPublisher.OnlineUserEvent event) {
        switch (event.getType()){
            case LOGIN, UPDATE_EXPIRE_TIME -> onlineUserService.addOrUpdateOnlineUser(event.getLoginInfo(), event.getExpireTime());
            case LOGOUT -> onlineUserService.deleteOnlineUser(event.getLoginInfo());
        }
        // sse 通知前端重新查询在线用户信息
        SseMessage<LoginUser.LoginInfo> sseMessage = new SseMessage<>();
        sseMessage.setData(event.getLoginInfo());
        sseMessage.setType(SseMessage.SseMessageTypeEnum.updateOnlineUserCount);
        this.sseService.sendMessage(sseMessage);
    }
}
