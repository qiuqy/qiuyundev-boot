package com.qiuyundev.boot.mapper;

import com.qiuyundev.boot.domain.entity.SysFile;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 文件 Mapper 接口
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-20
 */
public interface SysFileMapper extends BaseMapper<SysFile> {

}
