package com.qiuyundev.boot.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qiuyundev.boot.domain.entity.SysRoleMenu;

/**
 * <p>
 * 角色菜单 Mapper 接口
 * </p>
 *
 * @author Qiuyun
 * @since 2024/04/11
 */
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

}
