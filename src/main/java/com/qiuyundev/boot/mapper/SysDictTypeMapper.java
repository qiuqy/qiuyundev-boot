package com.qiuyundev.boot.mapper;

import com.qiuyundev.boot.domain.entity.SysDictType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 字典类型 Mapper 接口
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-13
 */
public interface SysDictTypeMapper extends BaseMapper<SysDictType> {

}
