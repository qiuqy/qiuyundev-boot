package com.qiuyundev.boot.mapper;

import com.qiuyundev.boot.domain.entity.SysOperationLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 操作日志 Mapper 接口
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-21
 */
public interface SysOperationLogMapper extends BaseMapper<SysOperationLog> {

}
