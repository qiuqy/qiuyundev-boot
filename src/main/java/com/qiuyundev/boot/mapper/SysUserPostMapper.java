package com.qiuyundev.boot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qiuyundev.boot.domain.entity.SysUserPost;

public interface SysUserPostMapper extends BaseMapper<SysUserPost> {
}
