package com.qiuyundev.boot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qiuyundev.boot.domain.entity.SysLoginLog;

/**
 * 登录日志 Mapper 接口
 *
 * @author Qiuyun
 * @since 2024/04/26
 */
public interface SysLoginLogMapper extends BaseMapper<SysLoginLog> {
}
