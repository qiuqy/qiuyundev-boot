package com.qiuyundev.boot.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qiuyundev.boot.domain.entity.SysRoleDept;

/**
 * <p>
 * 角色部门 Mapper 接口
 * </p>
 *
 * @author Qiuyun
 * @since 2024/04/11
 */
public interface SysRoleDeptMapper extends BaseMapper<SysRoleDept> {

}
