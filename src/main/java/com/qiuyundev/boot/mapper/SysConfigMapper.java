package com.qiuyundev.boot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qiuyundev.boot.domain.entity.SysConfig;

public interface SysConfigMapper extends BaseMapper<SysConfig> {
}
