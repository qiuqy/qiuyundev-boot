package com.qiuyundev.boot.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qiuyundev.boot.domain.entity.SysRole;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;


/**
 * <p>
 * 角色 Mapper 接口
 * </p>
 *
 * @author Qiuyun
 * @since 2024/04/11
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

    @Select("select sr.id, sr.code, sr.name, sr.data_scope from sys_role sr " +
            "inner join sys_user_role sur on sur.role_id = sr.id " +
            "where sr.is_deleted = 0 and sur.user_id = #{userId} ")
    List<SysRole> findByUserId(@Param("userId") Integer userId);
}
