package com.qiuyundev.boot.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qiuyundev.boot.domain.entity.SysMenu;
import com.qiuyundev.boot.domain.enums.menu.MenuTypeEnum;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 菜单 Mapper 接口
 * </p>
 *
 * @author Qiuyun
 * @since 2024/04/11
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {
    /**
     * 通过用户 id 查询权限
     * <p>
     * Caused by: net.sf.jsqlparser.parser.ParseException:
     * Encountered unexpected token: "ur" <K_ISOLATION>
     * 原因是 jsqlparser 解析 SQL 时 ur 不能用作别名
     * </p>
     *
     * @param userId 用户id
     * @return 权限集合
     */
    @Select("select distinct sm.permission from sys_user_role sur " +
            "inner join sys_role_menu srm on srm.role_id = sur.role_id " +
            "inner join sys_menu sm on sm.id = srm.menu_id and sm.is_deleted = 0 " +
            "where sur.user_id = #{userId} and sm.permission is not null and sm.permission <> ''")
    List<String> selectPermsByUserId(@Param("userId") Integer userId);

    @Select("<script>select distinct sm.* from  sys_menu sm " +
            "inner join sys_role_menu srm on srm.menu_id = sm.id " +
            "inner join sys_user_role sur on sur.role_id = srm.role_id " +
            "where sm.status = 'ENABLE' and sm.is_deleted = 0 and sur.user_id = #{userId} " +
            "<if test=\"typeList != null and typeList.size() != 0\">" +
            "and sm.type in " +
            "<foreach collection='typeList' index='index' item='item' open='(' separator=',' close=')'>#{item}</foreach>" +
            "</if>" +
            "</script>")
    List<SysMenu> selectMenusByUserIdAndType(@Param("userId") Integer userId, @Param("typeList") List<MenuTypeEnum> typeList);
}
