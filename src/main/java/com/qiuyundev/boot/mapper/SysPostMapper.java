package com.qiuyundev.boot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qiuyundev.boot.domain.entity.SysPost;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 职位 Mapper 接口
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-07
 */
public interface SysPostMapper extends BaseMapper<SysPost> {

    @Select("select sp.id, sp.name from sys_post sp " +
            "inner join sys_user_post sup on sup.post_id = sp.id " +
            "where sp.is_deleted = 0 and sup.user_id = #{userId} ")
    List<SysPost> findByUserId(@Param("userId") Integer userId);
}
