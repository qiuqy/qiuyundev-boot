package com.qiuyundev.boot.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qiuyundev.boot.domain.entity.SysUserRole;

/**
 * <p>
 * 用户角色 Mapper 接口
 * </p>
 *
 * @author Qiuyun
 * @since 2024/04/11
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}
