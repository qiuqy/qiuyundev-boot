package com.qiuyundev.boot.mapper;

import com.qiuyundev.boot.domain.entity.SysScheduleLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 任务日志表 Mapper 接口
 * </p>
 *
 * @author Qiuyun
 * @since 2024-06-03
 */
public interface SysScheduleLogMapper extends BaseMapper<SysScheduleLog> {

}
