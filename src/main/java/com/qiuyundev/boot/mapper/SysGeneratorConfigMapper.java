package com.qiuyundev.boot.mapper;

import com.qiuyundev.boot.domain.entity.SysGeneratorConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 代码生成器配置 Mapper 接口
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-31
 */
public interface SysGeneratorConfigMapper extends BaseMapper<SysGeneratorConfig> {

}
