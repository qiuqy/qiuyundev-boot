package com.qiuyundev.boot.mapper;

import com.qiuyundev.boot.domain.entity.SysSchedule;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 任务Mapper 接口
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-31
 */
public interface SysScheduleMapper extends BaseMapper<SysSchedule> {

}
