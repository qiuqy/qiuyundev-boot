package com.qiuyundev.boot.mapper;

import com.qiuyundev.boot.core.db.domain.TableColumnInfo;
import com.qiuyundev.boot.core.db.domain.TableInfo;
import com.qiuyundev.boot.core.db.provider.SqlProviderContext;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;

public interface DatabaseMapper {
    /**
     * 查询数据库有哪些表
     *
     * @return 数据库所有表
     */
    @SelectProvider(type = SqlProviderContext.class, method = "getQueryTablesSql")
    List<TableInfo> selectTables(String tableSchema, String tableName);

    /**
     * 查询表字段信息
     *
     * @return 数据库所有表
     */
    @SelectProvider(type = SqlProviderContext.class, method = "getQueryTableColumnsSql")
    List<TableColumnInfo> selectTableColumns(String tableSchema, String tableName);
}
