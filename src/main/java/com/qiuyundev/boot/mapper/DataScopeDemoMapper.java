package com.qiuyundev.boot.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.qiuyundev.boot.core.datascope.DataColumn;
import com.qiuyundev.boot.core.datascope.DataScope;
import com.qiuyundev.boot.domain.entity.SysUser;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface DataScopeDemoMapper extends BaseMapper<SysUser> {

    /**
     * 自定义部门数据权限
     */
    @DataScope(@DataColumn("dept_id"))
    SysUser selectById(Integer id);

    /**
     * 本人部门数据权限
     */
    @DataScope(@DataColumn("t.dept_id"))
    @Select("select t.id, t.username, t.dept_id from sys_user t where t.id = #{id}")
    List<SysUser> list(Integer id);

    /**
     * 本人部门及下级部门数据权限
     */
    @DataScope(@DataColumn("dept_id"))
    List<SysUser> selectBatchIds (@Param(Constants.COLL) List<Integer> ids);

    /**
     * 仅本人数据权限
     */
    @DataScope(@DataColumn("operator"))
    @Select("select * from sys_operation_log limit 10")
    List<Map<String, Object>> selectOperationLog();
}
