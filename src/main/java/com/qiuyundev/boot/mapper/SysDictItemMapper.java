package com.qiuyundev.boot.mapper;

import com.qiuyundev.boot.domain.entity.SysDictItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 字典项表 Mapper 接口
 * </p>
 *
 * @author Qiuyun
 * @since 2024-05-13
 */
public interface SysDictItemMapper extends BaseMapper<SysDictItem> {

}
