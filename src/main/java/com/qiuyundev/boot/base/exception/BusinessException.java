package com.qiuyundev.boot.base.exception;

import com.qiuyundev.boot.base.enums.response.IResponse;
import lombok.Getter;
import lombok.Setter;

/**
 * 业务异常
 *
 * @author Qiuyun
 * @since 2024/04/17
 */
@Getter
@Setter
public class BusinessException extends RuntimeException {
    private Integer code;
    private String message;

    public BusinessException(IResponse response) {
        super(response.getMessage());
        this.code = response.getCode();
        this.message = response.getMessage();
    }

    public BusinessException(String message, Throwable throwable) {
        super(message, throwable);
        this.code = 500;
        this.message = message;
    }

    public BusinessException(String message) {
        this.message = message;
    }
}
