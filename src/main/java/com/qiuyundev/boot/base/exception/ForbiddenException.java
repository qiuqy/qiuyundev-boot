package com.qiuyundev.boot.base.exception;

import com.qiuyundev.boot.base.enums.response.IResponse;
import com.qiuyundev.boot.base.enums.response.ResponseEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * 禁止访问异常
 *
 * @author Qiuyun
 * @since 2024/04/17
 */
@Getter
@Setter
public class ForbiddenException extends RuntimeException {

    private Integer code;
    private String message;

    public ForbiddenException() {
        super(ResponseEnum.FORBIDDEN.getMessage());
        this.code = ResponseEnum.FORBIDDEN.getCode();
        this.message = ResponseEnum.FORBIDDEN.getMessage();
    }

    public ForbiddenException(IResponse response) {
        super(response.getMessage());
        this.code = response.getCode();
        this.message = response.getMessage();
    }
}
