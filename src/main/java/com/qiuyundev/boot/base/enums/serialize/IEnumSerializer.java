package com.qiuyundev.boot.base.enums.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.lang.reflect.Field;

public class IEnumSerializer<T extends IEnum> extends JsonSerializer<T> { // implements ContextualSerializer {
    @Override
    public void serialize(T value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        // 序列化的时候使用 name
        gen.writeString(value.name());
        // 获取序列化对象
        Object currentValue = gen.getCurrentValue();
        // 序列化字段名称
        String fieldName = gen.getOutputContext().getCurrentName();
        Field field;
        try {
            field = currentValue.getClass().getDeclaredField(fieldName);
            // 获取注解的值
            EnumExtraInfo annotation = field.getAnnotation(EnumExtraInfo.class);
            // 如果有此注解，枚举序列化的时候额外生成一个包含枚举详细信息的字段
            if (null != annotation) {
                EnumView enumView = new EnumView();
                enumView.setCode(value.name());
                enumView.setText(value.getText());
                enumView.setColor(value.getColor());
                gen.writeObjectField(fieldName + "Enum", enumView);
            }
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        }
    }

}
