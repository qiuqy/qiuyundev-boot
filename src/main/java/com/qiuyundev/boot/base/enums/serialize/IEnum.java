package com.qiuyundev.boot.base.enums.serialize;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * 自定义枚举序列化规则
 * <p>
 * 实现此接口的枚举，在序列化的时候，会序列化为 {code: "", text: ""} 格式，方便前端展示
 * <p>
 * 反序列化采用的是枚举默认的反序列化
 *
 * @author Qiuyun
 * @see IEnumSerializer
 * // * @see IEnumDeserializer
 * @since 2024/04/20
 */
@JsonSerialize(using = IEnumSerializer.class)
public interface IEnum {
    String name();

    String getText();

    default String getColor() {
        return "";
    }

//    static <T extends Enum<T> & IEnum> T fromCode(Class<T> enumClass, Integer code) {
//        for (T value : enumClass.getEnumConstants()) {
//            if (value.getCode().equals(code)) {
//                return value;
//            }
//        }
//        throw new IllegalArgumentException("Invalid code: " + code + " for enum type: " + enumClass.getName());
//    }
}
