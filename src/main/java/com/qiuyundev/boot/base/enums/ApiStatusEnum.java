package com.qiuyundev.boot.base.enums;

import com.qiuyundev.boot.base.enums.serialize.IEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ApiStatusEnum implements IEnum {
    SUCCESS("成功", "success"),
    FAILURE("失败", "error");

    private final String text;
    private final String color;
}
