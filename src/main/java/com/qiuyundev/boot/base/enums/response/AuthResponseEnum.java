package com.qiuyundev.boot.base.enums.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
@AllArgsConstructor
public enum AuthResponseEnum implements IResponse {
    /**
     * 认证失败
     */
    UNAUTHORIZED(HttpStatus.UNAUTHORIZED.value(), "尚未登录或者登录过期，请重新登录"),

    /**
     * 通用没有权限访问资源提示
     */
    FORBIDDEN(HttpStatus.FORBIDDEN.value(), "没有权限访问资源"),

    /**
     * TOKEN 错误
     */
    TOKEN_ERROR(HttpStatus.INTERNAL_SERVER_ERROR.value(), "TOKEN 错误"),

    /**
     * 当前用户未登录
     */
    NOT_LOGIN(HttpStatus.UNAUTHORIZED.value(), "当前用户未登录"),

    /**
     * 用户名或密码错误
     */
    ACCOUNT_ERROR(HttpStatus.INTERNAL_SERVER_ERROR.value(), "用户名或密码错误"),

    /**
     * 用户已被禁用
     */
    ACCOUNT_DISABLED_ERROR(HttpStatus.INTERNAL_SERVER_ERROR.value(), "用户已被禁用"),

    /**
     * 用户已被锁定
     */
    ACCOUNT_LOCKED_ERROR(HttpStatus.INTERNAL_SERVER_ERROR.value(), "用户已被锁定"),

    /**
     * 账户过期
     */
    ACCOUNT_EXPIRED_ERROR(HttpStatus.INTERNAL_SERVER_ERROR.value(), "账户过期"),

    /**
     * 证书过期
     */
    CREDENTIALS_EXPIRED_ERROR(HttpStatus.INTERNAL_SERVER_ERROR.value(), "证书过期"),
    ;

    private final Integer code;
    private final String message;
}
