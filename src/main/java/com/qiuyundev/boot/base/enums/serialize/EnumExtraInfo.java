package com.qiuyundev.boot.base.enums.serialize;

import java.lang.annotation.*;

/**
 * 枚举额外信息
 * <p>
 * 有此注解的枚举字段，序列化的时候，额外生成一个包含枚举详细信息的字段
 *
 * @author Qiuyun
 * @since 2024/04/25
 */
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface EnumExtraInfo {
}
