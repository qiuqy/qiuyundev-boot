package com.qiuyundev.boot.base.enums.serialize;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EnumView {
    private String code;
    private String text;
    private String color;
}
