package com.qiuyundev.boot.base.enums;

import com.qiuyundev.boot.base.enums.serialize.IEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StatusEnum implements IEnum {
    ENABLE("启用", "success"),
    DISABLE("禁用", "error");

    private final String text;
    private final String color;
}

