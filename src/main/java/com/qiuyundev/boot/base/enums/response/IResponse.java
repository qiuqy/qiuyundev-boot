package com.qiuyundev.boot.base.enums.response;

public interface IResponse {
    Integer getCode();
    String getMessage();
}
