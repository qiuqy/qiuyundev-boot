package com.qiuyundev.boot.base.enums.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResponseEnum implements IResponse {
    /**
     * 通用成功提示
     */
    SUCCESS(200, "接口调用成功"),

    /**
     * 参数错误
     */
    BAD_REQUEST(400, "参数校验失败"),

    /**
     * 参数错误
     */
    UNAUTHORIZED(401, "认证失败，请重新登录"),

    /**
     * 禁止访问
     */
    FORBIDDEN(403, "没有权限访问资源"),

    /**
     * 数据不存在
     */
    NOT_FOUND(404, "数据不存在"),

    /**
     * 通用失败提示
     */
    FAILED(500, "接口调用失败"),
    ;

    private final Integer code;
    private final String message;
}
