package com.qiuyundev.boot.base.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 排序枚举
 *
 * @author Qiuyun
 * @since 2024/04/28
 */
@Getter
@AllArgsConstructor
public enum OrderEnum {
    ASC(true),
    DESC(false);

    private final boolean isAsc;
}
