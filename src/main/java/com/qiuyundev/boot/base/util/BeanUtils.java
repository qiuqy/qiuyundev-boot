package com.qiuyundev.boot.base.util;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class BeanUtils {
    /**
     * 复制对象
     * @param source 源对象数据
     * @param targetClazz 目前对象类型
     * @param <T> 对象类型
     * @return copy后的数据
     */
    public static <T> T copy(Object source, Class<T> targetClazz) {
        if(source == null || targetClazz == null) {
            return null;
        }
        try {
            T newInstance = targetClazz.getDeclaredConstructor().newInstance();
            org.springframework.beans.BeanUtils.copyProperties(source, newInstance);
            return newInstance;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 复制list
     * @param source 源对象数据
     * @param targetClazz 目前对象类型
     * @param <T> 源对象类型
     * @param <K> 目标对象类型
     * @return copy后的数据
     */
    public static <T, K> List<K> copyList(List<T> source, Class<K> targetClazz) {
        if (null == source || source.isEmpty()) {
            return Collections.emptyList();
        }
        return source.stream().map(e -> copy(e, targetClazz)).collect(Collectors.toList());
    }

}
