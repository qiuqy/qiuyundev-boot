package com.qiuyundev.boot.base.util.excel;

import cn.hutool.core.lang.Pair;
import cn.hutool.core.util.StrUtil;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestUtils {
    private static final String CREATE_TABLE_SQL_TEMPLATE = """
            -- 建表 {tableComment}
            DROP TABLE IF EXISTS ods_da_table_{originTable};
            CREATE TABLE ods_da_table_{originTable}(
            {createColumnInfo}
            )  COMMENT '{tableComment}';""";
    private static final String CREATE_COLUMN_DEFINITION_TEMPLATE = "   %s varchar(255) COMMENT '%s'%s\n";

    public static void main(String[] args) throws IOException {
        List<File> files = listFiles(new File("C:\\Users\\97208\\Desktop\\ods"));
        StringBuilder createTableSql = new StringBuilder();
        for (File file : files) {
            String originTable = file.getName().substring(0, file.getName().indexOf("."));
            String tableComment = "";
            List<Pair<String, String>> columns = new ArrayList<>();

            byte[] bytes = Files.readAllBytes(Paths.get(file.getAbsolutePath()));
            String content = new String(bytes, StandardCharsets.UTF_8);

            try (BufferedReader br = new BufferedReader(new StringReader(content))) {
                String line;
                while ((line = br.readLine()) != null) {
                    if (line.contains("TableComment: ")) {
                        tableComment = line.replace("|", "")
                                .replace("TableComment: ", "")
                                .trim();
                    }

                    if (countChar(line,'|') == 5 && !line.contains("Field")) {
                        String[] split = line.split("\\|");
                        columns.add(Pair.of(split[1].trim(), split[4].trim()));
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            StringBuilder createColumnInfo = new StringBuilder();
            for (int i = 0; i < columns.size(); i++) {
                Pair<String, String> column = columns.get(i);
                createColumnInfo.append(String.format(CREATE_COLUMN_DEFINITION_TEMPLATE,
                        column.getKey(),
                        column.getValue(),
                        i < columns.size() - 1  ? "," : ""
                ));
            }

            Map<String, String> param = new HashMap<>();
            param.put("originTable", originTable);
            param.put("tableComment", tableComment);
            param.put("createColumnInfo", createColumnInfo.toString());

            String createSql = StrUtil.format(CREATE_TABLE_SQL_TEMPLATE, param);
            createTableSql.append(createSql);
            createTableSql.append("\n\n");


        }

        writeToFile(String.format("C:\\Users\\97208\\Desktop\\ods\\sql\\%s.txt", "ods_da_table"),  createTableSql + "\n\n" );
    }


    public static void writeToFile(String filePath, String content) {
        File file = new File(filePath);

        try {
            // 如果文件已存在，则删除
            if (file.exists()) {
                file.delete();
            }

            // 创建文件
            file.createNewFile();

            // 使用BufferedWriter写入文件
            try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
                bw.write(content);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static int countChar(String str, char charToFind) {
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == charToFind) {
                count++;
            }
        }
        return count;
    }

    private static List<File> listFiles(File directory) {
        List<File> originTableFiles = new ArrayList<>();
        File[] files = directory.listFiles();
        for (File file : files) {
            if (file.isFile()) {
                originTableFiles.add(file);
            } else if (file.isDirectory()) {
//                listFiles(file);
            }
        }

        return originTableFiles;
    }

    private static String readData (File file) {
        StringBuilder data = new StringBuilder();

        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                data.append(line).append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return data.toString();
    }

    public static void createFile(String fileName) {
        String filePath = "C:\\Users\\97208\\Desktop\\ods\\%s.txt"; // 替换为你想要的文件路径
        BufferedWriter writer = null;

        try {
            writer = new BufferedWriter(new FileWriter(String.format(filePath)));
            writer.write(""); // 写入你想要的内容
            writer.close(); // 写入完成后，记得关闭writer
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (writer != null) {
                    writer.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
