package com.qiuyundev.boot.base.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qiuyundev.boot.base.vo.Response;
import jakarta.servlet.http.HttpServletResponse;
import lombok.SneakyThrows;
import org.springframework.http.HttpStatus;
import org.springframework.util.MimeTypeUtils;

public class ResponseUtils {

    /**
     * 将字符串渲染到客户端
     *
     * @param response 响应
     * @param res      响应的字符串
     */
    @SneakyThrows
    public static void renderToString(HttpServletResponse response, Response<?> res) {
        response.setStatus(HttpStatus.OK.value());
        response.setContentType(MimeTypeUtils.APPLICATION_JSON_VALUE);
        response.setCharacterEncoding("UTF-8");
        response.getWriter()
                .print(new ObjectMapper().writeValueAsString(res));
    }
}
