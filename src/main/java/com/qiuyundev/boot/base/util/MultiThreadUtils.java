package com.qiuyundev.boot.base.util;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * 多线程工具类
 *
 * @author Qiuyun
 * @since 2024/04/17
 */
public class MultiThreadUtils {
    private static final int THREAD_POOL_SIZE = 20;
    private static final ExecutorService executorService = Executors.newFixedThreadPool(THREAD_POOL_SIZE);

    public static <T> List<T> executeTasks(List<Callable<T>> tasks) throws InterruptedException, ExecutionException {
        List<Future<T>> futures = new ArrayList<>();
        for (Callable<T> task : tasks) {
            Future<T> future = executorService.submit(task);
            futures.add(future);
        }

        List<T> results = new ArrayList<>();
        for (Future<T> future : futures) {
            results.add(future.get());
        }

        return results;
    }

    public static void executeAsyncTasks(Runnable... tasks) {
        List<CompletableFuture<Void>> futures = new ArrayList<>();
        for (Runnable task : tasks) {
            futures.add(CompletableFuture.runAsync(task, executorService));
        }

        CompletableFuture.allOf(futures.toArray(new CompletableFuture[0])).join();
    }

    public static void shutdown() {
        executorService.shutdown();
    }
}
