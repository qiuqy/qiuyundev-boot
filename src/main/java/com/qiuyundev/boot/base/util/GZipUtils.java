package com.qiuyundev.boot.base.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class GZipUtils {
    /**
     * 压缩字符串
     *
     * @param str str
     * @return 压缩后的结果
     * @throws IOException IOException
     */
    public static byte[] compress(String str) throws IOException {
        if (str == null || str.length() == 0) {
            return null;
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try (GZIPOutputStream gzip = new GZIPOutputStream(out)) {
            gzip.write(str.getBytes(StandardCharsets.UTF_8));
        }
        return out.toByteArray();
    }

    public static String decompress(byte[] compressed) throws IOException {
        if (compressed == null || compressed.length == 0) {
            return null;
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try (GZIPInputStream gunzip = new GZIPInputStream(new ByteArrayInputStream(compressed))) {
            byte[] buffer = new byte[1024];
            int n;
            while ((n = gunzip.read(buffer)) >= 0) {
                out.write(buffer, 0, n);
            }
        }
        return out.toString(StandardCharsets.UTF_8);
    }

    public static void main(String[] args) throws IOException {
        String str = "https://www.baidu.com/s?ie=utf-8&f=8&rsv_bp=1&rsv_idx=1&tn=baidu&wd=%E5%AD%97%E7%AC%A6%E4%B8%B2%E5%8E%8B%E7%BC%A9%E7%AE%97%E6%B3%95&fenlei=256&oq=%25E5%25AD%2597%25E7%25AC%25A6%25E4%25B8%25B2%25E5%258E%258B%25E7%25BC%25A9%25E7%25AE%2597%25E6%25B3%2595%25E5%259C%25A8%25E7%25BA%25BF&rsv_pq=a2377aab0015fbff&rsv_t=4b92KJzKXyRzXoBh7zSfrsWyndHUhn1ZcqCID5SUK6QmSMC9SFmtXm01Ays&rqlang=cn&rsv_enter=1&rsv_dl=tb&rsv_btype=t&inputT=498&rsv_sug3=31&rsv_sug1=27&rsv_sug7=100&rsv_sug2=0&rsv_sug4=992&rsv_sug=1";

        byte[] compressedBytes = compress(str);
//        System.out.println(compressedBytes);
        System.out.println(str.getBytes().length);
        System.out.println("压缩后\t" + compressedBytes.length);
        System.out.println("压缩后string字符串\t" + new String(compressedBytes, StandardCharsets.UTF_8));
        System.out.println("压缩后string字符串\t" + new String(compressedBytes, StandardCharsets.UTF_8));
        System.out.println("压缩后string长度\t" + new String(compressedBytes).length());
        String uncompressStr = decompress(compressedBytes);

        System.out.println("解压后\t" + uncompressStr.length());
        System.out.println(uncompressStr);


        byte[] originalBytes = "Hello, World!".getBytes();

        // 转换为Base64字符串
        String base64String = Base64.getEncoder().encodeToString(originalBytes);

        // 打印Base64字符串
        System.out.println("Base64 Encoded String: " + base64String);

        // 计算Base64字符串和原始byte数组的大小
        System.out.println("Original byte array size: " + originalBytes.length + " bytes");
        System.out.println("Base64 encoded size: " + base64String.getBytes().length + " bytes");
    }
}
