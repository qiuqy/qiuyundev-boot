package com.qiuyundev.boot.base.util;

import cn.hutool.extra.spring.SpringUtil;
import org.springframework.stereotype.Component;

/**
 * Spring 工具类
 *
 * @author Qiuyun
 * @since  2023/1/19
 */
@Component
public final class SpringUtils extends SpringUtil {
}
