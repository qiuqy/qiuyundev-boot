package com.qiuyundev.boot.base.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * 数据库工具类
 *
 * @author Qiuyun
 * @date 2023/1/21
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DatabaseUtils {

    public static String findInSet(Object param, String column) {
        // find_in_set(100 , '0,100,101')
        return "find_in_set(" + param.toString() + " , " + column + ") <> 0";
    }
}
