package com.qiuyundev.boot.base.util;

import com.github.pagehelper.PageHelper;
import com.qiuyundev.boot.base.vo.PageParam;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PageUtils<T> {

    public static <T> void startPage(PageParam<T> pageParam) {
        try (com.github.pagehelper.Page<T> ignored = PageHelper.startPage(pageParam.getPage(), pageParam.getPageSize())) {
            log.error("startPage 异常：{}", pageParam);
        }
    }
}
