package com.qiuyundev.boot.base.util;

import java.util.ArrayList;
import java.util.List;

public class ListUtils {

    /**
     * 拆分列表
     *
     * @param list 列表
     * @param size 拆分成多大一个列表
     * @return 拆分后的列表
     */
    public static  <T> List<List<T>> parts(List<T> list, int size) {
        int numOfParts = (list.size() + size - 1) / size;
        List<List<T>> parts = new ArrayList<>(numOfParts);
        for (int i = 0; i < numOfParts; i++) {
            int start = i * size;
            int end = Math.min(start + size, list.size());
            List<T> subList = list.subList(start, end);
            parts.add(subList);
        }
        return parts;
    }
}
