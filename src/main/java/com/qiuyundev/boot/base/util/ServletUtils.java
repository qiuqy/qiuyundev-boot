package com.qiuyundev.boot.base.util;

import cn.hutool.extra.servlet.ServletUtil;
import jakarta.servlet.http.HttpServletRequest;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.util.ContentCachingRequestWrapper;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * servlet 工具类
 *
 * @author Qiuyun
 * @since 2024/05/21
 */
@Slf4j
@UtilityClass
public class ServletUtils {

    public static HttpServletRequest getCurrentRequest() {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        return (attributes != null) ? attributes.getRequest() : null;
    }

    public static String getRequestMethod() {
        HttpServletRequest request = getCurrentRequest();
        return (request != null) ? request.getMethod() : null;
    }

    public static boolean isContentTypeJson(HttpServletRequest request) {
        String contentType = Objects.requireNonNull(request).getContentType();
        if (contentType != null && contentType.toLowerCase().contains("application/json")) {
            return true;
        }
        return false;
    }

    public static String getParams(HttpServletRequest request) {
        if (null == request) {
            request = getCurrentRequest();
        }
        // 判断 contentType 是否是 application/json
        if (ServletUtils.isContentTypeJson(request)) {
            // 获取请求体中的参数，注意这适用于表单提交和JSON提交，对于复杂的JSON数据可能需要特殊处理
            try {
                // spring boot 中，请求体的参数只能被读取一遍
                // 确保请求被ContentCachingRequestWrapper包装过
                if (request instanceof ContentCachingRequestWrapper cachingRequest) {
                    // 获取请求体的字节数组
                    byte[] requestBodyBytes = cachingRequest.getContentAsByteArray();
                    // 转换为字符串（注意指定正确的字符编码）
                    return new String(requestBodyBytes, StandardCharsets.UTF_8);
                }
            } catch (Exception e) {
                log.info("Error reading request body: {}", e.getMessage());
            }
        } else {
            StringBuilder params = new StringBuilder();
            // 对于GET请求和其他非POST请求，可以从request.getParameterMap()获取参数
            Map<String, String[]> parameterMap = Objects.requireNonNull(request).getParameterMap();
            for (Map.Entry<String, String[]> entry : parameterMap.entrySet()) {
                params.append(entry.getKey()).append("=").append(Arrays.toString(entry.getValue())).append("&");
            }
            // 移除最后一个&
            params = new StringBuilder(params.substring(0, params.length() - 1));
            return params.toString();
        }

        return null;
    }
}
