package com.qiuyundev.boot.base.util;


import java.util.*;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Function;

public class TreeUtils<T> {
    // 定义节点数据结构
    public static class TreeNode<T> {
        private T data;
        private List<TreeNode<T>> children;

        public TreeNode(T data) {
            this.data = data;
            this.children = new ArrayList<>();
        }

        public T getData() {
            return data;
        }

        public void setData(T data) {
            this.data = data;
        }

        public List<TreeNode<T>> getChildren() {
            return children;
        }

        public void setChildren(List<TreeNode<T>> children) {
            this.children = children;
        }

        public void addChild(TreeNode<T> child) {
            this.children.add(child);
        }
    }

    /**
     * 构建树形结构
     *
     * @param nodes        扁平化节点列表，每个节点包含其父节点的标识信息（如ID）
     * @param parentIdFunc 用于从节点中获取父节点标识的函数
     * @param idFunc       用于从节点中获取节点标识的函数
     * @param rootId       根节点标识
     * @return 根节点
     */
    public TreeNode<T> buildTree(List<T> nodes, Function<T, Object> parentIdFunc, Function<T, Object> idFunc, Object rootId) {
        Map<Object, TreeNode<T>> nodeMap = new HashMap<>();
        for (T node : nodes) {
            Object nodeId = idFunc.apply(node);
            TreeNode<T> treeNode = new TreeNode<>(node);
            nodeMap.put(nodeId, treeNode);

            Object parentId = parentIdFunc.apply(node);
            if (!rootId.equals(parentId)) {
                TreeNode<T> parentNode = nodeMap.get(parentId);
                if (parentNode != null) {
                    parentNode.addChild(treeNode);
                }
            }
        }

        return nodeMap.get(rootId);
    }

    /**
     * 递归查找节点
     *
     * @param node           当前节点
     * @param targetData     目标数据
     * @param dataComparator 用于比较节点数据与目标数据是否相等的函数
     * @return 匹配的节点，未找到返回null
     */
    public TreeNode<T> findNode(TreeNode<T> node, T targetData, BiPredicate<T, T> dataComparator) {
        if (dataComparator.test(node.getData(), targetData)) {
            return node;
        }

        for (TreeNode<T> child : node.getChildren()) {
            TreeNode<T> foundNode = findNode(child, targetData, dataComparator);
            if (foundNode != null) {
                return foundNode;
            }
        }

        return null;
    }

    /**
     * 递归添加节点
     *
     * @param parentNode 父节点
     * @param newNode    待添加的新节点
     */
    public void addNode(TreeNode<T> parentNode, TreeNode<T> newNode) {
        parentNode.addChild(newNode);
    }

    /**
     * 递归删除节点
     *
     * @param parentNode     父节点
     * @param targetNode     待删除的目标节点
     * @param dataComparator 用于比较节点数据与目标节点数据是否相等的函数
     * @return 是否成功删除节点
     */
    public boolean deleteNode(TreeNode<T> parentNode, TreeNode<T> targetNode, BiPredicate<T, T> dataComparator) {
        if (dataComparator.test(parentNode.getData(), targetNode.getData())) {
            parentNode.getChildren().remove(targetNode);
            return true;
        }

        for (TreeNode<T> child : parentNode.getChildren()) {
            if (deleteNode(child, targetNode, dataComparator)) {
                return true;
            }
        }

        return false;
    }

    /**
     * 遍历树（深度优先搜索）
     *
     * @param node    起始节点
     * @param visitor 访问节点的回调函数
     */
    public void traverseDFS(TreeNode<T> node, Consumer<TreeNode<T>> visitor) {
        visitor.accept(node);
        for (TreeNode<T> child : node.getChildren()) {
            traverseDFS(child, visitor);
        }
    }

    /**
     * 遍历树（广度优先搜索）
     *
     * @param node    起始节点
     * @param visitor 访问节点的回调函数
     */
    public void traverseBFS(TreeNode<T> node, Consumer<TreeNode<T>> visitor) {
        Queue<TreeNode<T>> queue = new LinkedList<>();
        queue.offer(node);

        while (!queue.isEmpty()) {
            TreeNode<T> current = queue.poll();
            visitor.accept(current);
            queue.addAll(current.getChildren());
        }
    }


}
