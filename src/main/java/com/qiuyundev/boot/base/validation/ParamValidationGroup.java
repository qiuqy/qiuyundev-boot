package com.qiuyundev.boot.base.validation;

/**
 * 接口校验的分组
 *
 * @author Qiuyun
 * @since 2024/04/22
 */
public class ParamValidationGroup {
    /**
     * 新增
     */
    public interface Add {}

    /**
     * 修改
     */
    public interface Update {}
}
