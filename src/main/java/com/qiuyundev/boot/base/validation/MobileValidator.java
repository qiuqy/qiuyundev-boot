package com.qiuyundev.boot.base.validation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.springframework.util.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 手机号校验器
 *
 * @author Qiuyun
 * @date 2023/01/17
 */
public class MobileValidator implements ConstraintValidator<Mobile, CharSequence> {

    private static final Pattern MOBILE_PATTERN = Pattern.compile("^1[34578]\\d{9}$");

    /**
     * 在验证开始前调用注解里的方法，从而获取到一些注解里的参数
     *
     * @param constraintAnnotation annotation instance for a given constraint declaration
     */
    @Override
    public void initialize(Mobile constraintAnnotation) {
    }

    /**
     * 判断参数是否合法
     *
     * @param value   object to validate
     * @param context context in which the constraint is evaluated
     */
    @Override
    public boolean isValid(CharSequence value, ConstraintValidatorContext context) {
        if (StringUtils.hasText(value)) {
            return isMobile(value);
        }
        return false;
    }

    private boolean isMobile(final CharSequence str) {
        Matcher m = MOBILE_PATTERN.matcher(str);
        return m.matches();
    }
}
