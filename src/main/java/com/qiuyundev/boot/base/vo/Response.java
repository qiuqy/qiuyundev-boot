package com.qiuyundev.boot.base.vo;

import com.qiuyundev.boot.base.enums.response.IResponse;
import com.qiuyundev.boot.base.enums.response.ResponseEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Response<T> {
    /**
     * 响应状态码
     */
    private int code;
    /**
     *  响应信息
     */
    private String message;

    /**
     * 响应数据
     */
    private T data;

    public static Response<Void> success() {
        return new Response<>(ResponseEnum.SUCCESS.getCode(), ResponseEnum.SUCCESS.getMessage(), null);
    }

    public static <T> Response<T> success(T data) {
        return new Response<>(ResponseEnum.SUCCESS.getCode(), ResponseEnum.SUCCESS.getMessage(), data);
    }

    public static <T> Response<T> success(String message, T data) {
        return new Response<>(ResponseEnum.SUCCESS.getCode(), message, data);
    }

    public static Response<Void> failed() {
        return new Response<>(ResponseEnum.FAILED.getCode(), ResponseEnum.FAILED.getMessage(), null);
    }

    public static Response<Void> failed(String message) {
        return new Response<>(ResponseEnum.FAILED.getCode(), message, null);
    }

    public static Response<Void> failed(IResponse errorResponse) {
        return new Response<>(errorResponse.getCode(), errorResponse.getMessage(), null);
    }

    public static Response<Void> failed(Integer code, String message) {
        return new Response<>(code, message, null);
    }

    public static <T> Response<T> failed(Integer code, String message, T data) {
        return new Response<>(code, message, data);
    }

    public boolean isSuccess() {
        return ResponseEnum.SUCCESS.getCode().equals(this.code);
    }

}
