package com.qiuyundev.boot.base.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * API 返回结果的简要封装
 *
 * @author Qiuyun
 * @since 2024/04/11
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ApiResult {
    private boolean success;
    private String message;

    public static ApiResult success() {
        return new ApiResult(true, null);
    }

    public static ApiResult failed(String message) {
        return new ApiResult(false, message);
    }
}
