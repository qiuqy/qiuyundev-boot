package com.qiuyundev.boot.base.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 字典
 *
 * @author Qiuyun
 * @since 2024/04/11
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Dict {
    /**
     * 编码
     */
    private String code;

    /**
     * 名称
     */
    private String name;
}
