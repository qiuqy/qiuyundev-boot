package com.qiuyundev.boot.base.vo;

import java.util.List;

public interface Tree<T, K> {

    /**
     * 获取id
     */
    T getId();

    /**
     * 获取父id
     */
    T getParentId();

    /**
     * 获取排序
     */
    K getSort();

    /**
     * 子节点
     */
    void setChildren(List<Tree<T, K>> children);
}
