package com.qiuyundev.boot.base.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * 下拉选选项
 *
 * @author Qiuyun
 * @since 2024/05/13
 */
@Getter
@Setter
public class Option {
    private String label;
    private String value;
}
