package com.qiuyundev.boot.base.vo;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.github.pagehelper.PageInfo;
import com.qiuyundev.boot.base.util.BeanUtils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class PageResult<T> {

    /**
     * 分页信息
     */
    @JsonProperty("meta") // todo 序列化名称临时处理，与前端保持一致，后续应改前端名称为后端定义的名称
    private Pagination pagination;

    /**
     * 记录
     */
    @JsonProperty("items") // todo 序列化名称临时处理，与前端保持一致，后续应改前端名称为后端定义的名称
    private List<T> records;

    /**
     * 分页结果封装
     *
     * @param page    分页结果
     * @param targetClazz 记录类型
     * @return 分页结果
     */
    public static <T> PageResult<T> of(IPage<?> page, Class<T> targetClazz) {
        Pagination pagination = Pagination.builder().total(page.getTotal())
                .pages(page.getPages())
                .page(page.getCurrent())
                .pageSize(page.getSize())
                .build();
        List<T> records = BeanUtils.copyList(page.getRecords(), targetClazz);
        return new PageResult<>(pagination, records);
    }

    public static <T> PageResult<T> of(long page, long pageSize, long total, List<T> records) {
        long totalPages = (total + pageSize - 1) / pageSize;
        Pagination pagination = Pagination.builder().total(total)
                .pages(totalPages)
                .page(page)
                .pageSize(pageSize)
                .build();
        return new PageResult<>(pagination, records);
    }

    public static <T> PageResult<T> of(PageInfo<T> pageInfo) {
        Pagination pagination = Pagination.builder().total(pageInfo.getTotal())
                .pages(pageInfo.getPages())
                .page(pageInfo.getPageNum())
                .pageSize(pageInfo.getPageSize())
                .build();
        return new PageResult<>(pagination, pageInfo.getList());
    }


    /**
     * 分页信息
     *
     * @author Qiuyun
     * @since 2024/04/18
     */
    @Getter
    @Setter
    @Builder
    public static class Pagination {
        /**
         * 总记录数
         */
        @JsonProperty("totalItems") // todo 序列化名称临时处理，与前端保持一致，后续应改前端名称为后端定义的名称
        private long total;

        /**
         * 页面总数
         */
        @JsonProperty("totalPages") // todo 序列化名称临时处理，与前端保持一致，后续应改前端名称为后端定义的名称
        private long pages;

        /**
         * 当前页码
         */
        @JsonProperty("currentPage") // todo 序列化名称临时处理，与前端保持一致，后续应改前端名称为后端定义的名称
        private long page;

        /**
         * 单页大小
         */
        @JsonProperty("itemsPerPage") // todo 序列化名称临时处理，与前端保持一致，后续应改前端名称为后端定义的名称
        private long pageSize;
    }
}
