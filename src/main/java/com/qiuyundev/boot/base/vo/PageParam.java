package com.qiuyundev.boot.base.vo;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qiuyundev.boot.base.enums.OrderEnum;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.StringUtils;


/**
 * 分页请求参数
 *
 * <p>分页请求的参数继承此接口<p/>
 *
 * @author Qiuyun
 * @since 2024/04/11
 */
@Getter
@Setter
public class PageParam<T> {
    /**
     * 页码
     */
    @NotNull(message = "页码不能为空")
    @Min(value = 1, message = "页码最小值为 1")
    private Integer page = 1;

    /**
     * 每页显示记录数
     */
    @NotNull(message = "每页条数不能为空")
    @Min(value = 1, message = "每页条数最小值为 1")
    @Max(value = 100, message = "每页条数最大值为 100")
    private Integer pageSize = 20;

    /**
     * 排序字段
     */
    private String field;

    /**
     * 排序
     */
    private OrderEnum order;


    public IPage<T> page() {
        Page<T> page = new Page<>(this.getPage(), this.pageSize);
        // 排序字段处理
        if (StringUtils.hasText(this.field)) {
            page.addOrder(null == order || !order.isAsc()
                    ? OrderItem.desc(this.field)
                    : OrderItem.asc(this.field));
        }
        return page;
    }
}
