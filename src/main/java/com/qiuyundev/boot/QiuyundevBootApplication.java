package com.qiuyundev.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@EnableAsync
@EnableScheduling
@SpringBootApplication
public class QiuyundevBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(QiuyundevBootApplication.class, args);
    }

}
