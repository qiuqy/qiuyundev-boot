package ${package.ServiceImpl};

import ${package.Entity}.${entity};
import ${package.Mapper}.${table.mapperName};
<#if table.serviceInterface>
import ${package.Service}.${table.serviceName};
</#if>
import ${superServiceImplClassPackage};
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;
import com.baomidou.mybatisplus.core.metadata.IPage;

import java.util.Arrays;
import ${parentPackage}.base.util.BeanUtils;
import ${parentPackage}.base.vo.PageResult;
import ${packageVo}.${formReqName};
import ${packageVo}.${pageReqName};
import ${packageVo}.${respName};

/**
 * <p>
 * ${table.comment!} 服务实现类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@Service
public class ${table.serviceImplName} extends ${superServiceImplClass}<${table.mapperName}, ${entity}><#if table.serviceInterface> implements ${table.serviceName}</#if> {
   /**
    * 分页查询${table.comment!}
    *
    * @param pageReq ${table.comment!}分页查询参数
    * @return ${table.comment!}列表
    */
   @Override
   public PageResult<${respName}> page(${pageReqName} pageReq) {
      IPage<${entity}> page = this.lambdaQuery()
      .orderByDesc(${entity}::getUpdateTime)
      .page(pageReq.page());

      return PageResult.of(page, ${respName}.class);
   }

   /**
    * ${table.comment!}详情
    *
    * @param id ${table.comment!}ID
    * @return ${table.comment!}详情
    */
   @Override
   public ${respName} detail(Integer id) {
      return BeanUtils.copy(this.getById(id), ${respName}.class);
   }

   /**
    * 新增${table.comment!}
    *
    * @param formReq ${table.comment!}新增参数
    * @return ${table.comment!}新增结果
    */
   @Override
   public Boolean add(${formReqName} formReq) {
      return this.save(BeanUtils.copy(formReq, ${entity}.class));
   }


   /**
    * 修改${table.comment!}
    *
    * @param formReq ${table.comment!}修改参数
    * @return ${table.comment!}修改结果
    */
   @Override
   public Boolean update(${formReqName} formReq) {
      return this.updateById(BeanUtils.copy(formReq, ${entity}.class));
   }

   /**
    * 删除${table.comment!}
    *
    * @param ids ${table.comment!} ID列表
    * @return ${table.comment!}删除结果
    */
   @Override
   @Transactional
   public Boolean delete(Long[] ids) {
      return this.removeByIds(Arrays.asList(ids));
   }

}
