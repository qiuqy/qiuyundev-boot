package ${package.Service};

import ${package.Entity}.${entity};
import ${superServiceClassPackage};

import ${parentPackage}.base.vo.PageResult;
import ${packageVo}.${formReqName};
import ${packageVo}.${pageReqName};
import ${packageVo}.${respName};

/**
* <p>
    * ${table.comment!} 服务类
    * </p>
*
* @author ${author}
* @since ${date}
*/
public interface ${table.serviceName} extends ${superServiceClass}<${entity}> {
   /**
    * 分页查询${table.comment!}
    *
    * @param pageReq ${table.comment!}分页查询参数
    * @return ${table.comment!}列表
    */
   PageResult<${respName}> page(${pageReqName} pageReq);

    /**
     * ${table.comment!}详情
     *
     * @param id ${table.comment!} ID
     * @return ${table.comment!}详情
     */
    ${respName} detail(Integer id);

    /**
     * 新增${table.comment!}
     *
     * @param formReq ${table.comment!}新增参数
     * @return ${table.comment!}新增结果
     */
    Boolean add(${formReqName} formReq);

    /**
     * 修改${table.comment!}
     *
     * @param formReq ${table.comment!}修改参数
     * @return ${table.comment!}修改结果
     */
    Boolean update(${formReqName} formReq);


    /**
     * 删除${table.comment!}
     *
     * @param ids ${table.comment!} ID 列表
     * @return ${table.comment!}删除结果
     */
    Boolean delete(Long[] ids);
}
