package ${packageVo};

<#if springdoc>
import io.swagger.v3.oas.annotations.media.Schema;
</#if>
<#if entityLombokModel>
import lombok.Getter;
import lombok.Setter;
</#if>
import ${package.Entity}.${entity};
import com.qiuyundev.boot.base.vo.PageParam;

/**
 * <p>
 * ${table.comment!} 分页查询参数
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if entityLombokModel>
@Getter
@Setter
</#if>
@Schema(name = "${pageReqName}", description = "${table.comment!}")
public class ${pageReqName} extends PageParam<${entity}> {

<#-- ----------  BEGIN 字段循环遍历  ---------->
<#list pageVoFields as field>
    <#if field.comment!?length gt 0>
    @Schema(description = "${field.comment}")
    </#if>
    private ${field.propertyType} ${field.propertyName};

</#list>
}
