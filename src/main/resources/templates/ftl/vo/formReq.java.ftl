package ${packageVo};

<#if springdoc>
import io.swagger.v3.oas.annotations.media.Schema;
</#if>
<#if entityLombokModel>
import lombok.Getter;
import lombok.Setter;
</#if>


/**
 * <p>
 * ${table.comment!} 表单
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if entityLombokModel>
@Getter
@Setter
</#if>
@Schema(name = "${formReqName}", description = "${table.comment!}")
public class ${formReqName} {

<#-- ----------  BEGIN 字段循环遍历  ---------->
<#list formVoFields as field>
    <#if field.comment!?length gt 0>
    @Schema(description = "${field.comment}")
    </#if>
    private ${field.propertyType} ${field.propertyName};

</#list>
}
