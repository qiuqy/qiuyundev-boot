package ${package.Controller};

import org.springframework.web.bind.annotation.RequestMapping;
<#if restControllerStyle>
import org.springframework.web.bind.annotation.RestController;
<#else>
import org.springframework.stereotype.Controller;
</#if>
<#if superControllerClassPackage??>
import ${superControllerClassPackage};
</#if>

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ${parentPackage}.base.vo.PageResult;
import ${parentPackage}.base.vo.Response;
import ${package.Service}.${table.serviceName};
import ${packageVo}.${formReqName};
import ${packageVo}.${pageReqName};
import ${packageVo}.${respName};

/**
 * <p>
 * ${table.comment!} 前端控制器
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if restControllerStyle>
@RestController
<#else>
@Controller
</#if>
@RequiredArgsConstructor
@RequestMapping("<#if package.ModuleName?? && package.ModuleName != "">/${package.ModuleName}</#if>/<#if controllerMappingHyphenStyle>${controllerMappingHyphen}<#else>${table.entityPath}</#if>")
<#if superControllerClass??>
public class ${table.controllerName} extends ${superControllerClass} {
<#else>
public class ${table.controllerName} {
</#if>
    private final ${table.serviceName} service;

    /**
     * 分页查询${table.comment!}
     *
     * @param pageReq ${table.comment!}分页查询参数
     * @return ${table.comment!}列表
     */
    @GetMapping("/page")
    public Response<PageResult<${respName}>> page(${pageReqName} pageReq) {
        return Response.success(this.service.page(pageReq));
    }

    /**
     * ${table.comment!}详情
     *
     * @param id ${table.comment!} ID
     * @return ${table.comment!}详情
     */
    @GetMapping("/{id}")
    public Response<${respName}> detail(@PathVariable("id") Integer id) {
        return Response.success(this.service.detail(id));
    }

    /**
     * 新增${table.comment!}
     *
     * @param formReq ${table.comment!}新增参数
     * @return ${table.comment!}新增结果
     */
    @PostMapping
    public Response<Boolean> add(@RequestBody ${formReqName} formReq) {
       return Response.success(this.service.add(formReq));
    }

    /**
     * 修改${table.comment!}
     *
     * @param id          ${table.comment!} ID
     * @param formReq ${table.comment!}修改参数
     * @return ${table.comment!}修改结果
     */
    @PutMapping("/{id}")
    public Response<Boolean> update(@PathVariable("id") Integer id, @RequestBody ${formReqName} formReq) {
        formReq.setId(id);
        return Response.success(this.service.update(formReq));
    }


    /**
     * 删除${table.comment!}
     *
     * @param ids ${table.comment!} ID 列表
     * @return ${table.comment!}删除结果
     */
    @DeleteMapping("/{ids}")
    public Response<Boolean> delete(@PathVariable("ids") Long[] ids) {
        return Response.success(this.service.delete(ids));
    }

}
